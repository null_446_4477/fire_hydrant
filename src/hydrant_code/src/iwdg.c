#include "stm32f10x.h"
#include "stm32f10x_conf.h"

#include "system.h"

/* 看门狗开关, 1 */
//#define WATCH_DOG

void reset(void)
{
	IWDG_WriteAccessCmd(IWDG_WriteAccess_Enable);/* 写入0x5555,用于允许狗狗寄存器写入功能 */
	IWDG_SetPrescaler(IWDG_Prescaler_4);		 /* 狗狗时钟分频,40K/128=156HZ(3.2ms)*/
	IWDG_SetReload(1);        /* 喂狗时间 100ms/6.4MS=16 .注意不能大于0xfff*/
	IWDG_ReloadCounter();
	IWDG_Enable();
	while (1);
}

/*
 * 初始化iwdg
 */
void iwdg_init()
{
#ifdef WATCH_DOG
	RCC_LSICmd(ENABLE);
	/* wait until lsi is ready */
	while (RCC_GetFlagStatus(RCC_FLAG_LSIRDY) == RESET);

	IWDG_WriteAccessCmd(IWDG_WriteAccess_Enable);
	IWDG_SetPrescaler(IWDG_Prescaler_16);		// 0.4~1771ms
	IWDG_SetReload(2315);	// 1s
	IWDG_ReloadCounter();
#endif
	return;
}

void iwdg_start()
{
#ifdef WATCH_DOG
	IWDG_Enable();
#endif
	return;
}

void iwdg_reload(void)
{
#ifdef WATCH_DOG
	IWDG_ReloadCounter();
#endif
	return;
}



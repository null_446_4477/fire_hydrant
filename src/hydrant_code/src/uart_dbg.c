#include "system.h"
#include "uart_dbg.h"

/* debug uart:
 * dbg_tx -> PC10
 * dbg_rx -> PC11
 */

#define UART_BUFMAX	0x3F
#define UART_BUFLEN	64
#define UART_BUFADD(X, L)		((X + L) & UART_BUFMAX)
#define UART_BUFSUB(X, L)		((X - L) & UART_BUFMAX)
struct uart_buf {
	u8 data[UART_BUFLEN];
	u8 head;
	u8 tail;
};

static struct uart_buf uart_tx;
static struct uart_buf uart_rx;
extern u8 tt_cal_start;
static u8 uart_ready = 0;		/* 1->cmd ready */

int fputc (int ch, FILE *f)  {
	/* Debug output to serial port. */
	if (ch == '\n')  {
		while (!(DGBUART->SR & 0x0080));
		DGBUART->DR = 0x0D;
	}
	while (!(DGBUART->SR & 0x0080));
	DGBUART->DR = (ch & 0xFF);
	return (ch);
}

// buf清空
static void _buf_clear(struct uart_buf *buf)
{
	buf->head = buf->tail = 0;
}

// buf有数据?,0=无
static int _buf_is_data(struct uart_buf *buf)
{
	return (buf->tail - buf->head);
}

// buf中数据长度
static u8 _buf_len(struct uart_buf *buf)
{
	u8 len = 0;
	if (buf->tail - buf->head > 0) {
		len = buf->tail - buf->head;
	}
	if (buf->tail - buf->head < 0) {
		len = UART_BUFLEN - buf->head + buf->tail;
	}
	return len;
}

// 向buf中存数据
static int _buf_set(struct uart_buf *buf, u8 *data, u8 len)
{
	int i;
	if (len > UART_BUFLEN) {
		return -1;
	}
	for (i = 0; i < len; i++) {
		buf->data[buf->tail] = data[i];
		buf->tail = UART_BUFADD(buf->tail, 1);
	}
	return 0;
}

// 从buf中提取数据
static int _buf_get(u8 *data, struct uart_buf *buf, u8 len)
{
	int i;
	if (len > UART_BUFLEN) {
		return -1;
	}
	for (i = 0; i < len; i++) {
		data[i] = buf->data[buf->head];
		buf->head = UART_BUFADD(buf->head, 1);
	}
	return 0;
}

// 从buf中回退N个数据
static int _buf_back(struct uart_buf *buf, u8 len)
{
	while (buf->tail != buf->head && len) {
		buf->tail = UART_BUFSUB(buf->tail, 1);
		len--;
	}
	return 0;
}

static int dbg_uart_enable_clk(void)
{
	/* RCC */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);
	return 0;
}

static int dbg_uart_gpio_cfg(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;

	/* Configure usat1_tx as alternate function push-pull */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;	// wjzhe, 10M to 2M
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	/* Configure usat1_rx as input floating */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	return 0;
}

static void dbg_uart_inf(void)
{
	debug("debug init ok\n");
}

int dbg_uart_init(void)
{
	USART_InitTypeDef USART_InitStructure;
	NVIC_InitTypeDef  NVIC_InitStructure;

	dbg_uart_enable_clk();
	dbg_uart_gpio_cfg();

	NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;               //通道设置为串口1中断
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 9;       //中断占先等级0
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;              //中断响应优先级0
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;                 //打开中断
	NVIC_Init(&NVIC_InitStructure);                                 //初始化

	/* USART3 configured as follow:
	- BaudRate = 115200 baud
	- Word Length = 8 Bits
	- One Stop Bit
	- No parity
	- Hardware flow control disabled (RTS and CTS signals)
	- Receive and transmit enabled*/
	USART_InitStructure.USART_BaudRate = 115200;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode =  USART_Mode_Rx | USART_Mode_Tx;

	/* Configure the USART1 */
	USART_Init(DGBUART, &USART_InitStructure);

	/* Enable the USART Transmoit interrupt: this interrupt is generated when the
	USART1 transmit data register is empty */
	USART_ITConfig(DGBUART, USART_IT_TXE, DISABLE);

	/* Enable the USART Receive interrupt: this interrupt is generated when the
	USART1 receive data register is not empty */
	USART_ITConfig(DGBUART, USART_IT_RXNE, ENABLE);

	/* Enable USART1 */
	USART_Cmd(DGBUART, ENABLE);

	dbg_uart_inf();
	return 0;
}

// tx
void uart_tx_data(struct uart_buf *buf)
{
	u8 tmp;
	if (_buf_is_data(buf)) {
		_buf_get(&tmp, buf, 1);
		USART_SendData(DGBUART, tmp);
		USART_ITConfig(DGBUART, USART_IT_TXE, ENABLE);
	}
}

enum {
	CMD_SET = 1,
	CMD_SAVE,
	CMD_RESET,
};
// command one
static int _pkg_cmd_first(char *ptr)
{
	const char cmd_set[] = "Set";
	const char cmd_save[] = "Save";
	const char cmd_reset[] = "Reset";
	if (!strncasecmp(ptr, cmd_set, sizeof(cmd_set) - 1)) {
		return CMD_SET;
	}
	if (!strncasecmp(ptr, cmd_save, sizeof(cmd_save) - 1)) {
		return CMD_SAVE;
	}
	if (!strncasecmp(ptr, cmd_reset, sizeof(cmd_reset) - 1)) {
		return CMD_RESET;
	}
	return 0;
}
enum {
	CMD_MODE = 1,
	CMD_ADDR,
	CMD_DEBUG,
};

static int _pkg_cmd_second(char *ptr)
{
#define Mode		"mode"
#define Addr		"addr"
	if (!strncasecmp(ptr, Mode, strlen(Mode))) {
		return CMD_MODE;
	}
	if (!strncasecmp(ptr, Addr, strlen(Addr))) {
		return CMD_ADDR;
	}
	if (!strncasecmp(ptr, "debug", strlen("debug"))) {
		return CMD_DEBUG;
	}
	return 0;
}

// command three
static int _pkg_cmd_third(char *ptr, u8 inf)
{
	u32 tmp;
	int val;
	switch (inf) {
	case CMD_MODE :
		return 1;
	case CMD_ADDR :
		val = sscanf(ptr, "%d", &tmp);
		if (val == 1) {
			return 1;
		}
		break;
	case CMD_DEBUG:
		val = sscanf(ptr, "%d", &tmp);
		if (val == 1) {
			return 1;
		}
		break;
	default :
		break;
	}
	return 0;
}

static void _cmd_fail(void)
{
	debug("The command parsing failure...\r\n");
}

// 中断数据
void uart_handler(void)
{
	const char *delim = " "; /* separator: ' ' */
	char *token;
	char *data;
	char mode = 0xFF;
	char inf = 0xFF;
	token = strtok((char *)uart_rx.data, delim);
	if (token) {
		mode = _pkg_cmd_first(token);
	}
	switch (mode) {
	case CMD_SET :
		token = strtok(NULL, delim);
		data = strtok(NULL, delim);
		if (token && data) {
			inf = _pkg_cmd_second(token);
			if (!_pkg_cmd_third(data, inf)) {
				_cmd_fail();
			}
		} else {
			_cmd_fail();
		}
		printf("> ");
		break;
	case CMD_SAVE :
		printf("> ");
		break;
	case CMD_RESET:
		reset();
		printf("> ");
		break;
	default :
		_cmd_fail();
		printf("> ");
	}
	_buf_clear(&uart_rx);
}

// run n ms
int uart_scan(void)
{
	if (uart_ready) {
		uart_handler();
		uart_ready = 0;
	}
	return 0;
}

// uart char handle, 1->ok, 0,-1->non
static int readchar(u8 achar)
{
	u8 val;
	const char *erase_seq = "\b";		/* erase sequence	*/
	const char *enter_seq = "\r\n";		/* enter sequence */
	const char *enter2_seq = " \r\n> ";		/* enter sequence */
	const char *overflow_seq = "The Rx buf overflow, please clear!\r\n> ";
	switch (achar) {
	case '\r' :
	case '\n' :		/* Enter */
		val = '\0';
		_buf_set(&uart_rx, &val, 1);
		_buf_set(&uart_tx, (u8 *)enter_seq, sizeof(enter_seq));
		uart_tx_data(&uart_tx);
		return 1;
	case 0x03 :	/* ^C - break */
		_buf_clear(&uart_rx);
		_buf_set(&uart_tx, (u8 *)enter2_seq, sizeof(enter2_seq));
		uart_tx_data(&uart_tx);
		break;
	case 0x08 :	/* ^H - backspace */
	case 0x7F :	/* DEL - backspace */
		_buf_back(&uart_rx, 1);
		_buf_set(&uart_tx, (u8 *)erase_seq, sizeof(erase_seq));
		break;
	default : 		/* Must be a normal character then */
		if (_buf_len(&uart_rx) >= UART_BUFLEN - 2) {
			_buf_set(&uart_tx, (u8 *)overflow_seq, sizeof(overflow_seq));
		} else {
			_buf_set(&uart_rx, &achar, 1);
			_buf_set(&uart_tx, &achar, 1);
		}
		break;
	}
	uart_tx_data(&uart_tx);
	return 0;
}

// uart1 irq
void USART1_IRQHandler(void)
{
	u8 tmp;
	// tx
	if (USART_GetITStatus(DGBUART, USART_IT_TXE) == SET) {
		if (_buf_is_data(&uart_tx)) {
			_buf_get(&tmp, &uart_tx, 1);
			USART_SendData(DGBUART, tmp);
		} else {
			USART_ITConfig(DGBUART, USART_IT_TXE, DISABLE);
		}
		//USART_ClearITPendingBit(DGBUART, USART_IT_TXE);
	}
	// rx
	if (USART_GetITStatus(DGBUART, USART_IT_RXNE) == SET) {
		u8 val = USART_ReceiveData(DGBUART);
		if (readchar(val)) {
			uart_ready = 1;
		}
		USART_ClearITPendingBit(DGBUART, USART_IT_RXNE);
	}
}

void wait_key(void)
{
	debug("press enter to continue...\n");
	_buf_clear(&uart_rx);
	uart_ready = 0;
	while (uart_ready == 0);
	uart_ready = 0;
	_buf_clear(&uart_rx);
	return;
}


#include "system.h"
#include "i2c.h"
#include "delay.h"

struct i2c_gpio_data i2cgpio[] = {
	{	/* ADXL345 */
		GPIOB,			/* sda gpio */
		GPIOB,			/* scl gpio */
		GPIO_Pin_7,		/* sda */
		GPIO_Pin_6,		/* scl */
		2,				/* udelay */
		20,				/* timeout, ms */
		1				/* retries */
	}
};

#define setsda(i2c, val)	i2c_gpio_setsda_val(i2c, val)
#define setscl(i2c, val)	i2c_gpio_setscl_val(i2c, val)
#define getsda(i2c)			i2c_gpio_getsda(i2c)
#define getscl(i2c)			i2c_gpio_getscl(i2c)

/*
 * IO方式实现I2C
 */
void i2c_init(void)
{
	int i;
	int n = sizeof(i2cgpio) / sizeof(i2cgpio[0]);
	struct i2c_gpio_data *i2c = i2cgpio;
	GPIO_InitTypeDef gpio = {0};
	
	//RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);
	//RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOD, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);

	for (i = 0; i < n; i++, i2c++) {
		gpio.GPIO_Pin = i2c->sda_pin;
		gpio.GPIO_Speed = GPIO_Speed_10MHz;
		gpio.GPIO_Mode = GPIO_Mode_Out_OD;
		GPIO_Init(i2c->sda_gpio, &gpio);

		gpio.GPIO_Pin = i2c->scl_pin;
		gpio.GPIO_Speed = GPIO_Speed_10MHz;
		gpio.GPIO_Mode = GPIO_Mode_Out_OD;
		GPIO_Init(i2c->scl_gpio, &gpio);

		GPIO_SetBits(i2c->sda_gpio, i2c->sda_pin);
		GPIO_SetBits(i2c->scl_gpio, i2c->scl_pin);
	}
	return;
}

/* Toggle SDA by changing the direction of the pin */
static void i2c_gpio_setsda_dir(struct i2c_gpio_data *i2c, int state)
{
#if 0
	struct i2c_gpio_platform_data *pdata = data;

	if (state)
		gpio_direction_input(pdata->sda_pin);
	else
		gpio_direction_output(pdata->sda_pin, 0);
#endif
}

/*
 * Toggle SDA by changing the output value of the pin. This is only
 * valid for pins configured as open drain (i.e. setting the value
 * high effectively turns off the output driver.)
 */
static void i2c_gpio_setsda_val(struct i2c_gpio_data *i2c, int state)
{
	GPIO_TypeDef *gpio = i2c->sda_gpio;
	
	if (state) {
		gpio->BSRR = i2c->sda_pin;
	} else {
		gpio->BRR = i2c->sda_pin;
	}
}

/* Toggle SCL by changing the direction of the pin. */
static void i2c_gpio_setscl_dir(struct i2c_gpio_data *i2c, int state)
{
#if 0
	struct i2c_gpio_platform_data *pdata = data;

	if (state)
		gpio_direction_input(pdata->scl_pin);
	else
		gpio_direction_output(pdata->scl_pin, 0);
#endif
}

/*
 * Toggle SCL by changing the output value of the pin. This is used
 * for pins that are configured as open drain and for output-only
 * pins. The latter case will break the i2c protocol, but it will
 * often work in practice.
 */
static void i2c_gpio_setscl_val(struct i2c_gpio_data *i2c, int state)
{
	GPIO_TypeDef *gpio = i2c->scl_gpio;
	
	if (state) {
		gpio->BSRR = i2c->scl_pin;
	} else {
		gpio->BRR = i2c->scl_pin;
	}
}

static int i2c_gpio_getsda(struct i2c_gpio_data *i2c)
{
	return i2c->sda_gpio->IDR & i2c->sda_pin;
}

static int i2c_gpio_getscl(struct i2c_gpio_data *i2c)
{
	return i2c->scl_gpio->IDR & i2c->scl_pin;
}

static void sdalo(struct i2c_gpio_data *i2c)
{
	setsda(i2c, 0);
	udelay((i2c->udelay + 1) / 2);
}

static void sdahi(struct i2c_gpio_data *i2c)
{
	setsda(i2c, 1);
	udelay((i2c->udelay + 1) / 2);
}

static void scllo(struct i2c_gpio_data *i2c)
{
	setscl(i2c, 0);
	udelay(i2c->udelay / 2);
}

/*
 * Raise scl line, and do checking for delays. This is necessary for slower
 * devices.
 */
static int sclhi(struct i2c_gpio_data *i2c)
{
	unsigned long start;

	setscl(i2c, 1);

	start = i2c->timeout;
	if (!start) {
		goto done;
	}
	
	while (!getscl(i2c)) {
		/* This hw knows how to read the clock line, so we wait
		 * until it actually gets high.  This is safer as some
		 * chips may hold it low ("clock stretching") while they
		 * are processing data internally.
		 */
		if (start == 0) {
			return -1;
		}
		mdelay(1);
		start--;
	}

done:
	udelay(i2c->udelay);
	return 0;
}


/* --- other auxiliary functions --------------------------------------	*/
static void i2c_start(struct i2c_gpio_data *i2c)
{
	/* assert: scl, sda are high */
	setsda(i2c, 0);
	udelay(i2c->udelay);
	scllo(i2c);
}

static void i2c_repstart(struct i2c_gpio_data *i2c)
{
	/* assert: scl is low */
	sdahi(i2c);
	sclhi(i2c);
	setsda(i2c, 0);
	udelay(i2c->udelay);
	scllo(i2c);
}


static void i2c_stop(struct i2c_gpio_data *i2c)
{
	/* assert: scl is low */
	sdalo(i2c);
	sclhi(i2c);
	setsda(i2c, 1);
	udelay(i2c->udelay);
}



/* send a byte without start cond., look for arbitration,
   check ackn. from slave */
/* returns:
 * 1 if the device acknowledged
 * 0 if the device did not ack
 * -ETIMEDOUT if an error occurred (while raising the scl line)
 */
static int i2c_outb(struct i2c_gpio_data *i2c, unsigned char c)
{
	int i;
	int sb;
	int ack;
	//struct i2c_algo_bit_data *i2c = i2c_adap->algo_data;

	/* assert: scl is low */
	for (i = 7; i >= 0; i--) {
		sb = (c >> i) & 1;
		setsda(i2c, sb);
		udelay((i2c->udelay + 1) / 2);
		if (sclhi(i2c) < 0) { /* timed out */
			printf("i2c_outb: 0x%02x, timeout at bit #%d\n", (int)c, i);
			return -1;
		}
		/* FIXME do arbitration here:
		 * if (sb && !getsda(i2c)) -> ouch! Get out of here.
		 *
		 * Report a unique code, so higher level code can retry
		 * the whole (combined) message and *NOT* issue STOP.
		 */
		scllo(i2c);
	}
	sdahi(i2c);
	if (sclhi(i2c) < 0) { /* timeout */
		printf("i2c_outb: 0x%02x, timeout at ack\n", (int)c);
		return -1;
	}

	/* read ack: SDA should be pulled down by slave, or it may
	 * NAK (usually to report problems with the data we wrote).
	 */
	ack = !getsda(i2c);    /* ack: sda is pulled low -> success */
	//printf("i2c_outb: 0x%02x %s\n", (int)c, ack ? "A" : "NA");

	scllo(i2c);
	return ack;
	/* assert: scl is low (sda undef) */
}


static int i2c_inb(struct i2c_gpio_data *i2c)
{
	/* read byte via i2c port, without start/stop sequence	*/
	/* acknowledge is sent in i2c_read.			*/
	int i;
	unsigned char indata = 0;
	//struct i2c_algo_bit_data *adap = i2c_adap->algo_data;

	/* assert: scl is low */
	sdahi(i2c);
	for (i = 0; i < 8; i++) {
		if (sclhi(i2c) < 0) { /* timeout */
			printf("i2c_inb: timeout at bit #%d\n", 7 - i);
			return -1;
		}
		indata *= 2;
		if (getsda(i2c))
			indata |= 0x01;
		setscl(i2c, 0);
		udelay(i == 7 ? i2c->udelay / 2 : i2c->udelay);
	}
	/* assert: scl is low */
	return indata;
}

/*
 * Sanity check for the adapter hardware - check the reaction of
 * the bus lines only if it seems to be idle.
 */
int i2c_test_bus(struct i2c_gpio_data *i2c)
{
	int scl, sda;

	sda = getsda(i2c);
	scl = getscl(i2c);
	if (!scl || !sda) {
		printf("i2c bus seems to be busy\n");
		goto bailout;
	}

	sdalo(i2c);
	sda = getsda(i2c);
	scl = getscl(i2c);
	if (sda) {
		printf("i2c: SDA stuck high!\n");
		goto bailout;
	}
	if (!scl) {
		printf("i2c: SCL unexpected low while pulling SDA low!\n");
		goto bailout;
	}

	sdahi(i2c);
	sda = getsda(i2c);
	scl = getscl(i2c);
	if (!sda) {
		printf("i2c: SDA stuck low!\n");
		goto bailout;
	}
	if (!scl) {
		printf("i2c: SCL unexpected low while pulling SDA high!\n");
		goto bailout;
	}

	scllo(i2c);
	sda = getsda(i2c);
	scl = getscl(i2c);
	if (scl) {
		printf("i2c: SCL stuck high!\n");
		goto bailout;
	}
	if (!sda) {
		printf("i2c: SDA unexpected low while pulling SCL low!\n");
		goto bailout;
	}

	sclhi(i2c);
	sda = getsda(i2c);
	scl = getscl(i2c);
	if (!scl) {
		printf("i2c: SCL stuck low!\n");
		goto bailout;
	}
	if (!sda) {
		printf("i2c: SDA unexpected low while pulling SCL high!\n");
		goto bailout;
	}
	printf("i2c: Test OK\n");
	return 0;
bailout:
	sdahi(i2c);
	sclhi(i2c);
	return -1;
}

/* ----- Utility functions
 */

/* try_address tries to contact a chip for a number of
 * times before it gives up.
 * return values:
 * 1 chip answered
 * 0 chip did not answer
 * -x transmission error
 */
static int try_address(struct i2c_gpio_data *i2c,
		       unsigned char addr, int retries)
{
	int i, ret = 0;

	for (i = 0; i <= retries; i++) {
		ret = i2c_outb(i2c, addr);
		if (ret == 1 || i == retries)
			break;
		//bit_dbg(3, &i2c_adap->dev, "emitting stop condition\n");
		i2c_stop(i2c);
		udelay(i2c->udelay);
		//bit_dbg(3, &i2c_adap->dev, "emitting start condition\n");
		i2c_start(i2c);
	}
	if (i && ret)
		printf("I2C: Used %d tries to %s client at 0x%02x: %s\n", i + 1,
			addr & 1 ? "read from" : "write to", addr >> 1,
			ret == 1 ? "success" : "failed, timeout?");
	return ret;
}

static int sendbytes(struct i2c_gpio_data *i2c, struct i2c_msg *msg)
{
	const unsigned char *temp = msg->buf;
	int count = msg->len;
	unsigned short nak_ok = msg->flags & I2C_M_IGNORE_NAK;
	int retval;
	int wrcount = 0;

	while (count > 0) {
		retval = i2c_outb(i2c, *temp);

		/* OK/ACK; or ignored NAK */
		if ((retval > 0) || (nak_ok && (retval == 0))) {
			count--;
			temp++;
			wrcount++;

		/* A slave NAKing the master means the slave didn't like
		 * something about the data it saw.  For example, maybe
		 * the SMBus PEC was wrong.
		 */
		} else if (retval == 0) {
			printf("i2c: sendbytes: NAK bailout.\n");
			return -1;

		/* Timeout; or (someday) lost arbitration
		 *
		 * FIXME Lost ARB implies retrying the transaction from
		 * the first message, after the "winning" master issues
		 * its STOP.  As a rule, upper layer code has no reason
		 * to know or care about this ... it is *NOT* an error.
		 */
		} else {
			printf("i2c: sendbytes: error %d\n", retval);
			return retval;
		}
	}
	return wrcount;
}

static int acknak(struct i2c_gpio_data *i2c, int is_ack)
{
	//struct i2c_algo_bit_data *adap = i2c_adap->algo_data;

	/* assert: sda is high */
	if (is_ack)		/* send ack */
		setsda(i2c, 0);
	udelay((i2c->udelay + 1) / 2);
	if (sclhi(i2c) < 0) {	/* timeout */
		printf("i2c: readbytes: ack/nak timeout\n");
		return -1;
	}
	scllo(i2c);
	return 0;
}

static int readbytes(struct i2c_gpio_data *i2c, struct i2c_msg *msg)
{
	int inval;
	int rdcount = 0;	/* counts bytes read */
	unsigned char *temp = msg->buf;
	int count = msg->len;
	const unsigned flags = msg->flags;

	while (count > 0) {
		inval = i2c_inb(i2c);
		if (inval >= 0) {
			*temp = inval;
			rdcount++;
		} else {   /* read timed out */
			break;
		}

		temp++;
		count--;

#if 0
		/* Some SMBus transactions require that we receive the
		   transaction length as the first read byte. */
		if (rdcount == 1 && (flags & I2C_M_RECV_LEN)) {
			if (inval <= 0 || inval > I2C_SMBUS_BLOCK_MAX) {
				if (!(flags & I2C_M_NO_RD_ACK))
					acknak(i2c, 0);
				printf("i2c: readbytes: invalid block length (%d)\n", inval);
				return -1;
			}
			/* The original count value accounts for the extra
			   bytes, that is, either 1 for a regular transaction,
			   or 2 for a PEC transaction. */
			count += inval;
			msg->len += inval;
		}
#endif
#if 0
		bit_dbg(2, &i2c_adap->dev, "readbytes: 0x%02x %s\n",
			inval,
			(flags & I2C_M_NO_RD_ACK)
				? "(no ack/nak)"
				: (count ? "A" : "NA"));
#endif

		if (!(flags & I2C_M_NO_RD_ACK)) {
			inval = acknak(i2c, count);
			if (inval < 0)
				return inval;
		}
	}
	return rdcount;
}

/* doAddress initiates the transfer by generating the start condition (in
 * try_address) and transmits the address in the necessary format to handle
 * reads, writes as well as 10bit-addresses.
 * returns:
 *  0 everything went okay, the chip ack'ed, or IGNORE_NAK flag was set
 * -x an error occurred (like: -EREMOTEIO if the device did not answer, or
 *	-ETIMEDOUT, for example if the lines are stuck...)
 */
static int bit_doAddress(struct i2c_gpio_data *i2c, struct i2c_msg *msg)
{
	unsigned short flags = msg->flags;
	unsigned short nak_ok = msg->flags & I2C_M_IGNORE_NAK;
	//struct i2c_algo_bit_data *adap = i2c_adap->algo_data;

	unsigned char addr;
	int ret, retries;

	retries = nak_ok ? 0 : i2c->retries;

	if (flags & I2C_M_TEN) {
		/* a ten bit address */
		addr = 0xf0 | ((msg->addr >> 7) & 0x03);
		//bit_dbg(2, &i2c_adap->dev, "addr0: %d\n", addr);
		/* try extended address code...*/
		ret = try_address(i2c, addr, retries);
		if ((ret != 1) && !nak_ok)  {
			printf("i2c: died at extended address code\n");
			return -1;
		}
		/* the remaining 8 bit address */
		ret = i2c_outb(i2c, msg->addr & 0x7f);
		if ((ret != 1) && !nak_ok) {
			/* the chip did not ack / xmission error occurred */
			printf("i2c: died at 2nd address code\n");
			return -1;
		}
		if (flags & I2C_M_RD) {
			//bit_dbg(3, &i2c_adap->dev, "emitting repeated "
			//	"start condition\n");
			i2c_repstart(i2c);
			/* okay, now switch into reading mode */
			addr |= 0x01;
			ret = try_address(i2c, addr, retries);
			if ((ret != 1) && !nak_ok) {
				printf("i2c: died at repeated address code\n");
				return -1;
			}
		}
	} else {		/* normal 7bit address	*/
		addr = msg->addr << 1;
		if (flags & I2C_M_RD)
			addr |= 1;
		if (flags & I2C_M_REV_DIR_ADDR)
			addr ^= 1;
		ret = try_address(i2c, addr, retries);
		if ((ret != 1) && !nak_ok) {
			printf("i2c: died at extended address code\n");
			return -1;
		}
	}

	return 0;
}

int i2c_xfer(struct i2c_gpio_data *i2c,
		    struct i2c_msg msgs[], int num)
{
	struct i2c_msg *pmsg;
	//struct i2c_algo_bit_data *adap = i2c_adap->algo_data;
	int i, ret;
	unsigned short nak_ok;

	//bit_dbg(3, &i2c_adap->dev, "emitting start condition\n");
	i2c_start(i2c);
	for (i = 0; i < num; i++) {
		pmsg = &msgs[i];
		nak_ok = pmsg->flags & I2C_M_IGNORE_NAK;
		if (!(pmsg->flags & I2C_M_NOSTART)) {
			if (i) {
				//bit_dbg(3, &i2c_adap->dev, "emitting "
				//	"repeated start condition\n");
				i2c_repstart(i2c);
			}
			ret = bit_doAddress(i2c, pmsg);
			if ((ret != 0) && !nak_ok) {
				//bit_dbg(1, &i2c_adap->dev, "NAK from "
				//	"device addr 0x%02x msg #%d\n",
				//	msgs[i].addr, i);
				goto bailout;
			}
		}
		if (pmsg->flags & I2C_M_RD) {
			/* read bytes into buffer*/
			ret = readbytes(i2c, pmsg);
			if (ret >= 1)
				//bit_dbg(2, &i2c_adap->dev, "read %d byte%s\n",
				//	ret, ret == 1 ? "" : "s");
			if (ret < pmsg->len) {
				if (ret >= 0)
					ret = -1;
				goto bailout;
			}
		} else {
			/* write bytes from buffer */
			ret = sendbytes(i2c, pmsg);
			if (ret >= 1)
				//bit_dbg(2, &i2c_adap->dev, "wrote %d byte%s\n",
				//	ret, ret == 1 ? "" : "s");
			if (ret < pmsg->len) {
				if (ret >= 0)
					ret = -1;
				goto bailout;
			}
		}
	}
	ret = i;

bailout:
	//bit_dbg(3, &i2c_adap->dev, "emitting stop condition\n");
	i2c_stop(i2c);
	return ret;
}

/* i2c寄存器写入, data[0] reg addr */
int i2c_reg_write(struct i2c_gpio_data *i2c, u8 addr, u8 *data, u16 n)
{
	struct i2c_msg msg;

	msg.addr = addr;
	msg.flags = 0;
	msg.len = n;
	msg.buf = data;
	
	return i2c_xfer(i2c, &msg, 1);
}

int i2c_reg_read(struct i2c_gpio_data *i2c, u8 addr, u8 reg, u8 *data, u16 n)
{
	struct i2c_msg msg[2];
		
	// read register
	msg[0].addr = addr;
	msg[0].flags = 0;
	msg[0].len = 1;
	msg[0].buf = &reg;

	msg[1].addr = addr;
	msg[1].flags = I2C_M_RD;
	msg[1].len = n;
	msg[1].buf = data;
	
	return i2c_xfer(i2c, msg, 2);
}


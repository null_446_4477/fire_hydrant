#include "system.h"
#include "adc.h"
#include "device.h"
#include "led.h"
#include "timer.h"

#define adc1_ctrl_on (GPIOC->ODR |= GPIO_Pin_2)
#define adc1_ctrl_off (GPIOC->ODR &= ~GPIO_Pin_2)
u16 low_power_val = 280;
u16 adc_sample_interval = 7200;
// adc采样函数
u16 adc_sample(void)
{
	u16 temp = 0;

    ADC_Cmd(ADC1, ENABLE);    //使能ADC1
	
	ADC_RegularChannelConfig(ADC1, ADC_Channel_4, 1, ADC_SampleTime_13Cycles5);
	ADC_SoftwareStartConvCmd(ADC1, ENABLE);
	while(!ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC));
	temp = ADC_GetConversionValue(ADC1);
	
	ADC_Cmd(ADC1, DISABLE);   //失能ADC1

	return temp;
}

// adc 转换为真实电压值
u16 adc_get_realvol(void)
{
	u16 advol;         //检测到的AD端电压
	u16 truevccvol;       //真实电压
	u16 temp;

	temp = adc_sample();
	//AD基准电压 * 采集到的数字量 / 分辨率
	advol = (u16)((temp*3330)/4095);
	truevccvol = (u16)(((49.9 + 49.9)*advol)/49.9/10.0);
	
	return truevccvol;
}

//ctrl引脚初始化
void adc_ctrl_init(void)
{
	GPIO_InitTypeDef  GPIO_InitStructure;
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC,ENABLE);	//使能GPIOC时钟
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;				//端口配置
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; 		//推挽输出
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;		//IO口速度为50MHz
	GPIO_Init(GPIOC, &GPIO_InitStructure);

	adc1_ctrl_on;
}

// adc init
void ADC_config(void)
{
	ADC_InitTypeDef ADC_InitStructure;
	GPIO_InitTypeDef GPIO_InitStructure;
	RCC_ClocksTypeDef RCC_ClockFreq;

	debug("...........................................................\n");
	// RCC init, 12MHz
	RCC_ADCCLKConfig(RCC_PCLK2_Div2);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);
	RCC_GetClocksFreq(&RCC_ClockFreq);
	debug("ADCCLK[%dMHz]\n", RCC_ClockFreq.ADCCLK_Frequency / 1000000);
	
	// GPIO init
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	
	
	// ADC1 init
	ADC_InitStructure.ADC_Mode = ADC_Mode_Independent;
	ADC_InitStructure.ADC_ScanConvMode = DISABLE;
	ADC_InitStructure.ADC_ContinuousConvMode = DISABLE;
	ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None;	
	ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;	
	ADC_InitStructure.ADC_NbrOfChannel = 1;
	ADC_Init(ADC1, &ADC_InitStructure);
	// channel
	//ADC_RegularChannelConfig(ADC1, ADC_Channel_17, 1, ADC_SampleTime_13Cycles5);
	ADC_RegularChannelConfig(ADC1, ADC_Channel_4, 2, ADC_SampleTime_13Cycles5);
	// enable ADC1
	ADC_Cmd(ADC1, ENABLE);
	// Enable ADC1 reset calibration register
	ADC_ResetCalibration(ADC1);
	while(ADC_GetResetCalibrationStatus(ADC1));
	ADC_StartCalibration(ADC1);
	while(ADC_GetCalibrationStatus(ADC1));
	
//	adc_ctrl_init();//初始化CTRL引脚
	
	debug("ADC get the vol = %dmv\n", adc_sample());
	ADC_Cmd(ADC1, DISABLE);
	debug("...........................................................\n");
}

/*-------------------------------------------------------------------------------
读温度采样值		temp_get
-------------------------------------------------------------------------------*/
u16 temp_get(void)   
{
	// enable Temperate
	ADC_Cmd(ADC1, ENABLE);
	ADC_TempSensorVrefintCmd(ENABLE);
	ADC_RegularChannelConfig(ADC1, ADC_CH_TEMP, 1, ADC_SampleTime_239Cycles5);
	ADC_SoftwareStartConvCmd(ADC1, ENABLE);
	while(!ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC));
	
	// disable Temperate
	ADC_TempSensorVrefintCmd(DISABLE);
	ADC_Cmd(ADC1, DISABLE);
	return ADC_GetConversionValue(ADC1);	
}

/*-------------------------------------------------------------------------------
转换成真实温度值	temperate_get
-------------------------------------------------------------------------------*/
u8 temperate_get(void)
{
	u8 temperate = 0;
	u16 temp_val = 0;
	u16 adcx = 0;
	
	/*获取温度采样*/
    temp_val = temp_get();
	
	//转换成真实温度值
	adcx = ((2122 - temp_val) << 8) / 881 ; 
	temperate = adcx - 50;
	return temperate;
} 


/*-------------------------------------------------------
  函数名： Temperature_Get
  参  数： char recollect     0 不重新采集温度，1 重新采集温度
  返回值： unsigned int   当前温度值
  功  能： 获取温度值
---------------------------------------------------------
*/
char Temperature_Get(char recollect)
{
	static char l_Collected = 0;
	static char l_Temperature;
	//static u32 l_TempOrigData;
	if(recollect || !l_Collected)
	{
	//	l_TempOrigData = 80;
		l_Temperature = temperate_get();
		l_Collected = 1;
	}
	return l_Temperature;
}
void check_battery(dev_info *dev)
{
	//检测电压并判断是否报警
	THREAD_CYCLE(adc_sample_interval * 1000);
	dev->voltage = adc_get_realvol();
	debug("check_battery : voltage %d\n", dev->voltage);
}
void battery_task(dev_info *dev)
{
	check_battery(dev);
}

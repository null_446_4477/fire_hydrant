/**
  ******************************************************************************
  * @file    main.c
  * @author  Microcontroller Division
  * @version V1.0.3
  * @date    May-2013
  * @brief   Main program body
  ******************************************************************************
 **/
/* Includes ------------------------------------------------------------------*/
#include "chipinit.h"
#include "system.h"
#include "delay.h"
#include "timer.h"
#include "uart.h"
#include "uart_dbg.h"
#include "adc.h"
#include "motor.h"
#include "switch.h"
#include "bc95.h"
#include "device.h"
#include "beep.h"
#include "sleep.h"
#include "led.h"
#include "adxl345.h"
#include "uart_bluetooth.h"
/**
  * @brief main entry point.
  * @par Parameters None
  * @retval void None
  * @par Required preconditions: None
  */

int main(void)
{
	dev_info *dev = &dev_state;
	vs32 last_bc95_init_time = 0;
	onchipinit();
	get_uid(dev->id);
	dev->voltage = adc_get_realvol();
	get_device_state(dev);
	if (dev->gprs_conn_sta) {
		dev->gprs_cmd = GPRS_CMD_SEND;
		dev->cmd_type = CMD_STA_TICK;
		dev->is_retry = 0;
		sync_rtc_time();
	}
	while (1) 
	{
		if (!dev->gprs_conn_sta && (conf_get_ready() == 0)) {
			//在没配置参数的情况下，若需要重启模块，进入这里
			if ((jiffies - last_bc95_init_time) >= (60 * 1000)) {//间隔固定时间初始化
				//重启模块
				dev->gprs_conn_sta = bc95_init();
				if (dev->gprs_conn_sta) {
					dev->gprs_cmd = GPRS_CMD_SEND;
					dev->cmd_type = CMD_STA_TICK;
					dev->is_retry = 0;
					sync_rtc_time();
				} else {
					dev->gprs_cmd = GPRS_CMD_NONE;
				}
				last_bc95_init_time = jiffies;
			}
		}
		conf_scan();
		if (dev->run_mode && dev->gprs_conn_sta) {
			get_device_state(dev);
			msg_recv_task(dev);
			server_task(dev);
			iap_task(&iap);
			battery_task(dev);
		}
		if ((dev->gprs_cmd == GPRS_CMD_NONE) && (iap.gprs_cmd == GPRS_CMD_NONE) 
			&& (adxl345_detect_flag == 0) && (conf_get_ready() == 0)
			&& (dev->water_sta_change_flag == 0)) {
			sys_sleep_scan();
			//mdelay(10);
			if (reboot_cnt >= 60) {//定期重启模块
				reboot_cnt = 0;
				dev->gprs_conn_sta = 0;
			}
		}
	}
}

void hex_dump(char *str, unsigned char *pSrcBufVA, unsigned int SrcBufLen)
{
#ifdef DEBUG
	if (DEBUG >= DBG_WARN) {
		unsigned char *pt;
		int x;

		pt = pSrcBufVA;
		printf("%s: %p, len = %d\n", str, pSrcBufVA, SrcBufLen);
		for (x = 0; x < SrcBufLen; x++) {
			if (x % 16 == 0)
				printf("0x%04x : ", x);
			printf("%02x ", ((unsigned char)pt[x]));
			if (x % 16 == 15)
				printf("\n");
		}
		printf("\n");
	}
#endif
}

#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* Infinite loop */
  printf("Wrong parameters value: file %s on line %d\r\n", file, line);
  while (1);
}

#endif

/******************* (C) COPYRIGHT 2011 STMicroelectronics *****END OF FILE****/

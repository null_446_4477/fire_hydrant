/*
 * @JL, 2013-09-10
 */
#include "system.h"
#include "delay.h"
static vu16 fac_ms;
static vu16 fac_us;

/*
 * 延时函数初始化，使用SYSTICK
 */
void delay_config(void)
{
	u16 sys;
	RCC_ClocksTypeDef RCC_ClockFreq;
	RCC_GetClocksFreq(&RCC_ClockFreq);
	if (RCC_ClockFreq.HCLK_Frequency > 2000000) {
		sys = (RCC_ClockFreq.HCLK_Frequency / 1000000);
	} else {
		sys = 32;
	}
	SysTick_CLKSourceConfig(SysTick_CLKSource_HCLK_Div8);
	fac_us = sys / 8;
	fac_ms = fac_us * 1000;
}

/*
 * uS级延时
 */
void udelay(vu32 loop)
{
	u32 temp;
	if(loop <= 0)
		return;
	SysTick->LOAD = fac_us * loop; //时间加载
	SysTick->VAL = 0x00; 	//清空计数器
	SysTick->CTRL = 0x01 ; 	//开始倒数
	do {
		temp = SysTick->CTRL;
	}while(temp&0x01 && !(temp&(1 << 16)));//等待时间到达
	SysTick->CTRL = 0x00; 	//关闭计数器
	SysTick->VAL = 0x00; 	//清空计数器
	return;
}

/*
 * mS级延时
 */
void mdelay(vu32 loop)
{
	u32 temp;
	while(loop--) {
		SysTick->LOAD = fac_ms;	//时间加载(SysTick->LOAD为24bit)
		SysTick->VAL = 0x00; 				//清空计数器
		SysTick->CTRL = 0x01 ; 				//开始倒数
		do {
			temp = SysTick->CTRL;
		}while(temp & 0x01 && !(temp & (1 << 16)));//等待时间到达
		SysTick->CTRL = 0x00; 				//关闭计数器
		SysTick->VAL = 0X00; 				//清空计数器
	}
	return;
}

#include "system.h"
#include "stm32f10x.h"
#include "flash_if.h"
#include "bc95.h"
#include "device.h"
#include "switch.h"

/**
  * @brief  Unlocks Flash for write access
  * @param  None
  * @retval None
  */
void FLASH_If_Init(void)
{ 
	/* Unlock the Program memory */
	FLASH_Unlock();

	/* Clear all FLASH flags */  
	FLASH_ClearFlag(FLASH_FLAG_EOP | FLASH_FLAG_WRPRTERR | FLASH_FLAG_PGERR);   
}

/**
  * @brief  This function does an erase of all user flash area
  * @param  StartSector: start of user flash area
  * @retval 0: user flash area successfully erased
  *         1: error occurred
  */
int FLASH_If_Erase(u32 start_addr, u32 size)
{
	u32 flashaddress;

	flashaddress = start_addr;

	while (flashaddress <= start_addr + size) {
		if (FLASH_ErasePage(flashaddress) == FLASH_COMPLETE)
		{
			flashaddress += FLASH_SECTOR_SIZE;
		}
		else
		{
			/* Error occurred while page erase */
			return (1);
		}
	}
	return (0);
}

/*
 * write a data buffer in flash (data are 32-bit aligned).
 * @note   To correctly run this function, the FLASH_Unlock() function
 *		   must be called before.
 *		   Call the FLASH_Lock() to disable the flash memory access
 *		   (recommended to protect the FLASH memory against possible unwanted operation).
 */
int FLASH_If_Write2(__IO u32 FlashAddress, u32* Data , u16 DataLength)
{
	u32 tmp;
	u32 addr = FlashAddress;
	int i;
	
	if (DataLength & 0x3) {
		printf("flash: can not write data length %d\n", DataLength);
		return -1;
	}
	for (i = 0; i < DataLength; i += 4) {
		tmp = *Data;
		if (FLASH_ProgramWord(addr, tmp) != FLASH_COMPLETE) {
			printf("write flash 0x%08x failed\n", addr);
			return -1;
		}
		// check data
		tmp = *(u32 *)addr;
		if (tmp != *Data) {
			printf("write flash 0x%08x check data failed %08x %08x\n",
				addr, *Data, *(u32 *)addr);
			return -1;
		}
		Data++;
		addr += 4;
	}
	return 0;
}


//读取指定地址的16位数据
//read_addr:读地址(2的倍数)
//返回值:FLASH对应数据.
u16 FLASH_ReadHalfWord(u32 read_addr)
{
	return *(vu16*)read_addr; 
}

//不检查的写入
//write_addr:起始地址
//p_data:数据指针
//Num:半字(16位)数   
void FLASH_Write_NoCheck(u32 write_addr, u16 *p_data, u16 num)   
{ 			 		 
	u16 i;
	for (i = 0; i < num; i ++)
	{
		FLASH_ProgramHalfWord(write_addr, p_data[i]);
	    write_addr += 2;//地址增加2.
	}  
} 

//从指定地址开始写入指定长度的数据
//Write_addr:起始地址
//p_data:数据指针
//NumToWrite:半字(16位)数

u16 FLASH_BUFFER[FLASH_SECTOR_SIZE / 2];  //最多是2K字节

void FLASH_Write(u32 write_addr, u16 *p_data, u16 num)	
{
	u32 sec_addr;	   //扇区地址
	u32 offset_addr;   //去掉基地址后的地址
	u16 sec_offset;	   //扇区内偏移地址(16位字计算)
	u16 sec_remain;    //扇区内剩余地址(16位字计算)	   
 	u16 i;    
	if (write_addr < FLASH_START_ADDRESS 
		|| (write_addr >= (FLASH_START_ADDRESS + 1024 * FLASH_SECTOR_SIZE)))
	{
		return;                                             //非法地址
	}
	FLASH_Unlock();						                    //解锁
	offset_addr = write_addr - FLASH_START_ADDRESS;		    //实际偏移地址.
	sec_addr = offset_addr / FLASH_SECTOR_SIZE;			    //扇区地址  0~127 for STM32F103RCT6
	sec_offset = (offset_addr % FLASH_SECTOR_SIZE) / 2;		//在扇区内的偏移(2个字节为基本单位.)
	sec_remain = FLASH_SECTOR_SIZE / 2 - sec_offset;		//扇区剩余空间大小   
	if (num <= sec_remain)
	{
		sec_remain = num;                                   //不大于该扇区范围
	}
	while(1) 
	{	
		FLASH_Read(sec_addr * FLASH_SECTOR_SIZE + FLASH_START_ADDRESS, 
		           FLASH_BUFFER, FLASH_SECTOR_SIZE / 2);      //读出整个扇区的内容
		for ( i = 0; i < sec_remain; i ++)                  //校验数据
		{
			if (FLASH_BUFFER[sec_offset + i] != 0xFFFF)
			{
				break;                                      //需要擦除  	  
			}
		}
		if (i < sec_remain)                                 //需要擦除
		{
			FLASH_ErasePage(sec_addr * FLASH_SECTOR_SIZE + FLASH_START_ADDRESS);//擦除这个扇区
			for (i = 0; i < sec_remain; i ++)               //复制
			{
				FLASH_BUFFER[i + sec_offset] = p_data[i];	  
			}
			FLASH_Write_NoCheck(sec_addr * FLASH_SECTOR_SIZE + FLASH_START_ADDRESS,
                    			FLASH_BUFFER, FLASH_SECTOR_SIZE / 2);//写入整个扇区  
		}
		else 
		{
			FLASH_Write_NoCheck(write_addr, p_data, sec_remain);//写已经擦除了的,直接写入扇区剩余区间. 		
		}			
		if (num == sec_remain)
		{
			break;//写入结束了
		}
		else//写入未结束
		{
			sec_addr ++;				//扇区地址增1
			sec_offset = 0;				//偏移位置为0 	 
		   	p_data += sec_remain;  	    //指针偏移
			write_addr += (sec_remain * 2);	//写地址偏移	   
		   	num -= sec_remain;	        //字节(16位)数递减
			if (num > (FLASH_SECTOR_SIZE / 2))
			{
				sec_remain = FLASH_SECTOR_SIZE / 2;
			}			
			else                        //下一个扇区还是写不完
			{
				sec_remain = num;       //下一个扇区可以写完了
			}
		}	 
	};	
	FLASH_Lock();//上锁
}

//从指定地址开始读出指定长度的数据
//ReadAddr:起始地址
//p_data:数据指针
//Num:半字(16位)数
void FLASH_Read(u32 ReadAddr, u16 *p_data, u16 num)   	
{
	u16 i;
	for (i = 0; i < num; i ++)
	{
		p_data[i] = FLASH_ReadHalfWord(ReadAddr);//读取2个字节.
		ReadAddr += 2;//偏移2个字节.	
	}
}

//获取FLASH参数
void FLASH_check(void)
{
	u16 empty_FLASH[2] = {0xFFFF,0xFFFF};

	//读取设备序列号
	FLASH_Read(FLASH_PARAM_ADDR + PARAM_SERIAL_NUM_OFFSET, empty_FLASH, 2);
	if ((empty_FLASH[0] == 0xffff) && (empty_FLASH[1] == 0xffff)) {
		dev_state.serial_num[0] = (u8)empty_FLASH[0];
		dev_state.serial_num[1] = (u8)(empty_FLASH[0]>>8);
		dev_state.serial_num[2] = (u8)empty_FLASH[1];
		dev_state.serial_num[3] = (u8)(empty_FLASH[1]>>8);
	} else {
		dev_state.serial_num[0] = (u8)empty_FLASH[0];
		dev_state.serial_num[1] = (u8)(empty_FLASH[0]>>8);
		dev_state.serial_num[2] = (u8)empty_FLASH[1];
		dev_state.serial_num[3] = (u8)(empty_FLASH[1]>>8);
	}
	debug("FLASH_check : serial number %d %d %d %d\n", 
			dev_state.serial_num[0], dev_state.serial_num[1], 
			dev_state.serial_num[2], dev_state.serial_num[3]);
	
	//读取服务器IP
	FLASH_Read(FLASH_PARAM_ADDR + PARAM_HOST_IP_OFFSET ,empty_FLASH, 2);
	if ((empty_FLASH[0] == 0xffff) && (empty_FLASH[1] == 0xffff)) {
		strcpy(bc95_body.server_info.addr, BC95_COMM_SERVER_IP);
	} else {
		char buf[128];
		char addr_buf[128];
		memset(addr_buf, 0, sizeof(addr_buf));
		memset(buf, 0, sizeof(buf));
		sprintf(buf, "%d.", (u8)empty_FLASH[0]);
		strcat(addr_buf, buf);
		memset(buf, 0, sizeof(buf));
		sprintf(buf, "%d.", (u8)(empty_FLASH[0]>>8));
		strcat(addr_buf, buf);
		memset(buf, 0, sizeof(buf));
		sprintf(buf, "%d.", (u8)empty_FLASH[1]);
		strcat(addr_buf, buf);
		memset(buf, 0, sizeof(buf));
		sprintf(buf, "%d", (u8)(empty_FLASH[1]>>8));
		strcat(addr_buf, buf);
		strncpy(bc95_body.server_info.addr, addr_buf, sizeof(bc95_body.server_info.addr));
	}
	debug("FLASH_check : host ip %s\n", bc95_body.server_info.addr);
	
	//读取服务器端口号
	FLASH_Read(FLASH_PARAM_ADDR + PARAM_HOST_PORT_OFFSET ,empty_FLASH, 1);
	if (empty_FLASH[0] == 0xffff) {
		bc95_body.server_info.port = BC95_COMM_SERVER_PORT;
	} else {
		bc95_body.server_info.port = empty_FLASH[0];
	}
	debug("FLASH_check : host port %d\n", bc95_body.server_info.port);
	
	//读取通信间隔
	FLASH_Read(FLASH_PARAM_ADDR + PARAM_COMM_INTERVAL_OFFSET ,empty_FLASH, 2);
	if ((empty_FLASH[0] == 0xffff) && (empty_FLASH[1] == 0xffff)) {
		bc95_body.comm_interval = BC95_TICK_INTERVAL;
	} else {
		bc95_body.comm_interval = empty_FLASH[0] + (empty_FLASH[1]<<16);
	}
	debug("FLASH_check : comm interval %d\n", bc95_body.comm_interval);
	
	//读取工作模式
	FLASH_Read(FLASH_PARAM_ADDR + PARAM_RUN_MODE_OFFSET ,empty_FLASH, 1);
	if (empty_FLASH[0] == 0xffff) {
		dev_state.run_mode = 0;
	} else {
		dev_state.run_mode = empty_FLASH[0];
	}
	debug("FLASH_check : run mode %d\n", dev_state.run_mode);

	//读取液位传感器误触发的时间阈值
	FLASH_Read(FLASH_PARAM_ADDR + PARAM_WT_SENSOR_INIT_OFFSET ,empty_FLASH, 2);
	if ((empty_FLASH[0] == 0xffff) && (empty_FLASH[1] == 0xffff)) {
		dev_state.water_sensor_init_threshold_time = WATER_SENSOR_INIT_THRESHOLD_TIME;
	} else {
		dev_state.water_sensor_init_threshold_time = empty_FLASH[0] + (empty_FLASH[1]<<16);
	}
	debug("FLASH_check : water_sensor_init_threshold_time %d\n", dev_state.water_sensor_init_threshold_time);
}


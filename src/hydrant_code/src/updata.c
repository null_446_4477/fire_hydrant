#include "updata.h"
#include "IAP_flash.h"
#include "IAP_iwdg.h"

#pragma arm section code = "RAMCODE"

int write_to_flash(u32 base_addr, u32 backup_addr, u32 len)
{
	u32 flash_addr = base_addr;
	u32 *flash_update_addr = (u32 *)backup_addr;
	
	if (IAP_FLASH_If_Write2(&flash_addr, flash_update_addr, len)) {
		return -1;
	}
	
	return 0;
}
// 更新固件程序
int update_bin(struct iap_info *iap_msg)
{
	IAP_iwdg_reload();
	unsigned int base_addr = iap_msg->base_addr;
	unsigned int backup_addr = iap_msg->backup_addr;
	unsigned int code_size = iap_msg->code_size;
	unsigned int page_size = iap_msg->flash_page_size;
	printf("STM32L IAP earse FLASH \n");
	// flash 擦除
	if (IAP_FLASH_If_Erase(base_addr, code_size, page_size)) {
		return -1;
	}
	// flash 写入
	if (write_to_flash(base_addr, backup_addr, code_size) < 0) {
		return -1;
	}
	return 0;
}



// iap升级
void code_updata(struct iap_info *iap_msg)
{
	disableGlobalInterrupts();	// 关闭所有中断
	// 解锁FLASH
	IAP_FLASH_Unlock();
	// 清空FLASH标志位
	IAP_FLASH_ClearFlag(FLASH_FLAG_EOP | FLASH_FLAG_WRPRTERR | FLASH_FLAG_PGERR);
	printf("stm32 IAP init ok\n");
	// 开始更新程序
	update_bin(iap_msg);
	// 上锁FLASH
	IAP_FLASH_Lock();
	// 复位芯片
	IAP_reset();
	// 开启中断
//	enableGlobalInterrupts();
	while(1) {
	}
}

#pragma arm section


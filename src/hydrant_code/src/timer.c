/*
 * 1ms
 * @JL, 2013-09-06
 */
#include "timer.h"

/* wjzhe, change timer2 to timer6 */

vs32 jiffies = 0;
vs32 _t1s_cnt = 0;
// 1s标志
u8 t1s_flag = 0;
u8 t1s_flag_sensor = 0;

static int timer_nvic_cofig(void)
{
	NVIC_InitTypeDef NVIC_InitStructure;
	/* Enable the TIM6 gloabal Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = TIM6_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;

	NVIC_Init(&NVIC_InitStructure);
	return 1;
}

void timer_1ms_config(void)
{
	u16 Period;
	RCC_ClocksTypeDef RCC_ClockFreq;
	TIM_TimeBaseInitTypeDef TIM_InitStructure;
	
	/* TIM6 clock enable */
  	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM6, ENABLE);
	timer_nvic_cofig();

	RCC_GetClocksFreq(&RCC_ClockFreq);
	/* notice:APB1 prescaler == 1 clk=PB1, else clk=PB1x2 */
	if (RCC_ClockFreq.PCLK1_Frequency > 2000000) {
		Period = (RCC_ClockFreq.HCLK_Frequency / 1000000) - 1;
	} else {
		Period = 63;
	}
	
	TIM_InitStructure.TIM_Period = Period;
	TIM_InitStructure.TIM_Prescaler = 999;
	TIM_InitStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_InitStructure.TIM_ClockDivision = 0;
	TIM_TimeBaseInit(TIM6, &TIM_InitStructure);

	TIM_PrescalerConfig(TIM6, 999, TIM_PSCReloadMode_Immediate);

	TIM_ARRPreloadConfig(TIM6, ENABLE);

	TIM_ITConfig(TIM6, TIM_IT_Update, ENABLE);
	TIM_Cmd(TIM6, ENABLE);
}

s32 TIM6_TickNum(void)
{
	return jiffies;
}

/* irq handler */
void TIM6_IRQHandler(void)
{
	jiffies++;
	_t1s_cnt++;
	if (_t1s_cnt >= 1000) {
		_t1s_cnt = 0;
		t1s_flag = 1;
		t1s_flag_sensor = 1;
	}
	TIM_ClearITPendingBit(TIM6, TIM_IT_Update);
}



//**********************************************************************************************     
//  STM32F10x StopMode RTC Feed Dog 
//  compiler: Keil UV3     
//  2013-01-04 , By friehood     
//**********************************************************************************************  
#include "system.h"
#include "sleep.h"
#include "uart_dbg.h"
#include "led.h"
#include "rtc.h"
#include "timer.h"
#include "beep.h"
#include "switch.h"
#include "delay.h"
#include "adc.h"
#include "uart.h"
#include "uart_bluetooth.h"
#include "motor.h"
#include "i2c.h"
#include "adxl345.h"

#define WAKEUP_TIME 5
// 休眠计数, cnt seconds
//static int sleep_cnt = 0;
static long long sleep_ms;

/*******************************************************************************
* Function Name  : RTCAlarm_IRQHandler
* Description    : This function handles RTC Alarm interrupt request.
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
void RTCAlarm_IRQHandler(void)
{
	if (RTC_GetITStatus(RTC_IT_ALR) != RESET) {

		/* Clear EXTI line17 pending bit */
		EXTI_ClearITPendingBit(EXTI_Line17);

		/* Check if the Wake-Up flag is set */
		if (PWR_GetFlagStatus(PWR_FLAG_WU) != RESET)
		{
			/* Clear Wake Up flag */
			PWR_ClearFlag(PWR_FLAG_WU);
		}																		

		/* Wait until last write operation on RTC registers has finished */
		RTC_WaitForLastTask();   
		/* Clear RTC Alarm interrupt pending bit */
		RTC_ClearITPendingBit(RTC_IT_ALR);
		/* Wait until last write operation on RTC registers has finished */
		RTC_WaitForLastTask();
	}
}

void rtc_clear_flag()
{
	/* Check if the Wake-Up flag is set */
	if (PWR_GetFlagStatus(PWR_FLAG_WU) != RESET)
	{
		/* Clear Wake Up flag */
		PWR_ClearFlag(PWR_FLAG_WU);
	}
	if (RTC_GetITStatus(RTC_IT_ALR) != RESET) {

		/* Clear EXTI line17 pending bit */
		EXTI_ClearITPendingBit(EXTI_Line17);
		/* Clear RTC Alarm interrupt pending bit */
		RTC_ClearITPendingBit(RTC_IT_ALR);
	}
}

/*******************************************************************************
* Function Name  : dev_rtc_setAlarm
* Description    : 设置RTC闹钟.
* Input          : 闹钟时间
* Output         : None
* Return         : None
*******************************************************************************/
void dev_rtc_setAlarm(u32 AlarmValue)
{
#if 0
	/* Clear the RTC SEC flag */
	RTC_ClearFlag(RTC_FLAG_SEC);
	/* Wait clear RTC flag sccess */
    while(RTC_GetFlagStatus(RTC_FLAG_SEC) == RESET) {
		/* Clear the RTC SEC flag */
		RTC_ClearFlag(RTC_FLAG_SEC);
	};
#endif
	rtc_alarm_set(RTC_GetCounter() + AlarmValue);
	sleep_ms = rtc_get_msec();
}

static void dev_iwdg_init(void)
{
#ifdef WATCH_DOG
	/* Enable write access to IWDG_PR and IWDG_RLR registers */
	IWDG_WriteAccessCmd(IWDG_WriteAccess_Enable);
	/* IWDG counter clock: 40KHz(LSI) / 256 = 0.15625 KHz */
    IWDG_SetPrescaler(IWDG_Prescaler_256);
	/* Set counter reload value to 1562 */
	IWDG_SetReload(3125);	// 20s
	/* Reload IWDG counter */
	IWDG_ReloadCounter();
	/* Enable IWDG (the LSI oscillator will be enabled by hardware) */
	IWDG_Enable();
#endif
}

#if 0
/*******************************************************************************
* Function Name  : dev_clk_restore
* Description    : Restore system clock after wake-up from STOP: enable HSE, PLL
*                  and select PLL as system clock source.
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
void dev_clk_restore(void)
{
	/* Enable HSE */
	RCC_HSEConfig(RCC_HSE_ON);

	u8 HSEStartUpStatus;
	/* Wait till HSE is ready */
	HSEStartUpStatus = RCC_WaitForHSEStartUp();

	if(HSEStartUpStatus == SUCCESS)
	{
		/* Enable PLL */ 
		RCC_PLLCmd(ENABLE);

		/* Wait till PLL is ready */
		while(RCC_GetFlagStatus(RCC_FLAG_PLLRDY) == RESET)
		{
		}

		/* Select PLL as system clock source */
		RCC_SYSCLKConfig(RCC_SYSCLKSource_PLLCLK);

		/* Wait till PLL is used as system clock source */
		while(RCC_GetSYSCLKSource() != 0x08)
		{
		}
	}
}

/**
  * @brief  Sets System clock frequency to 36MHz and configure HCLK, PCLK2 
  *         and PCLK1 prescalers. 
  * @note   This function should be used only after reset.
  * @param  None
  * @retval None
  */
static void sys_power_resume_clock_36(void)
{
	__IO uint32_t StartUpCounter = 0, HSEStatus = 0;
  
	/* SYSCLK, HCLK, PCLK2 and PCLK1 configuration ---------------------------*/    
	/* Enable HSE */    
	RCC->CR |= ((uint32_t)RCC_CR_HSEON);
 
	/* Wait till HSE is ready and if Time out is reached exit */
	do
	{
		HSEStatus = RCC->CR & RCC_CR_HSERDY;
		StartUpCounter++;  
	} while((HSEStatus == 0) && (StartUpCounter != HSE_STARTUP_TIMEOUT));

	if ((RCC->CR & RCC_CR_HSERDY) != RESET)
	{
		HSEStatus = (uint32_t)0x01;
	}
	else
	{
		HSEStatus = (uint32_t)0x00;
	}  

	if (HSEStatus == (uint32_t)0x01)
	{
		/* Enable Prefetch Buffer */
		FLASH->ACR |= FLASH_ACR_PRFTBE;

		/* Flash 1 wait state */
		FLASH->ACR &= (uint32_t)((uint32_t)~FLASH_ACR_LATENCY);
		FLASH->ACR |= (uint32_t)FLASH_ACR_LATENCY_1;    

		/* HCLK = SYSCLK */
		RCC->CFGR |= (uint32_t)RCC_CFGR_HPRE_DIV1;

		/* PCLK2 = HCLK */
		RCC->CFGR |= (uint32_t)RCC_CFGR_PPRE2_DIV1;

		/* PCLK1 = HCLK */
		RCC->CFGR |= (uint32_t)RCC_CFGR_PPRE1_DIV1;


		/*  PLL configuration: PLLCLK = (HSE / 2) * 9 = 36 MHz */
		RCC->CFGR &= (uint32_t)((uint32_t)~(RCC_CFGR_PLLSRC | RCC_CFGR_PLLXTPRE | RCC_CFGR_PLLMULL));
		RCC->CFGR |= (uint32_t)(RCC_CFGR_PLLSRC_HSE | RCC_CFGR_PLLXTPRE_HSE_Div2 | RCC_CFGR_PLLMULL9);

		/* Enable PLL */
		RCC->CR |= RCC_CR_PLLON;

		/* Wait till PLL is ready */
		while((RCC->CR & RCC_CR_PLLRDY) == 0)
		{
		}

		/* Select PLL as system clock source */
		RCC->CFGR &= (uint32_t)((uint32_t)~(RCC_CFGR_SW));
		RCC->CFGR |= (uint32_t)RCC_CFGR_SW_PLL;    

		/* Wait till PLL is used as system clock source */
		while ((RCC->CFGR & (uint32_t)RCC_CFGR_SWS) != (uint32_t)0x08)
		{
		}
	}
	else
	{
	} 
}
#endif

/*
 * 唤醒后系统时钟恢复
 */
void sys_clock_resume(void)
{
	SystemInit();
	RCC_config();
}

/*
 * 低功耗模式的GIPO引脚设置
 */
void GPIO_LowPower_Config(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;

	/* Configure all GPIO port pins in Analog input mode (trigger OFF) */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_All;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;

	/* all GPIOA except swclk14,swdio13,out1 */
	GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_4 | \
									GPIO_Pin_5 | GPIO_Pin_6 | GPIO_Pin_7 | GPIO_Pin_8 | \
									GPIO_Pin_9 | GPIO_Pin_10 | GPIO_Pin_12 | GPIO_Pin_15 | \
									GPIO_Pin_0;
	GPIO_Init(GPIOA, &GPIO_InitStructure);  

	/* All GPIOC except led beep osc(7,8,9,13,14,15) */
	GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_4|  GPIO_Pin_5 | \
									GPIO_Pin_6 | GPIO_Pin_7 | GPIO_Pin_8 | GPIO_Pin_9 | \
									GPIO_Pin_10 | GPIO_Pin_11 |  GPIO_Pin_12 |  GPIO_Pin_13;
	GPIO_Init(GPIOC, &GPIO_InitStructure);  

	/* all GPIOB except swo out2 */
	GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_4 | \
									GPIO_Pin_5 | GPIO_Pin_6 | GPIO_Pin_7 | GPIO_Pin_8 | \
									GPIO_Pin_9 | GPIO_Pin_10 | GPIO_Pin_11 |GPIO_Pin_13 | \
									GPIO_Pin_14 | GPIO_Pin_15;
	GPIO_Init(GPIOB, &GPIO_InitStructure);
}

/*
 * 关闭所有的外设
 */
void sys_stop_dev()
{	
	/* adc在停机的时候会耗电, 关掉 */
	ADC_Cmd(ADC1, DISABLE);
    return;
}

/*
 * 清除所有外部中断
 */
int sys_clear_intr()
{
	rtc_clear_flag();
	if (EXTI_GetITStatus(EXTI_Line2)) {
		// 休眠期间发生了中断
		enableGlobalInterrupts();
		return 1;
	}
	if (EXTI_GetITStatus(EXTI_Line12)) {
		// 休眠期间发生了中断
		enableGlobalInterrupts();
		return 1;
	}
	if (EXTI_GetITStatus(EXTI_Line9)) {
		// 休眠期间发生了中断
		enableGlobalInterrupts();
		return 1;
	}
	return 0;
}

/*
 * 系统进入休眠t秒
 */
int sys_sleep(u32 t)
{
	// 屏蔽中断
	disableGlobalInterrupts();
	// watch dog
	iwdg_reload();
	// 停止外设
	sys_stop_dev();
	// 清除所有exti和rtc alarm中断
	if (sys_clear_intr()) {
		// 发生中断，不休眠
		debug("sys intr occured... no sleep\n");
		return 1;
	}
	// 设置RTC闹钟，wkuptime产生一次RTC闹钟中断*/
	dev_rtc_setAlarm(t);
	// 进入停止模式（低功耗），直至外部中断触发时被唤醒
	PWR_EnterSTOPMode(PWR_Regulator_LowPower, PWR_STOPEntry_WFI);
	iwdg_reload();
	return 0;
}

/*
 * 唤醒后的外设初始化
 */
void sys_wakeup_devconfig(void)
{
	long long current_ms = rtc_get_msec();
	jiffies += current_ms - sleep_ms;         //加入休眠时间
	// 延时函数初始化
	delay_config();
	/* 使能全局中断 */
	enableGlobalInterrupts();     //唤醒后开启全局中断
}
void sys_sleep_scan(void)
{
	led_off(0);
	if (sys_sleep(WAKEUP_TIME)) {
		led_on(0);
		return;
	}
	sys_clock_resume();                                 //唤醒后时钟恢复
	sys_wakeup_devconfig();                             //唤醒后外设恢复
	debug("wakeup jiffies = %d\n", jiffies);
	led_on(0);
}

#include "uart.h"
#include "bc95.h"
#include "timer.h"

// rcc type
enum {
	RCC_APB1,
	RCC_APB2,
	RCC_AHB
};

// x=uart, y=gp, z=dma
#define RCC_TYPE(x,y,z)		(((x) & 3) | (((y) & 3) << 2) | (((z) & 3) << 4))
#define RCC_TYPE_UART(f)	((f) & 3)
#define RCC_TYPE_GPIO(f)	(((f) >> 2) & 3)
#define RCC_TYPE_DMA(f)		(((f) >> 4) & 3)

typedef struct _uart_dev {
	USART_TypeDef *uart;
	GPIO_TypeDef *gpio;
	uint16_t	txpin;
	uint16_t	rxpin;
	DMA_Channel_TypeDef *dma_tx;
	DMA_Channel_TypeDef *dma_rx;
	uint32_t	rcc_uart;
	uint32_t	rcc_gpio;
	uint32_t	rcc_dma;
	uint8_t		rcc_type;	// bit[0:1]uart,bit[2:3]gpio,bit[4:5]dma
	uint8_t		irq_uart;
	uint8_t		irq_txdma;
	uint8_t		irq_rxdma;
} uart_dev_t;

/**
 * 硬件接口1:
 * UART3_TX (PB10), UART3_RX (PB11)
 * UART3_DMA: TX -> CH2, RX -> CH3
 */
static const uart_dev_t uartdev = {
	USART3,				// uart dev
	GPIOB,				// gpio x
	GPIO_Pin_10,			// tx pin
	GPIO_Pin_11,			// rx pin
	DMA1_Channel2,		// dma tx
	DMA1_Channel3,		// dma rx
	RCC_APB1Periph_USART3,	// rcc uart
	RCC_APB2Periph_GPIOB,	// rcc gpio
	RCC_AHBPeriph_DMA1,	// rcc dma
	RCC_TYPE(RCC_APB1, RCC_APB2, RCC_AHB),
	USART3_IRQn,		// uart irq
	DMA1_Channel2_IRQn,	// tx dma irq
	DMA1_Channel3_IRQn,	// rx dma irq
};

extern bc95_info bc95_body;

/* wan rx/tx buffer, used by DMA */
static u8 dma_rxbuf[MAX_BUFSIZE];
static u8 dma_txbuf[MAX_BUFSIZE];

/** 中断函数注册 */
static void _nvic_init(FunctionalState state)
{
	const uart_dev_t *p = &uartdev;
	NVIC_InitTypeDef nvic;

	// config dma nvic tx
	nvic.NVIC_IRQChannel = p->irq_txdma;
	nvic.NVIC_IRQChannelPreemptionPriority = 3;
	nvic.NVIC_IRQChannelSubPriority = 0;
	nvic.NVIC_IRQChannelCmd = state;
	NVIC_Init(&nvic);
	
	// config dma nvic rx
	nvic.NVIC_IRQChannel = p->irq_rxdma;
	nvic.NVIC_IRQChannelPreemptionPriority = 0;
	nvic.NVIC_IRQChannelSubPriority = 0;
	nvic.NVIC_IRQChannelCmd = state;
	NVIC_Init(&nvic);

	// config uart2 nvic
	nvic.NVIC_IRQChannel = p->irq_uart;		//通道设置为串口2中断
	nvic.NVIC_IRQChannelPreemptionPriority = 0;	//中断占先等级0
	nvic.NVIC_IRQChannelSubPriority = 1;		//中断响应优先级0
	nvic.NVIC_IRQChannelCmd = state;			//打开中断
	NVIC_Init(&nvic);

	return;
}

/*
 * uart DMA发送函数
 */
static int _dma_tx(u8 *buf, u16 len)
{
	const uart_dev_t *p = &uartdev;
	DMA_InitTypeDef dma;

	// 外设地址
	dma.DMA_PeripheralBaseAddr = (u32)(&p->uart->DR);
	// 内存地址
	dma.DMA_MemoryBaseAddr = (u32)(buf);
	// 传输方向, 外设目的
	dma.DMA_DIR = DMA_DIR_PeripheralDST;
	// 缓冲区大小
	dma.DMA_BufferSize = len;
	// 外设地址递增模式
	dma.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	// 内存地址递增模式
	dma.DMA_MemoryInc = DMA_MemoryInc_Enable;
	// 外设数据宽度
	dma.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
	// 内存数据宽度
	dma.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
	// 传输模式
	dma.DMA_Mode = DMA_Mode_Normal;
	// 优先级
	dma.DMA_Priority = DMA_Priority_Medium;
	// no use memory to memory transfer
	dma.DMA_M2M = DMA_M2M_Disable;

	DMA_Init(p->dma_tx, &dma);

	// 设置传输完成中断
	DMA_ITConfig(p->dma_tx, DMA_IT_TC, ENABLE);
	
	// 设置当前数据长度，开始发送
	DMA_SetCurrDataCounter(p->dma_tx, len);
	DMA_Cmd(p->dma_tx, ENABLE);
	return 0;
}

/*
 * UART DMA RX start开始接收函数
 */
static void _dma_rx()
{
	const uart_dev_t *p = &uartdev;
	DMA_InitTypeDef dma;
	u16 len = sizeof(dma_rxbuf);

	// 外设地址
	dma.DMA_PeripheralBaseAddr = (u32)(&p->uart->DR);
	// 内存地址
	dma.DMA_MemoryBaseAddr = (u32)(dma_rxbuf);
	// 传输方向
	dma.DMA_DIR = DMA_DIR_PeripheralSRC;
	// 缓冲区大小
	dma.DMA_BufferSize = len;
	// 外设地址递增模式
	dma.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	// 内存地址递增模式
	dma.DMA_MemoryInc = DMA_MemoryInc_Enable;
	// 外设数据宽度
	dma.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
	// 内存数据宽度
	dma.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
	// 传输模式
	dma.DMA_Mode = DMA_Mode_Normal;
	// 优先级
	dma.DMA_Priority = DMA_Priority_VeryHigh; //DMA_Priority_Medium
	// no use memory to memory transfer
	dma.DMA_M2M = DMA_M2M_Disable;

	DMA_Init(p->dma_rx, &dma);

	// 设置传输完成中断
	DMA_ITConfig(p->dma_rx, DMA_IT_TC, ENABLE);		// complete

	// uart rx enable
	p->uart->CR1 |= USART_Mode_Rx;
	/* Uart Interrupt config, enable idle intr */
	USART_ITConfig(p->uart, USART_IT_IDLE, ENABLE);
	
	// 设置当前数据长度，开始接收
	DMA_SetCurrDataCounter(p->dma_rx, len);
	DMA_Cmd(p->dma_rx, ENABLE);

	return;
}

/*
 * UART 发送函数
 */
int uart3_tx(u8 *buf, u16 len)
{
	len = min(sizeof(dma_txbuf), len);
	
	memcpy(dma_txbuf, buf, len);
	
	_dma_tx(dma_txbuf, len);
	return 0;
}

/*
 * uart2 开启接收
 */
int uart3_rx()
{
	_dma_rx();
	return 0;
}

static __inline void _rcc_init(u8 type, uint32_t rcc, FunctionalState state)
{
	switch(type) {
	case RCC_APB1:
		RCC_APB1PeriphClockCmd(rcc, state);
		break;
	case RCC_APB2:
		RCC_APB2PeriphClockCmd(rcc, state);
		break;
	case RCC_AHB:
		RCC_AHBPeriphClockCmd(rcc, state);
		break;
	default:
		break;
	}
}

int dev_uart_init()
{
	const uart_dev_t *p = &uartdev;
	/* gpio config */
	GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;

	_nvic_init(ENABLE);

	/* Enable the GPIOs clocks */
	_rcc_init(RCC_TYPE_GPIO(p->rcc_type), p->rcc_gpio, ENABLE);
	/* Enable the UART1 clocks */
	_rcc_init(RCC_TYPE_UART(p->rcc_type), p->rcc_uart, ENABLE);
	/* Enable the DMA1 clocks */
	_rcc_init(RCC_TYPE_DMA(p->rcc_type), p->rcc_dma, ENABLE);

	/* Configure uart3_tx as alternate function push-pull */
	GPIO_InitStructure.GPIO_Pin = p->txpin;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;	// wjzhe, 10M to 2M
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_Init(p->gpio, &GPIO_InitStructure);
	
	/* Configure usat3_rx as input floating */
	GPIO_InitStructure.GPIO_Pin = p->rxpin;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(p->gpio, &GPIO_InitStructure);

	/* USART3 configured as follow:
		- BaudRate = 9600 baud
		- Word Length = 8 Bits
		- One Stop Bit
		- No parity
		- Hardware flow control disabled (RTS and CTS signals)
		- Receive and transmit enabled
	*/
	memset(&USART_InitStructure, 0, sizeof(USART_InitStructure));
	USART_InitStructure.USART_BaudRate = 9600;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No ;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Tx;// | USART_Mode_Rx;	// rx disable

	/* Configure the USART*/
	USART_Init(p->uart, &USART_InitStructure);

	// Deinitializes the DMA1 Channel
	DMA_DeInit(p->dma_tx);
	DMA_DeInit(p->dma_rx);

	// DMA方式发送数据
	USART_DMACmd(p->uart, USART_DMAReq_Tx, ENABLE);

	// DMA方式接收数据
	USART_DMACmd(p->uart, USART_DMAReq_Rx, ENABLE);

	// 开启dma rx
	//_dma_rx(devn);
	/* Enable the USART */
	USART_Cmd(p->uart, ENABLE);

//	/* 中断使能 */
//	USART_ITConfig(p->uart, USART_IT_PE, ENABLE);
//	USART_ITConfig(p->uart, USART_IT_FE, ENABLE);
//	USART_ITConfig(p->uart, USART_IT_NE, ENABLE);
//	USART_ITConfig(p->uart, USART_IT_ORE, ENABLE);
	return 0;
}


/**********************************************
 *
 * NOTE: 以下函数为固定,修改串口需要同时修改
 *
 *********************************************/

// 1发送完成中断
void DMA1_Channel2_IRQHandler(void)
{
	const uart_dev_t *p = &uartdev;
	DMA_ClearFlag(DMA1_FLAG_TC2);
	DMA_Cmd(p->dma_tx, DISABLE);
	USART_ITConfig(p->uart, USART_IT_TC, ENABLE);
	return;
}

// 1接收完成中断
void DMA1_Channel3_IRQHandler(void)
{
	const uart_dev_t *p = &uartdev;

	DMA_ClearFlag(DMA1_FLAG_TC3);
	DMA_Cmd(p->dma_rx, DISABLE);

	DMA_SetCurrDataCounter(p->dma_rx, sizeof(dma_rxbuf));
	DMA_Cmd(p->dma_rx, ENABLE);
	return;
}

// 1空闲接收
void USART3_IRQHandler(void)
{
	const uart_dev_t *p = &uartdev;
	char *pst;
	int recv_len;
	u8 ReadData;
	u32 sr = p->uart->SR;

	// 清空空闲标志位
	if (sr & USART_FLAG_IDLE) {
		// recv new data from dma
		DMA_Cmd(p->dma_rx, DISABLE);
		ReadData = p->uart->DR;
		recv_len = sizeof(dma_rxbuf) - p->dma_rx->CNDTR;     //获取接收字节长度
		memset(bc95_body.recv_buf, 0, sizeof(bc95_body.recv_buf));
		memcpy(bc95_body.recv_buf, dma_rxbuf, recv_len);
		bc95_body.recv_buf[recv_len] = '\0';
		debug("USART3_IRQHandler : recv %s\n", bc95_body.recv_buf);
		if ((pst = strstr((const char *)bc95_body.recv_buf, "+NSONMI:")) != NULL) {
			pst += strlen("+NSONMI:");
			bc95_body.recv_len = atoi(pst + 2);
			bc95_body.recv_data_flag = 1;
			debug("USART3_IRQHandler : recv udp data len : %d time : %ld\n", bc95_body.recv_len, jiffies);
		} else {
			bc95_body.recv_complete = 1;
		}
		// uart rx disable
	//	p->uart->CR1 &= ~USART_Mode_Rx;
		DMA_SetCurrDataCounter(p->dma_rx, sizeof(dma_rxbuf));
		DMA_Cmd(p->dma_rx, ENABLE);
	}
	// rx
	if (sr & USART_FLAG_RXNE) {
		/* can not reach here */
		ReadData = p->uart->DR;
		sr = p->uart->SR;
	}
	// over err
	if (sr & USART_FLAG_ORE) {
//		debugL(DBG_ERR, "UART2 ORE 0x%04x\n", sr);
		ReadData = p->uart->DR;
	}
	// NE
	if (sr & USART_FLAG_NE) {
//		debugL(DBG_ERR, "UART2 NE 0x%04x\n", sr);
		ReadData = p->uart->DR;
	}
	// FE
	if (sr & USART_FLAG_FE) {
//		debugL(DBG_ERR, "UART2 FE 0x%04x\n", sr);
		ReadData = p->uart->DR;
	}
	// PE
	if (sr & USART_FLAG_PE) {
//		debugL(DBG_ERR, "UART2 PE 0x%04x\n", sr);
		ReadData = p->uart->DR;
	}
	// send complete
	if (sr & USART_FLAG_TC) {
		// 不可以用sr = 0清TC，会出现TC中断不正常情况
		//p->uart->DR = 0;	// clear RXNE TC
		USART_ITConfig(p->uart, USART_IT_TC, DISABLE);
		//p->uart->CR1 &= ~USART_Mode_Tx;
	}
	return;
}



#include "system.h"
#include "switch.h"
#include "device.h"
#include "motor.h"
#include "beep.h"
#include "bc95.h"
#include "delay.h"
#include "timer.h"
#include "adxl345.h"
#include <math.h>

u8 left_sensor_flag = 0, right_sensor_flag = 0;
u8 adxl345_detect_flag = 0;
u8 send_flag = 0;
#define SENSOR_DELYA_TIME 30

void reset_water_detector(void)
{
	PDout(2) = 0;
	mdelay(10);
	PDout(2) = 1;
}

/*
 * 开关初始化，主要工作为初始化IO口
 */
void switch_init(void)
{
	GPIO_InitTypeDef  GPIO_InitStructure;
	NVIC_InitTypeDef   nvic;
	EXTI_InitTypeDef   exti;
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC,ENABLE);	//使能GPIOC时钟
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB,ENABLE);	//使能GPIOB时钟
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOD,ENABLE);	//使能GPIOB时钟
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;				//S1-->PC.5 端口配置
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPD; 		//浮空输入
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(GPIOB, &GPIO_InitStructure);					//根据设定参数初始化GPIOC.5
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12;				//S1-->PC.5 端口配置
	GPIO_Init(GPIOC, &GPIO_InitStructure);					//根据设定参数初始化GPIOC.5

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;				//S1-->PC.5 端口配置
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; 		//浮空输入
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(GPIOD, &GPIO_InitStructure);					//根据设定参数初始化GPIOC.5
	PDout(2) = 1;
	
	/* Enable SYSCFG clock */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
	/* Connect EXTI0 Line to GPO0-PB8 pin */
	GPIO_EXTILineConfig(GPIO_PortSourceGPIOB, GPIO_PinSource8);
	GPIO_EXTILineConfig(GPIO_PortSourceGPIOB, GPIO_PinSource9);
	GPIO_EXTILineConfig(GPIO_PortSourceGPIOC, GPIO_PinSource12);

	nvic.NVIC_IRQChannel = EXTI15_10_IRQn;
	nvic.NVIC_IRQChannelPreemptionPriority = 3;
	nvic.NVIC_IRQChannelSubPriority = 0;
	nvic.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&nvic);
	nvic.NVIC_IRQChannel = EXTI9_5_IRQn;
	nvic.NVIC_IRQChannelPreemptionPriority = 3;
	nvic.NVIC_IRQChannelSubPriority = 0;
	nvic.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&nvic);

	/* Configure EXTIx line, disable interrupt */
	exti.EXTI_Line = EXTI_Line9;
	exti.EXTI_Mode = EXTI_Mode_Interrupt;
	exti.EXTI_Trigger = EXTI_Trigger_Rising;  
	exti.EXTI_LineCmd = ENABLE;
	EXTI_Init(&exti);
	/* Configure EXTIx line, disable interrupt */
	exti.EXTI_Line = EXTI_Line12;
	exti.EXTI_Mode = EXTI_Mode_Interrupt;
	exti.EXTI_Trigger = EXTI_Trigger_Rising_Falling;  
	exti.EXTI_LineCmd = ENABLE;
	EXTI_Init(&exti);
}

void get_device_state(dev_info *dev)
{
	static u8 cnt = 0;
	static int prev_water_sta = 0;
	static int prev_hydrant_sta = 0;
	static vs32 alarm_time = 0;
	static vs32 adxl345_detect_time = 0;
	static vs32 water_detect_time = 0;
	int water_temp;
	/**********震动传感器测量方式********************
	if ((right_sensor_flag == 1) || (left_sensor_flag == 1)) {
		if ((right_sensor_flag == 1) && (left_sensor_flag == 1)) {
			//有情况发生置一
			send_flag = 1;
			dev->hydrant_sta = 1;
			cnt = 0;
			right_sensor_flag = 0;
			left_sensor_flag = 0;
			debug("get_device_state : hydrant happened\n");
			
			prev_hydrant_sta = 1;//记录这一周期的报警状态，等待下次报警解除
			alarm_time = jiffies;
			
		}
		else {
			if (t1s_flag_sensor) {
				t1s_flag_sensor = 0;
				cnt++;
			}
			if (cnt == SENSOR_DELYA_TIME) {
				cnt = 0;
				right_sensor_flag = 0;
				left_sensor_flag = 0;
				if (prev_hydrant_sta == 1) {
					prev_hydrant_sta = 0;
					send_flag = 1;//若检测到有一个传感器触发，且间隔SENSOR_DELYA_TIME后并未触发两个传感器，则认为解除报警，上传状态
					printf("get_device_state : hydrant cancel right %d, left %d\n", right_sensor_flag, left_sensor_flag);
				}
			}
		}
	} else {
		//此处不清零，上报完成后才清零
//		dev->hydrant_sta = 0;
		if (prev_hydrant_sta == 1) {
			if ((jiffies - alarm_time) >= (SENSOR_DELYA_TIME * 1000)) {
				prev_hydrant_sta = 0;
				send_flag = 1;//若检测到有一个传感器触发，且间隔SENSOR_DELYA_TIME后并未触发两个传感器，则认为解除报警，上传状态
				printf("get_device_state : hydrant cancel right %d, left %d\n", right_sensor_flag, left_sensor_flag);
			}
		}
	}
	********************************************/
	/**********加速度计测量方式**********/
#if 0
	//获取角度值
	if (adxl345_detect_flag) {
//		adxl345_detect_flag = 0;
		s16 data[6];
		data[0] = adxl345_get_int_source();
		debug("switch data %x\n", data[0]);
		if (adxl345_get_val(data)) {
			dev->real_ang = adxl345_calcule_ang(data);
			debug("get_device_state : detect ang %f\n", dev->real_ang);
			adxl345_detect_time = jiffies;
		}
	} else {
		//若没有中断，查两个值是否相等且值是否稳定，加速度计稳定之后重新赋值零点
		if ((dev->zero_ang != dev->real_ang)
			&& ((jiffies - adxl345_detect_time) >= (SENSOR_DELYA_TIME * 1000))) {
			dev->zero_ang = dev->real_ang;
			debug("get_device_state : hydrant zero chenge %f\n", dev->zero_ang);
		}
	}
#endif
	//查询是否有报警
	//震动
	dev->hydrant_sta = adxl345_detect_flag;
	if (dev->hydrant_sta != prev_hydrant_sta) {
		dev->hydrant_time = rtc_get_sec();
		//有情况发生置一
		send_flag = 1;
		debug("get_device_state : hydrant %s\n", (dev->hydrant_sta?"happend":"cancel"));
		prev_hydrant_sta = dev->hydrant_sta;
	}
	//用水
	/*
	dev->water_sta = S3_STA;
	if (dev->water_sta != prev_water_sta) {
		dev->water_time = rtc_get_sec();
		//有情况发生置一
		send_flag = 1;
		debug("get_device_state : water %s\n", (dev->water_sta?"happend":"cancel"));
		if (dev->water_sta) {
			if (abs(jiffies - alarm_time) < dev->water_sensor_init_threshold_time) {//规定时间内发生了多次用水检测事件，认为是用水检测模块故障，重新上电
				reset_water_detector();
				dev->water_sta = S3_STA;
				debug("get_device_state : water detector reset %s, time [%d %d]\n",
						(dev->water_sta?"happend":"cancel"), jiffies, alarm_time);
			} else {
				alarm_time = jiffies;//记录用水事件上升沿时间
			}
		}
		prev_water_sta = dev->water_sta;
	}*/
	water_temp = S3_STA;
	if (water_temp != prev_water_sta) {
		if (water_temp) {
			if (abs(jiffies - alarm_time) < dev->water_sensor_init_threshold_time) {//规定时间内发生了多次用水检测事件，认为是用水检测模块故障，重新上电
				reset_water_detector();
				water_temp = S3_STA;
				debug("get_device_state : water detector reset %s, time [%d %d]\n",
						(water_temp?"happend":"cancel"), jiffies, alarm_time);
			} else {
				alarm_time = jiffies;//记录用水事件上升沿时间
			}
		}
		prev_water_sta = water_temp;
		//记录改变的时间并置位标志位
		dev->water_sta_change_flag = 1;
		water_detect_time = jiffies;
		debug("get_device_state : water sensor change sta[%d], memary time %d\n", water_temp, water_detect_time);
	}
	//状态改变了，此时要计时，实现报警延迟功能，且状态稳定时间超出3S
	if ((dev->water_sta_change_flag) && (abs(jiffies - water_detect_time) >= 3000)) {
		if (water_temp != dev->water_sta) {
			dev->water_sta = water_temp;
			
			dev->water_time = rtc_get_sec();
			//有情况发生置一
			send_flag = 1;
			debug("get_device_state : water %s\n", (dev->water_sta?"happend":"cancel"));
		}
		debug("get_device_state : water sensor inactive, sta[%d]\n", dev->water_sta);
		dev->water_sta_change_flag = 0;
	}
	
	if (send_flag == 1) {
		//有报警需要马上发送时，当为空闲状态或等待状态时，马上转为发送状态
		//其他状态等待
		if ((dev->gprs_cmd == GPRS_CMD_NONE) || (dev->gprs_cmd == GPRS_CMD_WAIT)) {
			send_flag = 0;
			dev->gprs_cmd = GPRS_CMD_SEND;
			dev->cmd_type = CMD_STA_UPDATE;
			dev->is_retry = 0;
		}
	}
}

void EXTI15_10_IRQHandler(void)
{
	/* exti_line10 PC10 *///LED灯旁边的
	if (EXTI->PR & (1 << 10)) {
		right_sensor_flag = 1;
		/* clear */
		EXTI_ClearITPendingBit(EXTI_Line10);
	}
	/* exti_line11 PC11 *///测量用水的
	if (EXTI->PR & (1 << 11)) {
		/* clear */
		EXTI_ClearITPendingBit(EXTI_Line11);
	}
	/* exti_line12 PC12 *///芯片旁边的
	if (EXTI->PR & (1 << 15)) {
		left_sensor_flag = 1;
		/* clear */
		EXTI_ClearITPendingBit(EXTI_Line15);
	}
	/* exti_line11 PC11 *///测量用水的，液位传感器
	if (EXTI->PR & (1 << 12)) {
		/* clear */
		EXTI_ClearITPendingBit(EXTI_Line12);
	}
}

void EXTI9_5_IRQHandler(void)
{
	/* exti_line8 PB8 *///加速度计中断2
	if (EXTI->PR & (1 << 8)) {
		/* clear */
		//adxl345_detect_flag = 1;
		EXTI_ClearITPendingBit(EXTI_Line8);
	}
	/* exti_line9 PB9 *///加速度计中断1
	if (EXTI->PR & (1 << 9)) {
		/* clear */
		adxl345_intterrupt();
		EXTI_ClearITPendingBit(EXTI_Line9);
	}
//	s16 data[6];
//	data[0] = adxl345_get_int_source();
//	debug("INT data %x\n", data[0]);
//	adxl345_get_val(data);
//	data[0] = adxl345_get_int_source();
//	debug("INT data %x\n", data[0]);
}


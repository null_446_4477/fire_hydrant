/*
 * @JL,2013-09-12
 */
#include "system.h"
#include "rtc.h"
#include <time.h>
#include "uart_dbg.h"

/**
  * @brief  Configures the RTC peripheral and select the clock source.
  * 用于系统初始化
  * @param  None
  * @retval None
  */
void RTC_config(void)
{
	/* Enable PWR and BKP clocks */
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR | RCC_APB1Periph_BKP, ENABLE);
	/* Allow access to BKP Domain */
	PWR_BackupAccessCmd(ENABLE);
	if (BKP_ReadBackupRegister(BKP_DR1) != 0x32F3) {
		/* Reset Backup Domain */
		BKP_DeInit();
		/* Enable the LSE OSC */
		RCC_LSICmd(ENABLE);
		/* Wait till LSE is ready */  
		while (RCC_GetFlagStatus(RCC_FLAG_LSIRDY) == RESET);
		/* Select the RTC Clock Source */
		RCC_RTCCLKConfig(RCC_RTCCLKSource_LSI);
		/* Enable the RTC Clock */
		RCC_RTCCLKCmd(ENABLE);
		/* Wait until last write operation on RTC registers has finished */
		RTC_WaitForLastTask();
		/* Wait for RTC APB registers synchronisation */
		RTC_WaitForSynchro();
		/* Enable the RTC Second */
		//RTC_ITConfig(RTC_IT_SEC, ENABLE);
		RTC_EnterConfigMode();	// 允许配置
		/* Set RTC prescaler: set RTC period to 1sec */
		RTC_SetPrescaler(RTC_PERIOD); /* RTC period = RTCCLK/RTC_PR = (32.768 KHz)/(32767+1) */
		/* Wait until last write operation on RTC registers has finished */
		RTC_WaitForLastTask();
		RTC_ExitConfigMode();	//退出配置模式
		/* Indicator for the RTC configuration */
		BKP_WriteBackupRegister(BKP_DR1, 0x32F3);
	} else {
		/* Check if the Power On Reset flag is set */
		if (RCC_GetFlagStatus(RCC_FLAG_PORRST) != RESET) {
			debugL(DBG_WARN, "WARN: Power On Reset occurred....\n\r");
		}
		/* Check if the Pin Reset flag is set */
		else if (RCC_GetFlagStatus(RCC_FLAG_PINRST) != RESET) {
			debugL(DBG_WARN, "WARN: External Reset occurred....\n\r");
		}
		/* Wait for RTC registers synchronization */
		RTC_WaitForSynchro();
		/* Wait for RTC APB registers synchronisation */
		RTC_WaitForSynchro();
	}
	/* Clear reset flags */
	RCC_ClearFlag();
	debug("RCC config success\n");
}

/*
 * 配置rtc nvic -> exti 17 enable
 */
void rtc_nvic_config()
{
	EXTI_InitTypeDef EXTI_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
	
	/* Configure EXTI Line17(RTC Alarm) to generate an interrupt on rising edge */
    EXTI_ClearITPendingBit(EXTI_Line17);
    EXTI_InitStructure.EXTI_Line = EXTI_Line17;
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
    EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;
    EXTI_Init(&EXTI_InitStructure);

	/* Enable the RTC Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = RTCAlarm_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	RTC_ITConfig(RTC_IT_ALR, ENABLE);
}

//初始化闹钟          
//返回值:0,成功;其他:错误代码.
int rtc_alarm_set(u32 sec)
{
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR | RCC_APB1Periph_BKP, ENABLE);    //使能PWR和BKP外设时钟   
	PWR_BackupAccessCmd(ENABLE);    //使能后备寄存器访问  

    RTC_SetAlarm(sec);
    RTC_WaitForLastTask();
    return 0;        
}

/*
 * mktime64 - Converts date to seconds.
 * Converts Gregorian date to seconds since 1970-01-01 00:00:00.
 * Assumes input in normal date format, i.e. 1980-12-31 23:59:59
 * => year=1980, mon=12, day=31, hour=23, min=59, sec=59.
 *
 * [For the Julian calendar (which was used in Russia before 1917,
 * Britain & colonies before 1752, anywhere else before 1582,
 * and is still in use by some communities) leave out the
 * -year/100+year/400 terms, and add 10.]
 *
 * This algorithm was first published by Gauss (I think).
 */
time64_t mktime64(const unsigned int year0, const unsigned int mon0,
		const unsigned int day, const unsigned int hour,
		const unsigned int min, const unsigned int sec)
{
	unsigned int mon = mon0, year = year0;

	/* 1..12 -> 11,12,1..10 */
	if (0 >= (int) (mon -= 2)) {
		mon += 12;	/* Puts Feb last since it has leap day */
		year -= 1;
	}

	return ((((time64_t)
		  (year/4 - year/100 + year/400 + 367*mon/12 + day) +
		  year*365 - 719499
	    )*24 + hour /* now have hours */
	  )*60 + min /* now have minutes */
	)*60 + sec; /* finally seconds */
}

u32 rtc_get_msec()
{
	unsigned long long msec = RTC_GetCounter();
	msec *= 1000;
	msec += (RTC_PERIOD - RTC_GetDivider()) * 1000 / RTC_PERIOD;
	return msec;
}

u32 rtc_get_sec()
{
	return RTC_GetCounter();
}

void rtc_set_sec(u32 sec)
{
	RTC_SetCounter(sec);
}


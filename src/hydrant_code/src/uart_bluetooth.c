#include "uart_bluetooth.h"
#include "device.h"
#include "bc95.h"
#include "flash_if.h"
#include "timer.h"

static param_conf_info conf_msg;

#define UART_BUFMAX	0x3F
#define UART_BUFLEN	64
#define UART_BUFADD(X, L)		((X + L) & UART_BUFMAX)
#define UART_BUFSUB(X, L)		((X - L) & UART_BUFMAX)
struct uart_buf {
	u8 data[UART_BUFLEN];
	u8 head;
	u8 tail;
};

struct _uart_send_info {
	u8 data[128];
	int length;
	int cnt;
};

static struct _uart_send_info uart_send_buf;

static struct uart_buf uart_tx;
static struct uart_buf uart_rx;

// buf清空
static void _buf_clear(struct uart_buf *buf)
{
	buf->head = buf->tail = 0;
}

// buf有数据?,0=无
static int _buf_is_data(struct uart_buf *buf)
{
	return (buf->tail - buf->head);
}

// buf中数据长度
static u8 _buf_len(struct uart_buf *buf)
{
	u8 len = 0;
	if (buf->tail - buf->head > 0) {
		len = buf->tail - buf->head;
	}
	if (buf->tail - buf->head < 0) {
		len = UART_BUFLEN - buf->head + buf->tail;
	}
	return len;
}

// 向buf中存数据
static int _buf_set(struct uart_buf *buf, u8 *data, u8 len)
{
	int i;
	if (len > UART_BUFLEN) {
		return -1;
	}
	for (i = 0; i < len; i++) {
		buf->data[buf->tail] = data[i];
		buf->tail = UART_BUFADD(buf->tail, 1);
	}
	return 0;
}

// 从buf中提取数据
static int _buf_get(u8 *data, struct uart_buf *buf, u8 len)
{
	int i;
	if (len > UART_BUFLEN) {
		return -1;
	}
	for (i = 0; i < len; i++) {
		data[i] = buf->data[buf->head];
		buf->head = UART_BUFADD(buf->head, 1);
	}
	return 0;
}

// 从buf中回退N个数据
static int _buf_back(struct uart_buf *buf, u8 len)
{
	while (buf->tail != buf->head && len) {
		buf->tail = UART_BUFSUB(buf->tail, 1);
		len--;
	}
	return 0;
}

static int uart_enable_clk(void)
{
	/* RCC */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);
	return 0;
}

static int uart_gpio_cfg(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;

	/* Configure usat1_tx as alternate function push-pull */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;	// wjzhe, 10M to 2M
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	/* Configure usat1_rx as input floating */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	return 0;
}

int bluetooth_uart_init(void)
{
	USART_InitTypeDef USART_InitStructure;
	NVIC_InitTypeDef  NVIC_InitStructure;

	uart_enable_clk();
	uart_gpio_cfg();

	NVIC_InitStructure.NVIC_IRQChannel = USART2_IRQn;               //通道设置为串口1中断
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 3;       //中断占先等级0
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;              //中断响应优先级0
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;                 //打开中断
	NVIC_Init(&NVIC_InitStructure);                                 //初始化

	/* USART3 configured as follow:
	- BaudRate = 115200 baud
	- Word Length = 8 Bits
	- One Stop Bit
	- No parity
	- Hardware flow control disabled (RTS and CTS signals)
	- Receive and transmit enabled*/
	USART_InitStructure.USART_BaudRate = 115200;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode =  USART_Mode_Rx | USART_Mode_Tx;

	/* Configure the USART1 */
	USART_Init(USART2, &USART_InitStructure);

	/* Enable the USART Transmoit interrupt: this interrupt is generated when the
	USART1 transmit data register is empty */
	USART_ITConfig(USART2, USART_IT_TXE, DISABLE);

	/* Enable the USART Receive interrupt: this interrupt is generated when the
	USART1 receive data register is not empty */
	USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);

	/* Enable USART1 */
	USART_Cmd(USART2, ENABLE);
	memset(&conf_msg, 0, sizeof(conf_msg));
	return 0;
}

// tx
void uart2_tx(u8 *buf, u16 len)
{
	memset(&uart_send_buf, 0, sizeof(uart_send_buf));
	memcpy(uart_send_buf.data, buf, len);
	uart_send_buf.length = len;
	uart_send_buf.cnt = 0;
	USART_SendData(USART2, uart_send_buf.data[uart_send_buf.cnt]);
	uart_send_buf.cnt += 1;
	for ( ; uart_send_buf.cnt < uart_send_buf.length; uart_send_buf.cnt += 1) {
		while (!(USART2->SR & USART_FLAG_TC));
		USART_SendData(USART2, uart_send_buf.data[uart_send_buf.cnt]);
	}
	
//	USART_ITConfig(USART2, USART_IT_TXE, ENABLE);
}

// uart1 irq
void USART2_IRQHandler(void)
{
	u8 tmp;
	// tx
	if (USART_GetITStatus(USART2, USART_IT_TXE) == SET) {
		if (uart_send_buf.cnt < uart_send_buf.length) {
			USART_SendData(USART2, uart_send_buf.data[uart_send_buf.cnt]);
			uart_send_buf.cnt += 1;
		} else {
			USART_ITConfig(USART2, USART_IT_TXE, DISABLE);
		}/*
		if (_buf_is_data(&uart_tx)) {
			_buf_get(&tmp, &uart_tx, 1);
			USART_SendData(USART2, tmp);
		} else {
			USART_ITConfig(USART2, USART_IT_TXE, DISABLE);
		}*/
		//USART_ClearITPendingBit(USART2, USART_IT_TXE);
	}
	// rx
	if (USART_GetITStatus(USART2, USART_IT_RXNE) == SET) {
		u8 val = USART_ReceiveData(USART2);
		if ((conf_msg.recv_len == 0) && (conf_msg.recv_buf[0] == 0)) {//判断接收第一个字节
			conf_msg.recv_buf[conf_msg.recv_len] = val;
			conf_msg.recv_len = 1;
		} else {
			if ((conf_msg.recv_buf[0] + 1) < (conf_msg.recv_len + 1)) {//判断接收完成一包数据
				
			} else if ((conf_msg.recv_buf[0] + 1) == (conf_msg.recv_len + 1)) {
				conf_msg.recv_buf[conf_msg.recv_len] = val;
				conf_msg.recv_len += 1;

				conf_msg.recv_ready = 1;
				debug("conflen:%d\n", conf_msg.recv_len);
			} else {
				conf_msg.recv_buf[conf_msg.recv_len] = val;
				conf_msg.recv_len += 1;
			}
		}
		
		USART_ClearITPendingBit(USART2, USART_IT_RXNE);
	}
}

void param_conf_handler()
{
	memset(conf_msg.send_buf, 0, sizeof(conf_msg.send_buf));
	u8 *recv = conf_msg.recv_buf;
	u8 *send = conf_msg.send_buf;
	const char *delim = ".";	/* separator: ' ' */
	char *token;
	debug("param_conf_handler : config cmd %d length %d ", recv[1], recv[0]);
	for (int k = 0; k < recv[0]; k++) {
		debug(" %d", recv[2 + k]);
	}
	debug("\n");
	switch (recv[1]) {
	//get
		case PARAM_GET_CPU_ID:
			//判断命令长度
			if (recv[0] != 0x01) {
				break;
			}
			send[0] = sizeof(dev_state.id) + 1;//length
			send[1] = PARAM_GET_CPU_ID;//CMD
			memcpy(&send[2], dev_state.id, sizeof(dev_state.id));//data
			uart2_tx(send, 20);//send[0] + 1);
			break;
		case PARAM_GET_SERIAL_NUM:
			//判断命令长度
			if (recv[0] != 0x01) {
				break;
			}
			send[0] = sizeof(dev_state.serial_num) + 1;//length
			send[1] = PARAM_GET_SERIAL_NUM;//CMD
			memcpy(&send[2], dev_state.serial_num, sizeof(dev_state.serial_num));//data
			uart2_tx(send, 20);//send[0] + 1);
			break;
		case PARAM_GET_HOST_INFO:
			{
			//判断命令长度
			if (recv[0] != 0x01) {
				break;
			}
			u8 buf[10];
			char addr_buf[128];
			int j = 0;
			//ip
			strncpy(addr_buf, bc95_body.server_info.addr, sizeof(addr_buf));
			for (token = strtok(addr_buf, delim); token != NULL; token = strtok(NULL, delim)) {
				if (j >= 4) {//做个简单的内存保护
					break;
				}
				buf[j] = strtol(token, NULL, 0);
				j++;
			}
			//port
			buf[j] = (u8)bc95_body.server_info.port;j++;
			buf[j] = (u8)(bc95_body.server_info.port>>8);j++;
			//interval
			buf[j] = (u8)bc95_body.comm_interval;j++;
			buf[j] = (u8)(bc95_body.comm_interval>>8);j++;
			buf[j] = (u8)(bc95_body.comm_interval>>16);j++;
			buf[j] = (u8)(bc95_body.comm_interval>>24);j++;
			
			send[0] = sizeof(buf) + 1;//length
			send[1] = PARAM_GET_HOST_INFO;//CMD
			memcpy(&send[2], buf, sizeof(buf));//data
			uart2_tx(send, 20);//send[0] + 1);
			}
			break;
		case PARAM_GET_VERSION_NUM:
			//判断命令长度
			if (recv[0] != 0x01) {
				break;
			}
			send[0] = 2 + 1;//length
			send[1] = PARAM_GET_VERSION_NUM;//CMD
			send[2] = (u8)VERSION_NUM;
			send[3] = (u8)(VERSION_NUM>>8);
			uart2_tx(send, 20);//send[0] + 1);
			break;
		case PARAM_GET_DEV_STA:
			//判断命令长度
			if (recv[0] != 0x01) {
				break;
			}
			send[0] = 1 + 2 + 1;//length, 1btye status 2byte voltage
			send[1] = PARAM_GET_DEV_STA;//CMD
			send[2] = (dev_state.hydrant_sta << 1) + dev_state.water_sta;
			send[3] = (u8)dev_state.voltage;
			send[4] = (u8)(dev_state.voltage>>8);
			uart2_tx(send, 20);//send[0] + 1);
			break;
		case PARAM_GET_IMEI:
			//判断命令长度
			if (recv[0] != 0x01) {
				break;
			}
			send[0] = sizeof(bc95_body.client_info.imei) + 1;//length
			send[1] = PARAM_GET_IMEI;//CMD
			memcpy(&send[2], bc95_body.client_info.imei, sizeof(bc95_body.client_info.imei));//data
			uart2_tx(send, 20);//send[0] + 1);
			break;
		case PARAM_GET_IMSI:
			//判断命令长度
			if (recv[0] != 0x01) {
				break;
			}
			send[0] = sizeof(bc95_body.client_info.imsi) + 1;//length
			send[1] = PARAM_GET_IMSI;//CMD
			memcpy(&send[2], bc95_body.client_info.imsi, sizeof(bc95_body.client_info.imsi));//data
			uart2_tx(send, 20);//send[0] + 1);
			break;
		case PARAM_GET_RUN_MODE:
			//判断命令长度
			if (recv[0] != 0x01) {
				break;
			}
			send[0] = 1 + 1;//length
			send[1] = PARAM_GET_RUN_MODE;//CMD
			send[2] = dev_state.run_mode;
			uart2_tx(send, 20);//send[0] + 1);
			break;
		case PARAM_GET_WT_INIT_OFFSET:
			//判断命令长度
			if (recv[0] != 0x01) {
				break;
			}
			send[0] = 1 + 4;//length
			send[1] = PARAM_GET_WT_INIT_OFFSET;//CMD
			send[2] = (u8)dev_state.water_sensor_init_threshold_time;
			send[3] = (u8)(dev_state.water_sensor_init_threshold_time>>8);
			send[4] = (u8)(dev_state.water_sensor_init_threshold_time>>16);
			send[5] = (u8)(dev_state.water_sensor_init_threshold_time>>24);
			uart2_tx(send, 20);//send[0] + 1);
			break;
	//set
		case PARAM_SET_SERIAL_NUM:
			//判断命令长度
			if (recv[0] != 0x05) {
				send[0] = 1 + 1;//length
				send[1] = PARAM_SET_SERIAL_NUM;//CMD
				send[2] = 0;//failed
			} else {
				memcpy(dev_state.serial_num, &recv[2], sizeof(dev_state.serial_num));
				//写入FLASH
				u16 _buf[2];
				_buf[0] = recv[2] + (recv[3]<<8);
				_buf[1] = recv[4] + (recv[5]<<8);
				FLASH_Write(FLASH_PARAM_ADDR + PARAM_SERIAL_NUM_OFFSET, _buf, 2);
				
				send[0] = 1 + 1;//length
				send[1] = PARAM_SET_SERIAL_NUM;//CMD
				send[2] = 1;//success
			}
			uart2_tx(send, 20);//send[0] + 1);
			break;
		case PARAM_SET_HOST_INFO:
			//判断命令长度
			if (recv[0] != 0x0B) {
				send[0] = 1 + 1;//length
				send[1] = PARAM_SET_HOST_INFO;//CMD
				send[2] = 0;//failed
			} else {
				char buf[128];
				char addr_buf[128];
				memset(addr_buf, 0, sizeof(addr_buf));
				for (int i = 0; i < 4; i++) {
					memset(buf, 0, sizeof(buf));
					sprintf(buf, "%d", recv[2 + i]);
					strcat(addr_buf, buf);
					if (i < 3) {
						strcat(addr_buf, ".");
					}
				}
				
				strncpy(bc95_body.server_info.addr, addr_buf, sizeof(bc95_body.server_info.addr));
				//写入FLASH
				u16 _buf[2];
				_buf[0] = recv[2] + (recv[3]<<8);
				_buf[1] = recv[4] + (recv[5]<<8);
				FLASH_Write(FLASH_PARAM_ADDR + PARAM_HOST_IP_OFFSET, _buf, 2);
				
				bc95_body.server_info.port = recv[6] + (recv[7]<<8);
				//写入FLASH
				_buf[0] = recv[6] + (recv[7]<<8);
				FLASH_Write(FLASH_PARAM_ADDR + PARAM_HOST_PORT_OFFSET, _buf, 1);
				
				bc95_body.comm_interval = recv[8] + (recv[9]<<8) + (recv[10]<<16) + (recv[11]<<24);
				//写入FLASH
				_buf[0] = recv[8] + (recv[9]<<8);
				_buf[1] = recv[10] + (recv[11]<<8);
				FLASH_Write(FLASH_PARAM_ADDR + PARAM_COMM_INTERVAL_OFFSET, _buf, 2);
				
				send[0] = 1 + 1;//length
				send[1] = PARAM_SET_HOST_INFO;//CMD
				send[2] = 1;//success
			}
			uart2_tx(send, 20);//send[0] + 1);
			break;
		case PARAM_SET_RUN_MODE:
			//判断命令长度
			if (recv[0] != 0x02) {
				send[0] = 1 + 1;//length
				send[1] = PARAM_SET_RUN_MODE;//CMD
				send[2] = 0;//failed
			} else {
				dev_state.run_mode = recv[2];
				//写入FLASH
				u16 _buf[2];
				_buf[0] = recv[2];
				FLASH_Write(FLASH_PARAM_ADDR + PARAM_RUN_MODE_OFFSET, _buf, 1);
				
				send[0] = 1 + 1;//length
				send[1] = PARAM_SET_RUN_MODE;//CMD
				send[2] = 1;//success
			}
			uart2_tx(send, 20);//send[0] + 1);
			break;
		case PARAM_SET_WT_INIT_OFFSET:
			//判断命令长度
			if (recv[0] != 0x02) {
				send[0] = 1 + 1;//length
				send[1] = PARAM_SET_WT_INIT_OFFSET;//CMD
				send[2] = 0;//failed
			} else {
				dev_state.water_sensor_init_threshold_time = recv[2] + (recv[3]<<8) + (recv[4]<<16) + (recv[5]<<24);
				//写入FLASH
				u16 _buf[2];
				_buf[0] = recv[2] + (recv[3]<<8);
				_buf[1] = recv[4] + (recv[5]<<8);
				FLASH_Write(FLASH_PARAM_ADDR + PARAM_WT_SENSOR_INIT_OFFSET, _buf, 2);
				
				send[0] = 1 + 1;//length
				send[1] = PARAM_SET_WT_INIT_OFFSET;//CMD
				send[2] = 1;//success
			}
			uart2_tx(send, 20);//send[0] + 1);
			break;
		default:
			break;
	}
}

int conf_get_ready(void)
{
	return conf_msg.conf_flag;
}

int conf_scan()
{
	if (!conf_msg.conf_flag) {//配参流程中
		return 0;
	}
	
	if (conf_msg.open_uart_flag) {
		debug("conf_scan : MCU wake up\n");
		conf_msg.open_uart_flag = 0;
		//开始配参，打开时钟和串口
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);
	}
	if (conf_msg.recv_ready) {
		debug("conf_scan : recv pack\n");
		param_conf_handler();
		memset(&conf_msg, 0, sizeof(conf_msg));
		//完成配参，关闭时钟和串口
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, DISABLE);
	} else {
		if ((jiffies - conf_msg.start_tick) >= 1000) {//超时未接收
			debug("conf_scan : recv timeout cancel config\n");
			memset(&conf_msg, 0, sizeof(conf_msg));
			//完成配参，关闭时钟和串口
			RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, DISABLE);
		}
	}
	return 0;
}

//设置外部中断唤醒
/*
 * 唤醒脚初始化，初始化为外部中断IO口
 */
void bluetooth_exitirq_init()
{
	GPIO_InitTypeDef  GPIO_InitStructure;
	NVIC_InitTypeDef   nvic;
	EXTI_InitTypeDef   exti;
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA,ENABLE);	//使能GPIOC时钟
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1;				//S1-->PC.5 端口配置
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPD; 		//浮空输入
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(GPIOA, &GPIO_InitStructure);					//根据设定参数初始化GPIOA.1
	
	/* Enable SYSCFG clock */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
	/* Connect EXTI0 Line to GPO0-PB8 pin */
	GPIO_EXTILineConfig(GPIO_PortSourceGPIOA, GPIO_PinSource1);

	nvic.NVIC_IRQChannel = EXTI1_IRQn;
	nvic.NVIC_IRQChannelPreemptionPriority = 3;
	nvic.NVIC_IRQChannelSubPriority = 0;
	nvic.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&nvic);

	/* Configure EXTIx line, disable interrupt */
	exti.EXTI_Line = EXTI_Line1;
	exti.EXTI_Mode = EXTI_Mode_Interrupt;
	exti.EXTI_Trigger = EXTI_Trigger_Rising;  
	exti.EXTI_LineCmd = ENABLE;
	EXTI_Init(&exti);
}

void bluetooth_exitirq_close()
{
	EXTI_InitTypeDef   exti;
	/* Configure EXTIx line, disable interrupt */
	exti.EXTI_Line = EXTI_Line1;
	exti.EXTI_Mode = EXTI_Mode_Interrupt;
	exti.EXTI_Trigger = EXTI_Trigger_Rising;
	exti.EXTI_LineCmd = DISABLE;
	EXTI_Init(&exti);
}

void EXTI1_IRQHandler(void)
{
	/* exti_line2 PA2 *///
	if (EXTI->PR & (1 << 1)) {
		/* clear */
		EXTI_ClearITPendingBit(EXTI_Line1);
		u32 sr = USART2->SR;
		memset(&conf_msg, 0, sizeof(conf_msg));
		conf_msg.conf_flag = 1;
		conf_msg.open_uart_flag = 1;
		conf_msg.start_tick = jiffies;
	}
}

#include "adxl345.h"
#include "i2c.h"
#include <math.h>
#include "timer.h"
#include "device.h"
#include "delay.h"
#include "system.h"
#include "switch.h"

void io_init(void)
{
	//int i;
	GPIO_InitTypeDef GPIO_InitStructure;
	
	/* Enable the GPIOs clocks */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
	
	/* Configure PA in output pushpull mode */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_5;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;	
	GPIO_Init(GPIOB, &GPIO_InitStructure);

	GPIO_SetBits(GPIOB, GPIO_Pin_5);
}
//写ADXL345寄存器
//addr:寄存器地址
//val:要写入的值
//返回值:无
void ADXL345_WR_Reg(u8 reg, u8 val)
{
	struct i2c_gpio_data *i2c = &i2cgpio[0];
	u8 data[10];
	data[0] = reg;
	data[1] = val;
	i2c_reg_write(i2c, ADXL_ADDR, data, 2);
}

//读ADXL345寄存器
//addr:寄存器地址
//返回值:读到的值
int ADXL345_RD_Reg(u8 reg, u8 *data, u16 n)
{
	struct i2c_gpio_data *i2c = &i2cgpio[0];
	i2c_reg_read(i2c, ADXL_ADDR, reg, data, n);
	return 0;
} 

int adxl345_init()
{
	u8 dev_id;
	io_init();
	mdelay(100);
	ADXL345_WR_Reg(INT_ENABLE,0x00);	//关中断
	ADXL345_RD_Reg(DEVICE_ID, &dev_id, 1);
	if(dev_id == 0XE5)	//读取器件ID
	{
		// set ofs 0
		ADXL345_WR_Reg(OFSX, 0x00);
		ADXL345_WR_Reg(OFSY, 0x00);
		ADXL345_WR_Reg(OFSZ, 0x00);

		// set part into standby mode
		ADXL345_WR_Reg(POWER_CTL, 0x0);

		// 高电平中断输出,13位全分辨率,输出数据右对齐,16g量程 
		ADXL345_WR_Reg(DATA_FORMAT, 0x0B);

		ADXL345_WR_Reg(BW_RATE,0x0A);		//数据输出速度为100Hz

		// set active thresh 0x8 * 62.5mg
		ADXL345_WR_Reg(THRESH_ACT, 0x06);	//睡眠激活activity阈值，当大于这个值的时候唤醒，其中08代表0.5g
		// set inactive thresh 0x3 * 62.5mg
		ADXL345_WR_Reg(THRESH_INACT, 0x02);	//睡眠开始inactivity阈值，当小于这个值的时候睡眠，其中02代表0.2g
		// set inactive time 2s
		ADXL345_WR_Reg(TIME_INACT, 0x02);	//当小于inactivity值时间超过这个值的时候进入睡眠，其中02代表2秒

		// set act inact ctl, dc act xyz, ac inact xyz
	 	ADXL345_WR_Reg(ACT_INACT_CTL, 0xFF);		//直流交流触发配置，XYZ使能触发配置，此处选用X交流触发
		
		ADXL345_WR_Reg(INT_MAP, 0x00);		//中断引脚选择，此处将activity映射到INT1
		// int enable act & inact
		ADXL345_WR_Reg(INT_ENABLE, 0x18);	//打开中断

		// read int soure reg
		//ADXL345_RD_Reg(INT_SOURCE, &dev_id, 1);
		//debug("init isr %02x\n", dev_id);

		// mesure & auto sleep
		ADXL345_WR_Reg(POWER_CTL, 0x38);	   	//自动休眠模式，休眠时以8Hz的频率采样
		return 0;
	} else {
		debug("adxl345_init : failed\n");
	}
	return -1;
}

int adxl345_check_data(s16 *data)
{
	double x = data[0] * 0.004, y = data[1] * 0.004, z = data[2] * 0.004;
	double val = x * x + y * y + z * z;
	if (fabs(val - 1) > 0.4) {
		debug("adxl345_check_data : failed\n");
		return 0;
	}
	return 1;
}

int adxl345_get_val(s16 *data)
{
	u8 buf[6];
	ADXL345_RD_Reg(DATA_X0, buf, 6);
	data[0] = (s16)(((u16)buf[1]<<8)+buf[0]);
	data[1] = (s16)(((u16)buf[3]<<8)+buf[2]);
	data[2] = (s16)(((u16)buf[5]<<8)+buf[4]);
	return adxl345_check_data(data);
}

//获取中断源，也可以清中断
u8 adxl345_get_int_source(void)
{
	u8 data = 0;
	ADXL345_RD_Reg(INT_SOURCE, &data, 1);
	return data;
}
//得到角度
//x,y,z:x,y,z方向的重力加速度分量(不需要单位,直接数值即可)
//dir:要获得的角度.0,与Z轴的角度;1,与X轴的角度;2,与Y轴的角度.
//返回值:角度值.单位0.1°.
double adxl345_calcule_ang(s16 *data)
{
	double x = data[0] * 0.004, y = data[1] * 0.004, z = data[2] * 0.004;
	double temp = y / sqrt(x * x + z * z);
 	double ang = atan(temp);
	return rad2deg(ang);
}

int adxl345_calibration()
{
	double prev_ang = 0, ang;
	s16 data[3];
	vs32 prev_time = jiffies;
	while (1) {
		if (adxl345_get_val(data)) {
			ang = adxl345_calcule_ang(data);
			if (fabs(ang - prev_ang) > 2) {
				prev_time = jiffies;
				prev_ang = ang;
			}
			if ((jiffies - prev_time) >= (CALIBRATION_TIME * 1000)) {
				dev_state.zero_ang = dev_state.real_ang = ang;
				break;
			}
		} else {//得到的值校验不符合静止状态
			prev_time = jiffies;
		}
		mdelay(100);
	}
	return 0;
}

int adxl345_intterrupt(void)
{
	u8 isr = 0;
	// read int source reg
	ADXL345_RD_Reg(INT_SOURCE, &isr, 1);
	debug("isr = %02x\n", isr);
	if (isr & (1 << 4)) {
		debug("activity...\n");
		adxl345_detect_flag = 1;
	}
	if (isr & (1 << 3)) {
		debug("inactivity...\n");
		adxl345_detect_flag = 0;
	}
	return 0;
}

/*******************************************************
*芯片内部和外设初始化文件
*包含：
*初始化状态：
*
*
*
*
*
********************************************************/

#include "chipinit.h"
#include "system.h"
#include "delay.h"
#include "uart_dbg.h"
#include "timer.h"
#include "rtc.h"
#include "adc.h"
#include "motor.h"
#include "uart.h"
#include "uart_bluetooth.h"
#include "bc95.h"
#include "switch.h"
#include "beep.h"
#include "led.h"
#include "i2c.h"
#include "adxl345.h"
#include "flash_if.h"

void RCC_config(void)//时钟配置
{
	RCC_HCLKConfig(RCC_SYSCLK_Div1);	// 8Mhz
	RCC_PCLK1Config(RCC_HCLK_Div1);		// 8Mhz
	RCC_PCLK2Config(RCC_HCLK_Div1);		// 8Mhz
}

void onchipinit(void)
{
	//中断初始化
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_3);
	//各时钟分频
	RCC_config();
	//调试输出串口初始化
	dbg_uart_init();
	//ms级计时器初始化
	timer_1ms_config();
	//延时函数初始化
	delay_config();
	//读取FLASH参数信息
	FLASH_check();
	//信号开关引脚初始化
	switch_init();
	//ADC初始化
	ADC_config();
    //IIC初始化
    i2c_init();
	//加速度计初始化
	adxl345_init();
	//LED灯初始化
	led_init();
	led_off(0x0);
	led_off(0x1);
	//GPRS模块串口初始化
	dev_uart_init();
	//蓝牙串口初始化
	bluetooth_uart_init();
	//外部中断唤醒初始化
	bluetooth_exitirq_init();
	//GPRS模块初始化
	dev_state.gprs_conn_sta = bc95_init();
	//RTC初始化
	RTC_config();
	rtc_nvic_config();
	// 看门狗初始化
//	iwdg_init();	
	/* 使能全局中断 */
	enableGlobalInterrupts();
	// 开启看门狗
//	iwdg_start();
	debug("verson : %d\n", VERSION_NUM);
}

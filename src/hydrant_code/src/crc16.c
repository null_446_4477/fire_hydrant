#include "crc16.h"


unsigned short crc16_update(unsigned short crc, unsigned char a)//在函数usCRC16内部调用
{
	int i;
	crc^=a;
	for(i=0;i<8;++i)
	{
	    if(crc&1)
		crc=(crc>>1)^0xA001;
	    else
		crc=(crc>>1);
	}
	return crc;
}
unsigned short usCRC16(unsigned char *pucFrame,unsigned int usLen)//纯计算法CRC，参数为数据首地址和数据字节长度
{
	unsigned int crc_temp=0xffff;
	unsigned int x;
	while(usLen)
	{
		crc_temp=crc16_update(crc_temp,*pucFrame++);
		--usLen;
	}
	x = crc_temp>>8;//处理CRC数据，调换高低位
	crc_temp = crc_temp<<8;
	crc_temp = crc_temp+x;
	return crc_temp;//注意：CRC值高低位已调换，高位对应高位，低位对应低位
}

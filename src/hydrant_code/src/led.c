#include "system.h"
#include "stm32f10x_conf.h"
#include "led.h"

/* if修改:RCC单独查看是否打开,初始化配置 */
/* LED管脚定义 */
const u16 led_GPIOp[LED_NUM] = {GPIO_Pin_6, GPIO_Pin_7};
static GPIO_TypeDef *led_GPIOx[LED_NUM] = {GPIOC, GPIOC};

/*----------------------------------------------------------------------------
  initialize LED Pins
 *----------------------------------------------------------------------------*/
void led_init(void)
{
	//int i;
	GPIO_InitTypeDef GPIO_InitStructure;
	
	/* Enable the GPIOs clocks */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);
	
	/* Configure PA in output pushpull mode */
	GPIO_InitStructure.GPIO_Pin = led_GPIOp[0];
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;	
	GPIO_Init(led_GPIOx[0], &GPIO_InitStructure);
	/* Configure PA in output pushpull mode */
	GPIO_InitStructure.GPIO_Pin = led_GPIOp[1];
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;	
	GPIO_Init(led_GPIOx[1], &GPIO_InitStructure);

	led_on(0x0);
	led_on(0x1);
}

/*----------------------------------------------------------------------------
  Function that turns on requested LED
 *----------------------------------------------------------------------------*/
void led_on(u32 num)
{
	if (num < LED_NUM) {
		led_GPIOx[num]->BRR = led_GPIOp[num];
	}
}

/*----------------------------------------------------------------------------
  Function that turns off requested LED
 *----------------------------------------------------------------------------*/
void led_off(u32 num)
{
	if (num < LED_NUM) {
		led_GPIOx[num]->BSRR = led_GPIOp[num];
	}
}

/*----------------------------------------------------------------------------
  Function that outputs value to LEDs
 *----------------------------------------------------------------------------*/
void led_out(u32 value)
{
	int i;

	for (i = 0; i < LED_NUM; i++) {
		if (value & (1 << i)) {
			led_on(i);
		} else {
			led_off(i);
		}
	}
}

// 返回LED [?]当前状态, 返回1 on, 返回0 off
int led_status(u32 num)
{
	return !(led_GPIOx[num]->ODR & led_GPIOp[num]);
}


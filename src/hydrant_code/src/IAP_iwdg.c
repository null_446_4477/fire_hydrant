/* Includes ------------------------------------------------------------------*/
#include "stm32f10x_iwdg.h"

#pragma arm section code = "RAMCODE"
/* KR register bit mask */
#define KR_KEY_Reload    ((uint16_t)0xAAAA)
#define KR_KEY_Enable    ((uint16_t)0xCCCC)

void IAP_IWDG_WriteAccessCmd(uint16_t IWDG_WriteAccess)
{
  IWDG->KR = IWDG_WriteAccess;
}
void IAP_IWDG_SetPrescaler(uint8_t IWDG_Prescaler)
{
  IWDG->PR = IWDG_Prescaler;
}
void IAP_IWDG_SetReload(uint16_t Reload)
{
  IWDG->RLR = Reload;
}
void IAP_IWDG_ReloadCounter(void)
{
  IWDG->KR = KR_KEY_Reload;
}
void IAP_IWDG_Enable(void)
{
  IWDG->KR = KR_KEY_Enable;
}

void IAP_reset(void)
{
	IAP_IWDG_WriteAccessCmd(IWDG_WriteAccess_Enable);/* 写入0X5555，用于允许狗狗寄存器写入功能 */
	IAP_IWDG_SetPrescaler(IWDG_Prescaler_256);		 /* 狗时钟分频，40K/256=156HZ(6.4ms)*/
	IAP_IWDG_SetReload(1);        /* 喂狗时间100ms/6.4MS=16 注意不能大于0xfff*/
	IAP_IWDG_ReloadCounter();
	IAP_IWDG_Enable();
}

void IAP_iwdg_reload(void)
{
	IAP_IWDG_ReloadCounter();
	return;
}

#pragma arm section


#include "system.h"
#include "dataflash.h"
#include "spi.h"

#define BUF_LEN	(DATAFLASH_PAGEZISE + 4)

#define FADDR_REC_N 526
#define FADDR_REC_SUM 527
#define FADDR_REC_M 524

/* flash读写缓冲区 */
static u8 flash_buf_tx[BUF_LEN];
static u8 flash_buf_rx[BUF_LEN];
static void data_flash_set_cs(u8 cs, int pol);

static const struct spi_master dataflash_spi = {
	1,
	1,
	data_flash_set_cs,
	SPI2
};

static const struct spi_dev spi2_devs[] = {
	{
		0,
		GPIO_Pin_12,
		GPIOB
	}
};

/*********************** JL,2013 ****************************/
enum {
	AT45_RD = 0x03,	// Continuous Array Read (Low Frequency)
	AT45_WD = 0x82,	// Main Memory Page Program Through Buffer 1
	AT45_ERASE = 0x81,	// Page Erase
	AT45_FLASH_STATUS = 0xD7	// BUZY
};

//0 flash空闲 -1 写忙
static int _is_busy(void)
{
	u8 tx[2] = {0};
	u8 rx[2] = {0};
	tx[0] = AT45_FLASH_STATUS;
	spi_dma_txrx_selftry(&dataflash_spi, spi2_devs, tx, rx, 2);
	if((rx[1] & 0x80) == 0) {
		return -1;
	}
	return 0;
}
static int _wait_busy(void)
{
	while(_is_busy());
	return 0;
}
/*
 * 从FLASH中读取数据,不超过一页
 * page :页数0-4095; base :页内偏移0-641
 * addr = page << 9 + base, FLASH_ADDR(P, B)
 * -1表示错误忙
 */
int dataflash_read(u16 page, u16 base, u8 *buf, u16 len)
{
	u8 *tx = flash_buf_tx;
	u8 *rx = flash_buf_rx;
	u32 addr = FLASH_ADDR(page, base);

	_wait_busy();

	tx[0] = AT45_RD;
	tx[1] = addr >> 16;
	tx[2] = addr >> 8;
	tx[3] = addr;
	spi_dma_txrx_selftry(&dataflash_spi, spi2_devs, tx, rx, len + 4);
	memcpy(buf, rx + 4, len);
	return 0;
}
/*
 * 写入FLASH数据,不超过一页
 * page :页数0-4095; base :页内偏移0-641
 * addr = page << 9 + base, FLASH_ADDR(P, B)
 * -1表示错误忙
 */
int dataflash_write(u32 page, u16 base, u8 *buf, u32 len)
{
	u8 *tx = flash_buf_tx;
	u8 *rx = flash_buf_rx;
	u32 addr = FLASH_ADDR(page, base);
	
	_wait_busy();
	
	tx[0] = AT45_WD;
	tx[1] = addr >> 16;
	tx[2] = addr >> 8;
	tx[3] = addr;
	memcpy(tx + 4, buf, len);
	spi_dma_txrx_selftry(&dataflash_spi, spi2_devs, tx, rx, len + 4);	
	return 0;
}
/*
 * 页擦除
 * page :页数0-4095
 * -1表示错误忙
 */
int dataflash_earse(u16 page)
{
	u8 tx[4];
	u8 rx[4];
	page &= DATAFLASH_PAGE_MASK;

	_wait_busy();

	tx[0] = AT45_ERASE;
	tx[1] = page >> 6;
	tx[2] = page << 2;
	tx[3] = 0x00;
	spi_dma_txrx_selftry(&dataflash_spi, spi2_devs, tx, rx, 4);
	return 0;
}

/*
 * 读取flash page页, 要求p空间大小268
 * BUF_LEN
 */
int _flash_read_page(u8 *p, u16 page)
{
	u32 addr = FLASH_ADDR(page, 0);
	u8 *tx = flash_buf_tx;
	_wait_busy();
	tx[0] = AT45_RD;
	tx[1] = addr >> 16;
	tx[2] = addr >> 8;
	tx[3] = addr;
	spi_dma_txrx_selftry(&dataflash_spi, spi2_devs, tx, p, BUF_LEN);
	return 0;
}

/*
 * 写入flash page页, 要求p空间大小268
 * BUF_LEN
 */
int _flash_write_page(u8 *p, u16 page)
{
	u32 addr = FLASH_ADDR(page, 0);
	_wait_busy();
	p[0] = AT45_WD;
	p[1] = addr >> 16;
	p[2] = addr >> 8;
	p[3] = addr;
	spi_dma_txrx_selftry(&dataflash_spi, spi2_devs, p, flash_buf_rx, BUF_LEN);	
	return 0;
}

/********************* JL,2013 *****************************/

static void data_flash_set_cs(u8 cs, int pol)
{
	u16 pin;
	pin = spi2_devs[cs].pin;
	if (pol) {
		GPIO_ResetBits(spi2_devs[cs].gpio, pin);
	} else {
		GPIO_SetBits(spi2_devs[cs].gpio, pin);
	}
	return;
}

static void spi2_init(void)
{
	GPIO_InitTypeDef gpio;
	SPI_InitTypeDef spi;

	/* enable gpiob, spi2 clocks */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_SPI2, ENABLE);

	/* Enable the DMA1 clocks */
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);

	/* spi2 gpio config */
	gpio.GPIO_Pin = GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15;
	gpio.GPIO_Mode = GPIO_Mode_AF_PP;
	gpio.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOB, &gpio);

	/* SPI2 configuration */ 
	spi.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
	spi.SPI_Mode = SPI_Mode_Master;
	spi.SPI_DataSize = SPI_DataSize_8b;
	spi.SPI_CPOL = SPI_CPOL_Low;
	spi.SPI_CPHA = SPI_CPHA_1Edge;
	spi.SPI_NSS = SPI_NSS_Soft;
	spi.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_2;
	spi.SPI_FirstBit = SPI_FirstBit_MSB;
	spi.SPI_CRCPolynomial = 7;
	SPI_Init(SPI2, &spi);

	// Deinitializes the DMA1 Channel4 & 5, spi_rx & spi_tx
	DMA_DeInit(DMA1_Channel4);
	DMA_DeInit(DMA1_Channel5);

	/* spi dma config */
	SPI_I2S_DMACmd(SPI2, SPI_I2S_DMAReq_Tx, ENABLE);
	SPI_I2S_DMACmd(SPI2, SPI_I2S_DMAReq_Rx, ENABLE);

	/* Enable SPI2  */ 
	SPI_Cmd(SPI2, ENABLE);   //使能SPI2外设 
}

/*
 * dataflash PA4
 */
static void dataflash_cs_init(void)
{
	GPIO_InitTypeDef gpio;

	/* enable gpioc clocks */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
	
	gpio.GPIO_Pin = GPIO_Pin_12;
	gpio.GPIO_Mode = GPIO_Mode_Out_PP;
	gpio.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOB, &gpio);

	GPIO_SetBits(GPIOB, GPIO_Pin_12);
	return;
}

void dataflash_init(void)
{
	spi2_init();			// spi1
	dataflash_cs_init();	// cs -> gpio output
}


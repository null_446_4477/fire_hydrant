#include "system.h"
#include "bc95.h"
#include "uart.h"
#include "delay.h"
#include "device.h"
#include "timer.h"
#include "motor.h"
#include "crc16.h"
#include "switch.h"
#include "flash_if.h"
#include "updata.h"
#include "rtc.h"

#define FIND_R_N "\r\n"
#define RECV_LINE 30
static char *recv_cut[RECV_LINE];
static int recv_cut_n;

bc95_info bc95_body;
iap_comm_info iap;

int reboot_cnt = 0;

void reset_io_init(void)
{
	GPIO_InitTypeDef  GPIO_InitStructure;
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA,ENABLE);
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(GPIOC, &GPIO_InitStructure);
}
/*
 * 模块复位
 */
void bc95_reset(void)
{
	PCout(2) = 1;//驱动复位引脚低
	mdelay(500);
	PCout(2) = 0;//释放复位引脚，复位成功
}

/*
 * 查找收到bc95数据后用\r\n隔断的每一行
 */
char *find_recv_r_n(char *string)
{
	char *p;
	p = strstr(string, FIND_R_N);
	if (p) {
		*p = '\0';
		return string;
	}
	return NULL;
}
/*
 * 找到str中需要的字符串存入line中
 */
int find_line(char *line[], int n, char *str)
{
	int i;
	char *p = str;
	for (i = 0; i < n; i++) {
		line[i] = find_recv_r_n(p);
		if (line[i] == NULL) {
			break;
		}
		p = line[i] + strlen(line[i]) + 2;							 
	}
	return i;
}
/*
 * 查找收到bc95数据recv_cut里面的str
 */
char *bc95_recv_find(char *str)
{
	int i = 0;
	char *p = NULL;
	for (; i < recv_cut_n; i++) {
		p = strstr(recv_cut[i], str);
		if (p) {
			break;
		}
	}
	return p;
}
/*
 * 返回收到的bc95数据切割后的字符串
 */
char *bc95_get_str(int cnt)
{
	return recv_cut[cnt];
}
/*
 * 返回收到的bc95数据切割后的字符串数量
 */
int bc95_get_str_num(void)
{
	return recv_cut_n;
}

/*
 * BC95模块串口交互代码
 */
int bc95_comm(u8 *buf, u8 len, u16 timeout)
{
	int i = 0;
	int cnt;
	bc95_info *p = &bc95_body;
	p->recv_complete = 0;
	debug("\nbc95_comm : send %s\n", buf);
	uart3_tx(buf, len);
	uart3_rx();
	recv_cut_n = 0;
	timeout = timeout * 10;
	while (1) {
		if (p->recv_complete) {//若收到数据
			p->recv_complete = 0;
			debug("recv info : %s\n\n", p->recv_buf);
			char *q = (char *)p->recv_buf;
			recv_cut_n = cnt = find_line(recv_cut, RECV_LINE, q);
			for (i = 0; i < cnt; i++) {
			//	debug("recv_cut recv_cut : %s\n", recv_cut[i]);
				//收到的数据里有预期的字符串
				if (!strcasecmp(recv_cut[i], "OK")) {
					debug("bc95_comm find OK.\n");
					goto end;
				}
			}
			debug("recv info ERROR\n");
			return -1;
		} else {//判断超时
			if (i >= timeout) {
				debug("bc95_comm : timeout\n");
				return -1;
			}//10S
			i++;
			mdelay(100);
		}
	}
	end:
	return 0;
}
/*
 * 接收开机响应
 */
int bc95_turnon_response(void)
{
	int i = 0, x = 0;
	bc95_info *p = &bc95_body;
	mdelay(8000);
	return 0;
	uart3_rx();//开启接收
	while (1) {
		if (p->recv_complete) {//若收到数据则判断是否有开机响应字符串
			p->recv_complete = 0;
			x++;
			if (strstr((const char *)p->recv_buf, RESP_INIT) != NULL) {//开机成功
				//收到的数据里有预期的字符串
				break;
			} else {
				uart3_rx();//开启接收
				//若收到n次错误开机数据，则重启
				if (x >= 10) {
					x = 0;
					return -1;
				}
			}
		} else {//判断超时
			if (i >= 100) {
				return -1;
			}//30S
			i++;
			mdelay(100);
		}
	}
	return 0;
}
/*
 * 设置自动连接功能。mode=1为使能，=0为失能
 */
int bc95_cmd_autoconnect(u8 mode)
{
	char buf[MAX_BUFSIZE];
	int ret;
	sprintf(buf, "AT+NCONFIG=AUTOCONNECT,%s\r\n", mode?"TRUE":"FALSE");
	ret = bc95_comm((u8 *)buf, strlen(buf), 10);
	return ret;
}
/*
 * 查询自动连接功能
 */
int bc95_req_autoconnect(void)
{
	char buf[MAX_BUFSIZE], *pst;
	int ret;
	bc95_info *p = &bc95_body;
	sprintf(buf, "AT+NCONFIG?\r\n");
	ret = bc95_comm((u8 *)buf, strlen(buf), 1);
	//处理收到的自动连接数据
	if (ret >= 0) {
		if ((pst = bc95_recv_find("AUTOCONNECT,TRUE")) != NULL) {
			ret = 1;
		} else {
			ret = -1;
		}
	}
	return ret;
}
/*
 * 设置IMEI
 */
int bc95_cmd_setimei(u32 *imei)
{
	char buf[MAX_BUFSIZE];
	int ret;
	sprintf(buf, "AT+NTSETID=1,%d%d\r\n", imei[0], imei[1]);
	ret = bc95_comm((u8 *)buf, strlen(buf), 10);
	return ret;
}
/*
 * 查询IMEI
 */
int bc95_req_getimei(u8 *imei)
{
	char buf[MAX_BUFSIZE], *pst;
	int ret;
	sprintf(buf, "AT+CGSN=1\r\n");
	ret = bc95_comm((u8 *)buf, strlen(buf), 10);
	//处理收到的数据
	if (ret >= 0) {
		if ((pst = bc95_recv_find("+CGSN:")) != NULL) {
			pst += strlen("+CGSN:");
			memcpy(imei, pst, 15);
		} else {
			return -1;
		}
	}
	return ret;
}
/*
 * 查询IMSI
 */
int bc95_req_getimsi(u8 *imsi)
{
	char buf[MAX_BUFSIZE], *pst;
	int ret;
	sprintf(buf, "AT+CIMI\r\n");
	ret = bc95_comm((u8 *)buf, strlen(buf), 1);
	//处理收到的数据
	if (ret >= 0) {
		memcpy(imsi, &bc95_body.recv_buf[2], 15);
	}
	return ret;
}
/*
 * 设置频带
 */
int bc95_cmd_band(u8 band)
{
	char buf[MAX_BUFSIZE];
	int ret;
	sprintf(buf, "AT+NBAND=%d\r\n", band);
	ret = bc95_comm((u8 *)buf, strlen(buf), 10);
	return ret;
}
/*
 * 查询频带
 */
int bc95_req_band(void)
{
	char buf[MAX_BUFSIZE], *pst;
	int ret;
	bc95_info *p = &bc95_body;
	sprintf(buf, "AT+NBAND?\r\n");
	ret = bc95_comm((u8 *)buf, strlen(buf), 1);
	//处理收到的数据
	if (ret >= 0) {
		if ((pst = bc95_recv_find("+NBAND:")) != NULL) {
			pst += strlen("+NBAND:");
			ret = atoi(pst);
		} else {
			return -1;
		}
	}
	return ret;
}
/*
 * 设置CFUN
 */
int bc95_cmd_cfun(u8 cfun)
{
	char buf[MAX_BUFSIZE];
	int ret;
	sprintf(buf, "AT+CFUN=%d\r\n", cfun);
	ret = bc95_comm((u8 *)buf, strlen(buf), 30);
	return ret;
}
/*
 * 设置CSCON
 */
int bc95_cmd_cscon(u8 cscon)
{
	char buf[MAX_BUFSIZE];
	int ret;
	sprintf(buf, "AT+CSCON=%d\r\n", cscon);
	ret = bc95_comm((u8 *)buf, strlen(buf), 30);
	return ret;
}
/*
 * 设置CSCON
 */
int bc95_cmd_npsmr(u8 npsmr)
{
	char buf[MAX_BUFSIZE];
	int ret;
	sprintf(buf, "AT+NPSMR=%d\r\n", npsmr);
	ret = bc95_comm((u8 *)buf, strlen(buf), 30);
	return ret;
}
/*
 * 查询CFUN
 */
int bc95_req_cfun(void)
{
	char buf[MAX_BUFSIZE], *pst;
	int ret;
	bc95_info *p = &bc95_body;
	sprintf(buf, "AT+CFUN?\r\n");
	ret = bc95_comm((u8 *)buf, strlen(buf), 1);
	//处理收到的数据
	if (ret >= 0) {
		if ((pst = bc95_recv_find("+CFUN:")) != NULL) {
			pst += strlen("+CFUN:");
			ret = atoi(pst);
		} else {
			return -1;
		}
	}
	return ret;
}
/*
 * 查询信号强度
 */
int bc95_req_rssi(void)
{
	char buf[MAX_BUFSIZE], *pst;
	int ret;
	bc95_info *p = &bc95_body;
	sprintf(buf, "AT+CSQ\r\n");
	ret = bc95_comm((u8 *)buf, strlen(buf), 1);
	//处理收到的数据
	if (ret >= 0) {
		if ((pst = bc95_recv_find("+CSQ:")) != NULL) {
			pst += strlen("+CSQ:");
			ret = atoi(pst);
		} else {
			return -1;
		}
	}
	return ret;
}
/*
 * 查询设备状态
 */
int bc95_req_devstate(void)
{
	char buf[MAX_BUFSIZE], *p;
	int ret, signal_power, total_power;
	sprintf(buf, "AT+NUESTATS\r\n");
	ret = bc95_comm((u8 *)buf, strlen(buf), 1);
	//处理收到的数据
	if (ret >= 0) {
		if ((p = bc95_recv_find("Signal power:")) != NULL) {
			p += strlen("Signal power:");
			signal_power = atoi(p);
		}
		if ((p = bc95_recv_find("Total power:")) != NULL) {
			p += strlen("Total power:");
			total_power = atoi(p);
		}
	}
	return ret;
}
/*
 * 查询网络激活状态
 */
int bc95_req_netalive(void)
{
	char buf[MAX_BUFSIZE], *pst;
	int ret;
	bc95_info *p = &bc95_body;
	sprintf(buf, "AT+CGATT?\r\n");
	ret = bc95_comm((u8 *)buf, strlen(buf), 1);
	//处理收到的数据
	if (ret >= 0) {
		if ((pst = bc95_recv_find("+CGATT:")) != NULL) {
			pst += strlen("+CGATT:");
			ret = atoi(pst);
		} else {
			return -1;
		}
	}
	return ret;
}
/*
 * 查询网络注册状态
 */
int bc95_req_netregiste(void)
{
	char buf[MAX_BUFSIZE], *pst;
	int ret;
	bc95_info *p = &bc95_body;
	sprintf(buf, "AT+CEREG?\r\n");
	ret = bc95_comm((u8 *)buf, strlen(buf), 1);
	//处理收到的数据
	if (ret >= 0) {
		if ((pst = bc95_recv_find("+CEREG:")) != NULL) {
			pst = strchr(pst, ',');
			pst += 1;
			ret = atoi(pst);
		} else {
			return -1;
		}
	}
	return ret;
}
/*
 * 查询网络连接状态
 */
int bc95_req_networksta(void)
{
	char buf[MAX_BUFSIZE], *pst;
	int ret;
	bc95_info *p = &bc95_body;
	sprintf(buf, "AT+CSCON?\r\n");
	ret = bc95_comm((u8 *)buf, strlen(buf), 1);
	//处理收到的数据
	if (ret >= 0) {
		if ((pst = bc95_recv_find("+CSCON:")) != NULL) {
			pst = strchr(pst, ',');
			pst += 1;
			ret = atoi(pst);
		} else {
			return -1;
		}
	}
	return ret;
}

/*
 * 设置CoAP IP
 */
int bc95_set_ip()
{
	char buf[MAX_BUFSIZE];
	int ret, len;
	memset(buf, 0, sizeof(buf));
	sprintf(buf, "AT+NCDP=%s\r\n", BC95_COMM_SERVER_IP);
	len = strlen(buf);
	ret = bc95_comm((u8 *)buf, len, 30);
	return ret;
}
/*
 * 设置NSMI,参数为是否使能
 */
int bc95_cmd_nsmi(u8 nsmi)
{
	char buf[MAX_BUFSIZE];
	int ret;
	sprintf(buf, "AT+NSMI=%d\r\n", nsmi);
	ret = bc95_comm((u8 *)buf, strlen(buf), 30);
	return ret;
}
/*
 * 设置NNMI,参数为是否使能
 */
int bc95_cmd_nnmi(u8 nnmi)
{
	char buf[MAX_BUFSIZE];
	int ret;
	sprintf(buf, "AT+NNMI=%d\r\n", nnmi);
	ret = bc95_comm((u8 *)buf, strlen(buf), 30);
	return ret;
}

/*
 * 查询CoAP发送数据情况
 */
int bc95_req_coapSenStatus()
{
	char buf[MAX_BUFSIZE], *pst;
	int ret;
	bc95_info *p = &bc95_body;
	sprintf(buf, "AT+NQMGS\r\n");
	ret = bc95_comm((u8 *)buf, strlen(buf), 1);
	//处理收到的数据
	if (ret >= 0) {
		char pending, send, error = 1;
		char *_pending, *_pending2, *_send, *_send2, *_error;
		if ((_error = bc95_recv_find("ERROR=")) != NULL) {
			_error += strlen("ERROR=");
			sscanf(_error, "%d", &error);
			debug("bc95_req_coapSenStatus error = %d\n", error);
			
			/*
			//send
			if ((_send = strstr((const char *)p->recv_buf, "SENT=")) != NULL) {
				_send += strlen("SENT=");
				if ((_send2 = strstr((const char *)_send, ",")) != NULL) {
					strncpy(&send, _send + 1, _send2 - _send);
				}
			}
			*/
			
			//pending
			if ((_pending = bc95_recv_find("PENDING=")) != NULL) {
				_pending += strlen("PENDING=");
				pending = atoi(_pending);
				
			}
		}
		//如果发送数据无误，查看是否发送完成
		if (!error) {
			if (pending == 0) {
				debug("bc95_req_coapSenStatus pendiing = %d\n", pending);
				return pending;
			}
		}
	}
	return -1;
}

/*
 * 查询PDP地址
 */
int bc95_req_pdpaddr(void)
{
	char buf[MAX_BUFSIZE], *p, *addr;
	const char *delim = " "; /* separator: ' ' */
	int ret;
	sprintf(buf, "AT+CGPADDR=1\r\n");
	ret = bc95_comm((u8 *)buf, strlen(buf), 1);
	//处理收到的数据
	if (ret >= 0) {
		if ((p = strstr(buf, ",")) != NULL) {
			p += strlen(",");
			addr = strtok(p, delim);
		}
	}
	return ret;
}
/*
 * PING地址
 */
int bc95_req_ping(void)
{
	char buf[MAX_BUFSIZE];
	int ret;
	sprintf(buf, "AT+NPING=%s\r\n", "192.168.2.1");
	ret = bc95_comm((u8 *)buf, strlen(buf), 1);
	return ret;
}
/*
 * 获取时间
 */
time64_t bc95_req_time(void)
{
	char buf[MAX_BUFSIZE], *pst;
	int ret, year, month, day, hour, minute, second, time_zone;
	time64_t utc_time;
	sprintf(buf, "AT+CCLK?\r\n");
	ret = bc95_comm((u8 *)buf, strlen(buf), 1);
	//处理收到的数据
	if (ret < 0) {
		return ret;
	}
	if ((pst = bc95_recv_find("+CCLK:")) != NULL) {
		pst += strlen("+CCLK:");
		year = atoi(pst);
		pst += 3;
		month = atoi(pst);
		pst += 3;
		day = atoi(pst);
		pst += 3;
		hour = atoi(pst);
		pst += 3;
		minute = atoi(pst);
		pst += 3;
		second = atoi(pst);
		pst += 3;
		time_zone = atoi(pst);
		pst += 3;
		utc_time = mktime64(year + 2000, month, day, hour, minute, second);
	} else {
		return -1;
	}
	return utc_time;
}
/*
 * reboot重启
 */
int bc95_cmd_reboot(void)
{
	char buf[MAX_BUFSIZE];
	sprintf(buf, "AT+NRB\r\n");
	uart3_tx((u8 *)buf,strlen(buf));
	return 0;
}
/*
 * 建立socket
 */
int bc95_cmd_sockcreat(void)
{
	char buf[MAX_BUFSIZE];
	int ret;
	bc95_info *p = &bc95_body;
	sprintf(buf, "AT+NSOCR=DGRAM,17,%d,%d\r\n", p->client_info.port, p->client_info.recv_mode);
	ret = bc95_comm((u8 *)buf, strlen(buf), 1);
	//处理收到的数据
	if (ret >= 0) {
		ret = atoi((const char *)&p->recv_buf[2]);
	}
	return ret;
}
void hex_to_ascii(unsigned char *ascii_buf, const unsigned char *hex_buf, int hex_num)
{
	int i;
	unsigned char high, low;
	for (i = 0; i < hex_num; i++) {
		high = hex_buf[i]>>4;
		low = hex_buf[i] & 0x0f;
		if ((high >= 0) && (high <= 9)) {
			ascii_buf[i * 2] = 0x30 + high;
		} else if ((high >= 0x0a) && (high <= 0x0f)) {
			ascii_buf[i * 2] = 0x37 + high;
		}
		if ((low >= 0) && (low <= 9)) {
			ascii_buf[i * 2 + 1] = 0x30 + low;
		} else if ((low >= 0x0a) && (low <= 0x0f)) {
			ascii_buf[i * 2 + 1] = 0x37 + low;
		}
	}
}
void ascii_to_hex(unsigned char *hex_buf, const unsigned char *ascii_buf, int ascii_num)
{
	int i;
	unsigned char high, low;
	for (i = 0; i < (ascii_num / 2); i++) {
		high = low = 0;
		if ((ascii_buf[i * 2] >= '0') && (ascii_buf[i * 2] <= '9')) {
			high = ascii_buf[i * 2] - 0x30;
		} else if ((ascii_buf[i * 2] >= 'A') && (ascii_buf[i * 2] <= 'F')) {
			high = ascii_buf[i * 2] - 0x37;
		}
		if ((ascii_buf[i * 2 + 1] >= '0') && (ascii_buf[i * 2 + 1] <= '9')) {
			low = ascii_buf[i * 2 + 1] - 0x30;
		} else if ((ascii_buf[i * 2 + 1] >= 'A') && (ascii_buf[i * 2 + 1] <= 'F')) {
			low = ascii_buf[i * 2 + 1] - 0x37;
		}
		hex_buf[i] = (high<<4) + low;
	}
}

//是否发送完成
int coap_send_finish(char *string)
{
	char *s = "+NSMI:SENT";
	int count = 0;
	char *p = string;
	p = strstr(p, s);
	if (p) {
		debug("coap_send_finish\n");
		return 0;
	} else {
		debug("coap_send_finish falied\n");
		return -1;
	}
}

/*
 * CoAP发送数据
 */
int bc95_cmd_coapSend(u8 *data, u8 num)
{
	u8 water_hydrant = 0;
	water_hydrant = dev_state.water_sta | (dev_state.hydrant_sta << 1);
	char buf[MAX_BUFSIZE];
	int ret, len;
	memset(buf, 0, sizeof(buf));
	sprintf(buf, "AT+NMGS=%d,", BC95_COMM_STATUS_LEN);
	len = strlen(buf);
	hex_to_ascii((u8 *)&buf[len], data, num);
	memcpy(&buf[len + (num * 2)], "\r\n", 2);
	ret = bc95_comm((u8 *)buf, len + (num * 2) +2, 1);
	debug("bc95_cmd_coapSend : %s\n", buf);
	return ret;
}

/*
 * 发送消息，两种模式，不需要应答和需要应答，区别在于发送完成是否立即关闭模块
 */
int bc95_cmd_udpsend(u8 fd, u8 mode, u8 *data, u8 num)
{
	char buf[MAX_BUFSIZE];
	int ret, len;
	memset(buf, 0, sizeof(buf));
	if (mode) {//mode==1需要应答
		sprintf(buf, "AT+NSOST=%d,%s,%d,%d,", bc95_body.client_info.fd, \
				bc95_body.server_info.addr, bc95_body.server_info.port, num);
	} else {//mode==0不需要应答，发送完成直接关闭
		sprintf(buf, "AT+NSOSTF=%d,%s,%d,0x%d,%d,", bc95_body.client_info.fd, \
				bc95_body.server_info.addr, bc95_body.server_info.port, 200, num);
	}
	len = strlen(buf);
	hex_to_ascii((u8 *)&buf[len], data, num);
	memcpy(&buf[len + (num * 2)], "\r\n", 2);
	debug("send msg : %s\n", buf);
	ret = bc95_comm((u8 *)buf, len + (num * 2) +2, 1);
	return ret;
}
/*
 * 等待消息到达通知
 */
int bc95_cmd_wait_response(void)
{
	char buf[MAX_BUFSIZE], *p;
	int ret;
	ret = uart3_rx();
	if (ret < 0) {
		return -1;
	}
	if ((p = strstr(buf, "+NSONMI:"))  != NULL) {
		p += strlen("+NSONMI:");
		if (atoi(p) == bc95_body.client_info.fd) {
			return atoi(p + 2);
		}
	}
	return -1;
}

static char udp_recv_buf[1024];
/*
 * 从模块内读取消息
 */
int bc95_cmd_udpread(u8 fd, int datanum, u8 *data)
{
	const char *delim = ","; /* separator: ' ' */
	char *token, *addr, *port, *num, *remain_num, *recvdata, *recv_buf_ptr;
	char buf[MAX_BUFSIZE];
	char *recv_buf = udp_recv_buf;
	memset(recv_buf, 0, sizeof(udp_recv_buf));
	recv_buf_ptr = recv_buf;
	int max_data_size = 300;
	int ret, len = 0, remain = min(datanum, max_data_size);
	if (datanum > max_data_size) {
		debug("bc95_cmd_udpread : datanum is too big %d\n", datanum);
	}
	bc95_info *bc95 = &bc95_body;
	for (;;) {
		memset(buf, 0, sizeof(buf));
		sprintf(buf, "AT+NSORF=%d,%d\r\n", bc95_body.client_info.fd, remain);
		ret = bc95_comm((u8 *)buf, strlen(buf), 1);
		//处理收到的数据
		if (ret >= 0) {
			token = strtok((char *)(&bc95->recv_buf[2]), delim);
			if (atoi(token) == bc95_body.client_info.fd) {
				addr = strtok(NULL, delim);
				port = strtok(NULL, delim);
				num = strtok(NULL, delim);
				recvdata = strtok(NULL, delim);
				remain_num = strtok(NULL, delim);
				ret = atoi(num);
				remain = atoi(remain_num);
				remain = min(remain, max_data_size);
				memcpy(recv_buf_ptr, recvdata, ret * 2);
				recv_buf_ptr += ret * 2;
				len += ret * 2;
			} else if (strcmp(token, "OK") == 0) {
				debug("bc95_cmd_udpread : recv OK, no data %s\n", token);
				return ret;
			} else {
				debug("bc95_cmd_udpread : recv something %s\n", token);
				break;
			}
			if (remain == 0) {
				break;
			}
		} else {
			return ret;
		}
	}
	ascii_to_hex(data, (const unsigned char *)recv_buf, min(len, datanum * 2));
	return min(len / 2, datanum);
}
/*
 * 关闭socket
 */
int bc95_cmd_sockclose(void)
{
	char buf[MAX_BUFSIZE];
	int ret;
	sprintf(buf, "AT+NSOCL=%d\r\n", bc95_body.client_info.fd);
	ret = bc95_comm((u8 *)buf, strlen(buf), 1);
	return ret;
}
/*
 * 创建UDP套接字
 */
int udp_creatsocket(void)
{
	int sockfd;
	//创建套接字
	if ((sockfd = bc95_cmd_sockcreat()) < 0) {
		return -1;
	} else {
		bc95_body.client_info.fd = sockfd;
	}
	return 0;
}
/*
 * 命令方式初始化模块
 */
int bc95_cmd_init(void)
{
	int i = 0;
	do {//开机后接收开机成功应答。不成功则重复开机过程
		bc95_reset();
	} while (bc95_turnon_response() < 0);
	i = 0;
	while (bc95_req_getimei((u8 *)bc95_body.client_info.imei) < 0) {
		debug("SIM Card do not have an IMEI.\n");
		mdelay(100);
		i++;
		if (i >= 100) {
			bc95_cmd_reboot();
			return -1;
		}
		//设置IMEI号，重启
	//	bc95_cmd_cfun(0);
	//	bc95_cmd_setimei(bc95_body.client_info.imei);
	//	bc95_cmd_reboot();
	//	return -1;
	}
	if (bc95_req_band() != BAND_900) {
		//设置频带，重启
		bc95_cmd_band(BAND_900);
		bc95_cmd_reboot();
		return -1;
	}
	if (bc95_req_autoconnect() != 1) {
		//设置自动连接功能
		bc95_cmd_autoconnect(1);
		bc95_cmd_reboot();
		return -1;
	}
	i = 0;
	while (bc95_req_cfun() != 1) {//等待10S让模块自动连接上网络
		mdelay(100);
		i++;
		if (i == 100) {
			bc95_cmd_cfun(1);
			bc95_cmd_reboot();
			return -1;
		}
	}
	if (bc95_req_getimsi((u8 *)bc95_body.client_info.imsi) < 0) {
		debug("SIM Card do not have an IMSI.\n");
		return -1;
	}
	bc95_req_rssi();
	bc95_req_devstate();
	bc95_cmd_nsmi(1);
	i = 0;
	while ((bc95_body.client_info.net_alive = bc95_req_netalive()) != 1) {
		mdelay(100);
		i++;
		if (i == 300) {
			bc95_cmd_reboot();
			return -1;
		}
	};
	bc95_body.client_info.net_registe = bc95_req_netregiste();
	bc95_body.client_info.net_worksta = bc95_req_networksta();
	/*
	//设置IP
	i = 0;
	while (bc95_set_ip() < 0) {
		debug("bc95_set_ip failed.\n");
		mdelay(100);
		i++;
		if (i == 100) {
			bc95_cmd_reboot();
			return -1;
		}
	}
	*/
	bc95_req_rssi();
	bc95_cmd_cscon(1);
	bc95_cmd_npsmr(1);
	return 0;
}
/*
 * 初始化UART，初始化模块，返回0失败，返回1成功
 */
int bc95_init(void)
{
	//暂时没用
	bc95_body.client_info.port = 8899;
	bc95_body.client_info.recv_mode = 1;
	reset_io_init();
	if (bc95_cmd_init() < 0) {
		return 0;
	}
	udp_creatsocket();//初始化完成后，创建socket
	return 1;
}
/*
 * 获取网络状态
 */
void get_net_sta(void)
{
	bc95_body.client_info.net_alive = bc95_req_netalive();
	bc95_body.client_info.net_registe = bc95_req_netregiste();
	bc95_body.client_info.net_worksta = bc95_req_networksta();
	bc95_req_rssi();
}

void get_server_sta(bc95_info *p)
{
	p->sta_update.cmd = dev_state.cmd_type;
	memcpy(p->sta_update.id, dev_state.id, sizeof(dev_state.id));
	if (!dev_state.is_retry) {
		p->sta_update.index++;
	}
	p->sta_update.dev_sta = (dev_state.hydrant_sta << 1) + dev_state.water_sta;
	p->sta_update.voltage = dev_state.voltage;
	p->sta_update.verson_num = VERSION_NUM;
	memcpy(p->sta_update.code_imei, p->client_info.imei, sizeof(p->client_info.imei));
	memcpy(p->sta_update.code_imsi, p->client_info.imsi, sizeof(p->client_info.imsi));
	p->sta_update.hydrant_time = dev_state.hydrant_time;
	p->sta_update.water_time = dev_state.water_time;
}

int update_sta_data(u8 *data, bc95_sta_update *p)
{
	u16 crcdata;
	memcpy(&data[PACKNUM_ID], p->id, sizeof(p->id));
	data[PACKNUM_INDEX] = p->index;
	data[PACKNUM_CMD] = p->cmd;
	data[PACKNUM_DATALEN] = STATE_LEN>>8;
	data[PACKNUM_DATALEN + 1] = (u8)STATE_LEN;
	data[STA_NUM_DEV_STA] = p->dev_sta;
	data[STA_NUM_VOLT] = p->voltage>>8;
	data[STA_NUM_VOLT + 1] = (u8)p->voltage;
	data[STA_NUM_VERSON_NUM] = p->verson_num>>8;
	data[STA_NUM_VERSON_NUM + 1] = (u8)p->verson_num;
	memcpy(&data[STA_NUM_IMEI], p->code_imei, sizeof(p->code_imei));
	memcpy(&data[STA_NUM_IMSI], p->code_imsi, sizeof(p->code_imsi));
	data[STA_NUM_HYDRANT_UTC] = (u8)(p->hydrant_time>>24);
	data[STA_NUM_HYDRANT_UTC + 1] = (u8)(p->hydrant_time>>16);
	data[STA_NUM_HYDRANT_UTC + 2] = (u8)(p->hydrant_time>>8);
	data[STA_NUM_HYDRANT_UTC + 3] = (u8)(p->hydrant_time>>0);
	data[STA_NUM_WATER_UTC] = (u8)(p->water_time>>24);
	data[STA_NUM_WATER_UTC + 1] = (u8)(p->water_time>>16);
	data[STA_NUM_WATER_UTC + 2] = (u8)(p->water_time>>8);
	data[STA_NUM_WATER_UTC + 3] = (u8)(p->water_time>>0);
	crcdata = usCRC16(data, STA_NUM_CRC);
	data[STA_NUM_CRC] = (u8)(crcdata>>8);
	data[STA_NUM_CRC + 1] = (u8)crcdata;
	return STA_NUM_CRC + 2;
}

void server_task(dev_info *dev)
{
	int i = 0;
	int count = 0;
	time64_t utc_time;
	static int j = 0, filed_count = 0;
	char *pst;
	char recv_buf[MAX_BUFSIZE];
	bc95_info *bc95 = &bc95_body;
	
	if (dev->gprs_cmd == GPRS_CMD_NONE) {//无任务退出
		THREAD_CYCLE(bc95->comm_interval * 1000);//到达通信间隔后转到发送状态
		dev->gprs_cmd = GPRS_CMD_SEND;
		dev->cmd_type = CMD_STA_TICK;
		dev->is_retry = 0;
		debug("server_task : tick send time\n");
		reboot_cnt++;
	} else if (dev->gprs_cmd == GPRS_CMD_SEND) {
		//打包发送数据
		get_server_sta(bc95);
		memset(bc95->send_buf, 0, sizeof(bc95->send_buf));
		bc95->send_num = update_sta_data(bc95->send_buf, &bc95->sta_update);
		utc_time = bc95_req_time();
		if (utc_time > 0) {
			rtc_set_sec(utc_time);
			debug("server_task : sync utc time %lld\n", utc_time);
		}
		for (i = 0; i < 5; i++) {
			//bc95_req_netalive();
			//bc95_req_netregiste();
			if (bc95_cmd_udpsend(bc95->client_info.fd, (dev->cmd_type == CMD_STA_TICK)?0:1, bc95->send_buf, bc95->send_num) < 0)  {
				if (i >= 4) {
					dev->gprs_cmd = GPRS_CMD_NONE;//发送失败后置位无任务，五次都失败就等待下次发送
					debug("server_task : send failed\n");
					dev->gprs_conn_sta = 0;
				}
				continue;
			} else if (dev->cmd_type == CMD_STA_TICK) {
				dev->gprs_cmd = GPRS_CMD_NONE;
				j = 0;
				debug("server_task : send tick successed\n");
				break;
			} else {
				dev->gprs_cmd = GPRS_CMD_WAIT;//发送成功后转到等待状态
				j = 0;
				debug("server_task : send sta successed, goto wait\n");
				uart3_rx();//开启接收
				break;
			}
		}
	} else if (dev->gprs_cmd == GPRS_CMD_WAIT) {
		if (bc95->sta_req.recv_complete) {
			bc95->sta_req.recv_complete = 0;
			if (bc95->sta_update.index == bc95->sta_req.index) {
				//dev->gprs_cmd = GPRS_CMD_NONE;
				//debug("server_task : recv successed\n");
				dev->gprs_cmd = GPRS_CMD_SEND;
				dev->cmd_type = CMD_STA_TICK;
				dev->is_retry = 0;
				debug("server_task : recv successed and send tick now\n");
				//查询是否需要升级
				if (bc95->sta_req.cmd == CMD_STA_RESPONSE) {//不是升级应答
					
				} else if (bc95->sta_req.cmd == CMD_STA_RESPONSE_IAP) {//是升级应答，需要升级
					if (bc95->sta_req.verson_num != VERSION_NUM) {//查版本号是否匹配
						//置位IAP命令，开始传输
						iap.iap_cmd = 1;
						//检查是否为断点续传
						if (bc95->sta_req.verson_num != iap.version) {
							debug("server_task : recv iap response, version changed[%d %d]\n", bc95->sta_req.verson_num, iap.version);
							//若是新的未传输过的升级包，则进行相应信息更新
							iap.version = bc95->sta_req.verson_num;
							iap.pack_num = bc95->sta_req.pack_num;
							iap.pack_id = 0;
							iap.bin_crc = bc95->sta_req.bin_crc;
							iap.data_len = 0;
							iap.write_len = 0;
							FLASH_Unlock();
							FLASH_If_Erase(APPLICATION_ADDRESS, bc95->sta_req.bin_len);
							FLASH_Lock();
						}
					}
				}
				j = 0;
				filed_count = 0;
			}
		} else {//超时30S再次发送以激活模块
			if (t1s_flag == 1) {
				t1s_flag = 0;
				j++;
			}
			if (j >= BC95_RECV_TIMEOUT) {
				j = 0;
				bc95_cmd_udpread(bc95->client_info.fd, 17, (u8 *)recv_buf);
				dev->gprs_cmd = GPRS_CMD_SEND;
				dev->is_retry = 1;
				filed_count++;//计算超时次数
				debug("server_task : wait timeout\n");
			}
			if (filed_count >= BC95_SEND_TRYNUM) {//长时间超时未接收时，尝试重启模块
				debug("bc95 comm filed %d times, retry init, cancel send\n", BC95_SEND_TRYNUM);
				filed_count = 0;
				dev->gprs_conn_sta = 0;
				dev->gprs_cmd = GPRS_CMD_NONE;
			}
		}
	}
}

//初始化iap
void iap_init(void)
{
	iap.gprs_cmd = GPRS_CMD_NONE;
	iap.iap_cmd = 0;
	iap.version = VERSION_NUM;
	iap.pack_num = 0;
	iap.pack_id = 0;
	iap.bin_crc = 0;
	iap.data_len = 0;
	iap.write_len = 0;
}

void get_iap_request(bc95_info *p)
{
	memcpy(p->iap_update.id, dev_state.id, sizeof(dev_state.id));
	p->iap_update.index++;
	p->iap_update.new_verson_num = iap.version;
	p->iap_update.pack_cnt = iap.pack_id;
}

int update_iap_data(u8 *data, bc95_iap_update *p)
{
	u16 crcdata;
	memcpy(&data[PACKNUM_ID], p->id, sizeof(p->id));
	data[PACKNUM_INDEX] = p->index;
	data[PACKNUM_CMD] = CMD_IAP_REQUEST;
	data[PACKNUM_DATALEN] = IAP_LEN>>8;
	data[PACKNUM_DATALEN + 1] = (u8)IAP_LEN;
	data[IAPREQ_NEW_VERSONNUM] = p->new_verson_num>>8;
	data[IAPREQ_NEW_VERSONNUM + 1] = (u8)p->new_verson_num;
	data[IAPREQ_PACK_CNT] = p->pack_cnt>>8;
	data[IAPREQ_PACK_CNT + 1] = (u8)p->pack_cnt;
	crcdata = usCRC16(data, IAPREQ_CRC);
	data[IAPREQ_CRC] = (u8)(crcdata>>8);
	data[IAPREQ_CRC + 1] = (u8)crcdata;
	return IAPREQ_CRC + 2;
}

// 把接收到的升级包字符串数据转换成u16的数据
int str_to_short(const char *src, u16 *dist)
{
	volatile int len = strlen(src), i;
	u16 data;
	const char *p = src;
	char buf[3];
	len = min(len, 512);
	for (i = 0; i < (len / 4); i++) {
		data = 0;
		
		memset(buf, 0, sizeof(buf));
		memcpy(buf, p, 2);
		buf[2] = '\0';
		data = strtol(buf, NULL, 16);
		p += 2;
		
		memset(buf, 0, sizeof(buf));
		memcpy(buf, p, 2);
		buf[2] = '\0';
		data += (strtol(buf, NULL, 16)<<8);
		p += 2;

		dist[i] = data;
	}
	if ((len % 4) != 0) {
		debug("error : IAP BIN file len is error %d\n", len / 2);
	}
	return i;
}

//把接收到的iap数据进行高低字节整理
int char_to_short(const char *src, u16 *dist, int pack_len)
{
	int i;
	unsigned char high, low;
	for (i = 0; i < (pack_len / 2); i++) {
		high = src[(i * 2) + 1];
		low = src[i * 2];
		dist[i] = (high<<8) + low;
	}
	return i;
}

void iap_task(iap_comm_info *p)
{
	int i = 0;
	int count = 0;
	static int j = 0, filed_count = 0;
	char recv_buf[MAX_BUFSIZE];
	bc95_info *bc95 = &bc95_body;
	volatile u16 crc_check;

	if (!p->iap_cmd) {
		return;
	}
	
	if (p->gprs_cmd == GPRS_CMD_NONE) {//无任务退出
		THREAD_CYCLE(BC95_IAP_INTERVAL * 1000);//到达通信间隔后转到发送状态
		p->gprs_cmd = GPRS_CMD_SEND;
		debug("iap_task : iap send time\n");
	} else if (p->gprs_cmd == GPRS_CMD_SEND) {
		//打包发送数据
		get_iap_request(bc95);
		memset(bc95->send_buf, 0, sizeof(bc95->send_buf));
		bc95->send_num = update_iap_data(bc95->send_buf, &bc95->iap_update);
		for (i = 0; i < 5; i++) {
			if (bc95_cmd_udpsend(bc95->client_info.fd, 1, bc95->send_buf, bc95->send_num) < 0)  {
				if (i >= 4) {
					p->gprs_cmd = GPRS_CMD_NONE;//发送失败后置位无任务，五次都失败就等待下次发送
					debug("iap_task : send failed\n");
					dev_state.gprs_conn_sta = 0;
				}
				mdelay(20);
				continue;
			} else {
				p->gprs_cmd = GPRS_CMD_WAIT;//发送成功后转到等待状态
				debug("iap_task : send successed, goto wait\n");
				uart3_rx();//开启接收
				break;
			}
		}
	} else if (p->gprs_cmd == GPRS_CMD_WAIT) {
		if (bc95->iap_req.recv_complete) {
			bc95->iap_req.recv_complete = 0;
			if (bc95->iap_update.index == bc95->iap_req.index) {
				if (iap.pack_id == bc95->iap_req.pack_cnt) {//对比请求的包号
					if (iap.version != bc95->iap_req.verson_num) {//传输过程中版本号变了
						debug("iap_task : version number is can not compare[%d %d], iap init\n", iap.version, bc95->iap_req.verson_num);
						iap_init();
					} else if (bc95->iap_req.pack_len % 2) {
						debug("iap_task : pack len is a signal number %d\n", bc95->iap_req.pack_len);
					} else {
						//id一致，正确接收该分包，进行转换并校验
						memset(iap.data, 0, sizeof(iap.data));
						//数据转换
						iap.data_len = char_to_short((const char *)bc95->iap_req.data, iap.data, bc95->iap_req.pack_len);
						//CRC校验
						crc_check = usCRC16((unsigned char *)bc95->iap_req.data, bc95->iap_req.pack_len);
						//对比CRC
						if (crc_check == bc95->iap_req.pack_crc) {//CRC对比正确
							FLASH_Unlock();
							//存储到FLASH
							FLASH_If_Write2((APPLICATION_ADDRESS + (iap.write_len * 2)), (u32 *)iap.data, iap.data_len * 2);
							FLASH_Lock();
							//更新写入长度
							iap.write_len += iap.data_len;
							//判断是否为最后一包
							if (iap.pack_id >= (iap.pack_num - 1)) {
								//正确接收最后一包
								//校验整个BIN文件
								crc_check = usCRC16((unsigned char *)APPLICATION_ADDRESS, iap.write_len * 2);
								if (crc_check == iap.bin_crc) {
									//BIN文件校验成功，调用IAP程序，进行升级
									debug("IAP RECV SUCCESS : ready to copy FLASH\n");
									struct iap_info iap_msg;
									iap_msg.base_addr = FLASH_START_ADDRESS;
									iap_msg.backup_addr = APPLICATION_ADDRESS;
									iap_msg.code_size = iap.write_len * 2;
									iap_msg.flash_page_size = FLASH_SECTOR_SIZE;
									code_updata(&iap_msg);
								} else {
									//BIN文件校验失败，重新升级
									debug("iap_task : recv complete but all bin crc check failed, iap init\n");
									iap_init();
									iap.gprs_cmd = GPRS_CMD_SEND;
								}
							} else {
								//当前包不是最后一包
								iap.pack_id++;
								iap.gprs_cmd = GPRS_CMD_SEND;
							}
						}
					}
				} else {
					debug("iap_task : pack_id not compare [%d %d]\n", iap.pack_id, bc95->iap_req.pack_cnt);
				}
				j = 0;
				filed_count = 0;
			}
		} else {//超时30S再次发送以激活模块
			if (t1s_flag_sensor) {
				t1s_flag_sensor = 0;
				j++;
			}
			if (j >= BC95_RECV_TIMEOUT) {
				j = 0;
				bc95_cmd_udpread(bc95->client_info.fd, 17, (u8 *)recv_buf);
				p->gprs_cmd = GPRS_CMD_SEND;
				filed_count++;//计算超时次数
				debug("iap_task : wait timeout\n");
			}
			if (filed_count >= BC95_SEND_TRYNUM) {//长时间超时未接收时，尝试重启模块
				debug("iap_task : bc95 comm filed 5 times, cancle iap\n");
				filed_count = 0;
				//取消iap
				iap.iap_cmd = 0;
				iap.gprs_cmd = GPRS_CMD_NONE;
			}
		}
	}
}

void msg_recv_task(dev_info *dev)
{
	char recv_buf[MAX_BUFSIZE];
	bc95_info *bc95 = &bc95_body;
	int recv_cmd;
	if (!bc95->recv_data_flag) {
		return;
	}
	
	bc95->recv_data_flag = 0;
	memset(recv_buf, 0, sizeof(recv_buf));
	int len = bc95_cmd_udpread(bc95->client_info.fd, bc95->recv_len, (u8 *)recv_buf);
	if (len <= 0) {
		return;
	}
	debug("msg_recv_task : recv msg len %d\n", len);
	//校验CRC
	if ((len - 2) <= 0) {//保证接收到的长度合理
		return;
	}
	u16 crc_check = usCRC16((u8 *)recv_buf, len - 2);
	if (crc_check != ((recv_buf[len - 2]<<8) + recv_buf[len - 1])) {
		return;
	}
	//校验ID号
	if (memcmp(recv_buf, dev->id, sizeof(dev->id)) != 0) {
		return;
	}
	recv_cmd = recv_buf[PACKNUM_CMD];
	switch (recv_cmd) {
		case CMD_STA_RESPONSE:
			bc95->sta_req.cmd = CMD_STA_RESPONSE;
			bc95->sta_req.index = recv_buf[PACKNUM_INDEX];
			bc95->sta_req.utc_time = (recv_buf[DATANUM_UTC] << 24) + (recv_buf[DATANUM_UTC + 1] << 16) \
									+ (recv_buf[DATANUM_UTC + 2] << 8) + recv_buf[DATANUM_UTC + 3];
			bc95->sta_req.recv_complete = 1;
			break;
		case CMD_STA_RESPONSE_IAP:
			bc95->sta_req.cmd = CMD_STA_RESPONSE_IAP;
			bc95->sta_req.index = recv_buf[PACKNUM_INDEX];
			bc95->sta_req.utc_time = (recv_buf[DATANUM_UTC] << 24) + (recv_buf[DATANUM_UTC + 1] << 16) \
									+ (recv_buf[DATANUM_UTC + 2] << 8) + recv_buf[DATANUM_UTC + 3];
			bc95->sta_req.verson_num = (recv_buf[DATANUM_VERSON_NUM] << 8) + recv_buf[DATANUM_VERSON_NUM + 1];
			bc95->sta_req.pack_num = (recv_buf[DATANUM_PACK_NUM] << 8) + recv_buf[DATANUM_PACK_NUM + 1];
			bc95->sta_req.bin_crc = (recv_buf[DATANUM_BIN_CRC] << 8) + recv_buf[DATANUM_BIN_CRC + 1];
			bc95->sta_req.bin_len = (recv_buf[DATANUM_BIN_LEN] << 24) + (recv_buf[DATANUM_BIN_LEN + 1] << 16) \
									+ (recv_buf[DATANUM_BIN_LEN + 2] << 8) + recv_buf[DATANUM_BIN_LEN + 3];
			bc95->sta_req.recv_complete = 1;
			break;
		case CMD_IAP_RESPONSE:
			bc95->iap_req.index = recv_buf[PACKNUM_INDEX];
			bc95->iap_req.pack_cnt = (recv_buf[IAPNUM_PACK_CNT] << 8) + recv_buf[IAPNUM_PACK_CNT + 1];
			bc95->iap_req.pack_crc = (recv_buf[IAPNUM_PACK_CRC] << 8) + recv_buf[IAPNUM_PACK_CRC + 1];
			bc95->iap_req.verson_num = (recv_buf[IAPNUM_VERSON_NUM] << 8) + recv_buf[IAPNUM_VERSON_NUM + 1];
			memcpy(bc95->iap_req.data, &recv_buf[IAPNUM_DATA], len - IAPNUM_DATA - 2);
			bc95->iap_req.pack_len = len - IAPNUM_DATA - 2;
			bc95->iap_req.recv_complete = 1;
			break;
		default:
			debug("msg_recv_task : illegal cmd %d\n", recv_cmd);
	}
	debug("msg_recv_task : recv successed\n");
}

void sync_rtc_time()
{
	time64_t utc_time;
	while (1) {
		utc_time = bc95_req_time();
		if (utc_time > 0) {
			rtc_set_sec(utc_time);
			return;
		}
	}
}


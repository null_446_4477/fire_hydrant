#include "system.h"
#include "device.h"
#include "crc16.h"
#include "bc95.h"

dev_info dev_state;

void get_uid(u16 *id)
{
	for(int i = 0; i < 6; i++) {
		id[i] = *(vu16*)(ID_BASE_ADDR + (i * 2));
	}
}
void print_uid(void)
{
	int i;
	debug("UID : ");
	for (i = 0; i < 6; i++) {
		debug("%x ", dev_state.id[i]);
	}
	debug("\n");
}

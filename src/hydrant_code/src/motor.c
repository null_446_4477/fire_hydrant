#include "system.h"
#include "motor.h"
#include "device.h"
#include "switch.h"
#include "delay.h"
#include "bc95.h"
/*
 * 
 */
void relay_init(void)
{
	GPIO_InitTypeDef  GPIO_InitStructure;
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA,ENABLE);
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
}
/*
 * 电机正转
 */
void foreward_motor(void)
{
	REV_OFF;
	FWD_ON;
}
/*
 * 电机反转
 */
void reversal_motor(void)
{
	FWD_OFF;
	REV_ON;
}
/*
 * 待机
 */
void idle_motor(void)
{
	FWD_OFF;
	REV_OFF;
}
/*
 * 刹车
 */
void stop_motor(void)
{
	FWD_ON;
	REV_ON;
}
/*
 * 获取电机工作状态
 */
u8 get_motor_sta(void)
{
	if ((INA_STA == 1) && (INB_STA == 0)) {
		return MOTOR_UNLOCK;
	} else if ((INA_STA == 0) && (INB_STA == 1)) {
		return MOTOR_LOCK;
	} else {
		return MOTOR_NONE;
	}
}
void motor_task(dev_info *dev)
{
	if (dev->cmd == MOTOR_UNLOCK) {
		switch(get_motor_sta()) {
			case MOTOR_NONE :
				foreward_motor();
				dev->legalsta = STA_LOCK_HALF;
				break;
			case MOTOR_LOCK :
				stop_motor();
				mdelay(100);
				foreward_motor();
				dev->legalsta = STA_LOCK_HALF;
				break;
			case MOTOR_UNLOCK :
				if (get_lock_current_sta() == STA_LOCK_DOWN) {
				//	stop_motor();
					mdelay(100);
					idle_motor();
					dev->legalsta = STA_LOCK_DOWN;
					set_tick_comm();//状态改变后上报状态。
					dev->cmd = MOTOR_NONE;
				}
				break;
			default :
				break;
		}
	} else if (dev->cmd == MOTOR_LOCK) {
		switch (get_motor_sta()) {
			case MOTOR_NONE :
				reversal_motor();
				dev->legalsta = STA_LOCK_HALF;
				break;
			case MOTOR_LOCK :
				if (get_lock_current_sta() == STA_LOCK_UP) {
				//	stop_motor();
					mdelay(100);
					idle_motor();
					dev->legalsta = STA_LOCK_UP;
					set_tick_comm();//状态改变后上报状态。
					dev->cmd = MOTOR_NONE;
				}
				break;
			case MOTOR_UNLOCK :
				stop_motor();
				mdelay(100);
				reversal_motor();
				dev->legalsta = STA_LOCK_HALF;
				break;
			default :
				break;
		}
	}
}

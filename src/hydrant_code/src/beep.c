#include "system.h"
#include "beep.h"
#include "timer.h"

vs32 warning_begin,warning_close;       //报警计时
/*
 * 蜂鸣器初始化，主要工作为配置IO口
 */
void beep_init(void)
{
	GPIO_InitTypeDef  GPIO_InitStructure;
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA,ENABLE);	//使能GPIOC时钟

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12;				//BEEP-->PC.1 端口配置
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; 		//推挽输出
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;		//IO口速度为50MHz
	GPIO_Init(GPIOA, &GPIO_InitStructure);					//根据设定参数初始化GPIOC.1
}
/*
 * 开始报警
 */
void warning_beginning(void)
{
	warning_begin = jiffies;
	dev_state.alarm |= ALARM_ILLEGAL_UNLOCK;
}

void beep_warnning_task(dev_info *dev)
{
	if(dev->alarm & ALARM_ILLEGAL_UNLOCK) {
		warning_close = jiffies;
		BEEP_ON;
		if((warning_close - warning_begin) >= 3000) {//报警3S
			BEEP_OFF;
			dev->alarm &= ~ALARM_ILLEGAL_UNLOCK;
		}
	}
}

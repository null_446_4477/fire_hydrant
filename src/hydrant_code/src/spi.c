#include "system.h"
#include "spi.h"
#include "delay.h"

/**
 * SPI DMA transfer
 * SPI1 -> CH2 & CH3
 * SPI2 -> CH4 & CH5
 */
int spi_dma_txrx(const struct spi_master *spi, const struct spi_dev *spidev, u8 *tx_buf, u8 *rx_buf, u16 len)
{
	SPI_TypeDef *reg = spi->reg;
	const struct spi_dev *dev = spidev;
	DMA_Channel_TypeDef *dma_tx, *dma_rx;
	u32 tx_flag, rx_flag;
	int loop = 0;
	int ret = 0;

	if (reg == SPI1) {
		dma_rx = DMA1_Channel2;
		dma_tx = DMA1_Channel3;
		rx_flag = DMA1_IT_TC2;
		tx_flag = DMA1_IT_TC3;
		// clear flag
		DMA1->IFCR = DMA1_IT_GL2 | DMA1_IT_TC2 | DMA1_IT_HT2 | DMA1_IT_TE2
			| DMA1_IT_GL3 | DMA1_IT_TC3 | DMA1_IT_HT3 | DMA1_IT_TE3;
	} else if (reg == SPI2) {
		dma_rx = DMA1_Channel4;
		dma_tx = DMA1_Channel5;
		rx_flag = DMA1_IT_TC4;
		tx_flag = DMA1_IT_TC5;
		// clear flag
		DMA1->IFCR = DMA1_IT_GL5 | DMA1_IT_TC5 | DMA1_IT_HT5 | DMA1_IT_TE5
			| DMA1_IT_GL4 | DMA1_IT_TC4 | DMA1_IT_HT4 | DMA1_IT_TE4;
	} else {
		return -1;
	}

	/* 配置DMA1_CH2, SPI1_RX */
	// 外设地址
	dma_rx->CPAR = (u32)(&reg->DR);
	// 内存地址
	dma_rx->CMAR = (u32)(rx_buf);
	// 控制寄存器配置, 禁止所有中断, DMA disabled
	dma_rx->CCR = DMA_Priority_High | DMA_MemoryInc_Enable | DMA_DIR_PeripheralSRC
		| DMA_PeripheralInc_Disable | DMA_PeripheralDataSize_Byte
		| DMA_MemoryDataSize_Byte | DMA_Mode_Normal | DMA_M2M_Disable;
	// 长度配置
	dma_rx->CNDTR = len;
	
	/* 配置DMA1_CH3, SPI1_TX */
	// 外设地址
	dma_tx->CPAR = (u32)(&reg->DR);
	// 内存地址
	dma_tx->CMAR = (u32)(tx_buf);
	// 控制寄存器配置, 禁止所有中断, DMA disabled
	dma_tx->CCR = DMA_Priority_High | DMA_MemoryInc_Enable | DMA_DIR_PeripheralDST
		| DMA_PeripheralInc_Disable | DMA_PeripheralDataSize_Byte
		| DMA_MemoryDataSize_Byte | DMA_Mode_Normal | DMA_M2M_Disable;
	// 长度配置
	dma_tx->CNDTR = len;

	// 设置传输完成中断
	//DMA_ITConfig(DMA1_Channel3, DMA_IT_TC, ENABLE);
	
	// 设置当前数据长度，开始发送
	//DMA_SetCurrDataCounter(DMA1_Channel3, len);

	/* cs pol */
	spi->set_cs(dev->chip_select, 1);
	dma_rx->CCR |= DMA_CCR1_EN;
	dma_tx->CCR |= DMA_CCR1_EN;

	/* wait spi transfer complete */
	while ((DMA1->ISR & tx_flag) == 0);
	while ((DMA1->ISR & rx_flag) == 0) {
		udelay(1);
		loop++;
		if (loop > 5) {
			debugL(DBG_ERR, "SPI DMA - RX timeout: %p, %p, isr %x, cndtr %x, ccr %x, %x, %x\n",
				spi, spidev, DMA1->ISR, dma_rx->CNDTR,
				dma_rx->CCR, dma_tx->CNDTR, dma_tx->CCR);
			ret = -1;
			break;
		}
	}

	DMA1->IFCR = tx_flag | rx_flag;
	spi->set_cs(spidev->chip_select, 0);
	
	dma_rx->CCR &= ~DMA_CCR1_EN;
	dma_tx->CCR &= ~DMA_CCR1_EN;
	return ret;
}

#define SPI_TRYN 	5

// 带重试spi
int spi_dma_txrx_selftry(const struct spi_master *spi, const struct spi_dev *spidev, u8 *tx_buf, u8 *rx_buf, u16 len)
{
	int cnt = 0;
	do {
		if (spi_dma_txrx(spi, spidev, tx_buf, rx_buf, len) == 0) {
			break;
		}
		if (cnt > SPI_TRYN) {
			debugL(DBG_ERR, "spi_dma_txrx_selftry() spi err\n");
			return -1;
		}
		cnt++;
	} while(1);
	return 0;
}

#ifndef DELAY_H
#define DELAY_H

typedef volatile unsigned int  vu32;

void delay_config(void);
void udelay(vu32 loop);
void mdelay(vu32 loop);

#endif

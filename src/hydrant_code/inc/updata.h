#ifndef _UPDATA_H
#define _UPDATA_H
#include "system.h"
struct iap_info {
	volatile u32 base_addr;
	volatile u32 backup_addr;
	volatile u32 code_size;// byte size
	volatile u32 flash_page_size;// 2048 or 1024
};

void code_updata(struct iap_info *iap_msg);

#endif

#ifndef _IWDG_H
#define _IWDG_H


void reset(void);
void iwdg_init(void);
void iwdg_start(void);
void iwdg_reload(void);

#endif


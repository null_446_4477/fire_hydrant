#ifndef I2C_H
#define I2C_H

#include "system.h"

#define I2C_M_TEN		0x0010	/* this is a ten bit chip address */
#define I2C_M_RD		0x0001	/* read data, from slave to master */
#define I2C_M_NOSTART		0x4000	/* if I2C_FUNC_PROTOCOL_MANGLING */
#define I2C_M_REV_DIR_ADDR	0x2000	/* if I2C_FUNC_PROTOCOL_MANGLING */
#define I2C_M_IGNORE_NAK	0x1000	/* if I2C_FUNC_PROTOCOL_MANGLING */
#define I2C_M_NO_RD_ACK		0x0800	/* if I2C_FUNC_PROTOCOL_MANGLING */

struct i2c_msg {
	u16 addr;	/* slave address			*/
	u16 flags;
//#define I2C_M_RECV_LEN		0x0400	/* length will be first received byte */
	u16 len;		/* msg length				*/
	u8 *buf;		/* pointer to msg data			*/
};

struct i2c_gpio_data {
	GPIO_TypeDef	*sda_gpio;
	GPIO_TypeDef	*scl_gpio;
	unsigned int	sda_pin;
	unsigned int	scl_pin;
	int		udelay;
	int		timeout;
	int		retries;
};

extern struct i2c_gpio_data i2cgpio[];
static int try_address(struct i2c_gpio_data *i2c,unsigned char addr, int retries);
int i2c_test_bus(struct i2c_gpio_data *i2c);
void i2c_init(void);
int i2c_xfer(struct i2c_gpio_data *i2c,
		    struct i2c_msg msgs[], int num);
int i2c_reg_write(struct i2c_gpio_data *i2c, u8 addr, u8 *data, u16 n);
int i2c_reg_read(struct i2c_gpio_data *i2c, u8 addr, u8 reg, u8 *data, u16 n);

#endif


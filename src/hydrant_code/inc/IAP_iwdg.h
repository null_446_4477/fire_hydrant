#ifndef _IAP_IWDG_H_
#define _IAP_IWDG_H_

void IAP_reset(void);
void IAP_iwdg_reload(void);

#endif


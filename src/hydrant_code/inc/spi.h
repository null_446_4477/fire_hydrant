#ifndef SPI_H
#define SPI_H

struct spi_dev {
	u8 chip_select;
	u16 pin;
	GPIO_TypeDef *gpio;
	/* irq */
};

struct spi_master {
	u8 num_cs;		/* number of cs */
	u8 bus_num;		/* spi bus number */
	void (*set_cs)(u8 cs, int pol);
	SPI_TypeDef *reg;
#if 0
	int trans_complete;
	void *context;
#endif
};
int spi_dma_txrx(const struct spi_master *spi, const struct spi_dev *spidev, u8 *tx_buf, u8 *rx_buf, u16 len);
int spi_dma_txrx_selftry(const struct spi_master *spi, const struct spi_dev *spidev, u8 *tx_buf, u8 *rx_buf, u16 len);

#if 0
struct spi_transfer {
	const u8 *tx_buf;
	u8 *rx_buf;
	u16 len;
	//struct list_head transfer_list;
};
#endif
#endif


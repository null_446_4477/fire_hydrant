/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __FLASH_IF_H
#define __FLASH_IF_H

/* Includes ------------------------------------------------------------------*/

/* Exported macro ------------------------------------------------------------*/
/* each sector size (bytes) */
#define FLASH_SECTOR_SIZE	0x800     /* 1024 Bytes */

/* define the address from where IAP will be loaded, 0x08000000:BANK1 or 
   0x08030000:BANK2 */
#define FLASH_START_ADDRESS   (u32)0x08000000

/* define the address from where user application will be loaded,
   the application address should be a start sector address */
#define APPLICATION_ADDRESS   (u32)0x08019800

//FLASH存储参数起始地址
#define FLASH_PARAM_ADDR 0x08037000    		//开始写入的地址

// 参数在扇区的偏移量
#define	PARAM_SERIAL_NUM_OFFSET		0	//序列号，占用4字节
#define	PARAM_HOST_IP_OFFSET		(PARAM_SERIAL_NUM_OFFSET + 4)	//服务器IP，占用4字节
#define	PARAM_HOST_PORT_OFFSET		(PARAM_HOST_IP_OFFSET + 4)	//服务器端口号，占用2字节
#define	PARAM_COMM_INTERVAL_OFFSET	(PARAM_HOST_PORT_OFFSET + 2)	//通信间隔，占用4字节
#define	PARAM_RUN_MODE_OFFSET		(PARAM_COMM_INTERVAL_OFFSET + 4)	//运行模式，占用2字节
#define	PARAM_WT_SENSOR_INIT_OFFSET		(PARAM_RUN_MODE_OFFSET + 2)	//液位传感器误触发的时间阈值，占用4字节

#define ABS_RETURN(x,y)         (x < y) ? (y-x) : (x-y)

/* Get the number of sectors from where the user program will be loaded */
#define FLASH_SECTOR_NUMBER  ((u32)(ABS_RETURN(APPLICATION_ADDRESS,FLASH_START_ADDRESS))>>12)

#ifdef STM32F10X_HD 
  //#define USER_FLASH_LAST_PAGE_ADDRESS  0x0801FF00
  #define USER_FLASH_END_ADDRESS        0x0803FFFF
  /* Compute the mask to test if the Flash memory, where the user program will be
  loaded, is write protected */
  #define  FLASH_PROTECTED_SECTORS   ((u32)~((1 << FLASH_SECTOR_NUMBER) - 1))
#else
 #error "Please select first the STM32 device to be used (in stm32l1xx.h)"    
#endif 

/* define the user application size */
//#define USER_FLASH_SIZE   (USER_FLASH_END_ADDRESS - APPLICATION_ADDRESS + 1)

/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */

// 适用于在线升级拷贝多扇区的写入与擦除
void FLASH_If_Init(void);
int FLASH_If_Erase(u32 start_addr, u32 size);
int FLASH_If_Write2(__IO u32 FlashAddress, u32* Data , u16 DataLength);

// 适用于写参数等长度小的写入与读出
u16 FLASH_ReadHalfWord(u32 read_addr);		  //读出半字  
void FLASH_Write(u32 write_addr, u16 *p_data, u16 num);		//从指定地址开始写入指定长度的数据
void FLASH_Read(u32 ReadAddr, u16 *p_data, u16 num);   	   		//从指定地址开始读出指定长度的数据
void FLASH_check(void);                                  //获取FLASH参数，置位获取状态标志位


#endif  /* __FLASH_IF_H */

/*******************(C)COPYRIGHT 2012 STMicroelectronics *****END OF FILE******/

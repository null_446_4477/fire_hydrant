/*****************************************************************
             版权所有 (C) 天津市信电科技发展有限公司

	文件类型：	C Header File
	文件名：		LocalSetting.h
	创建：		wangds 2010-09-05
	功能简介：	读写配置文件：加载配置信息，保存内存配置信息到文件
******************************************************************/

#ifndef STRUCTS_H_
#define STRUCTS_H_

#include <time.h>
typedef unsigned int  u32;
typedef unsigned short u16;
typedef unsigned char  u8;
// mode
enum RF_MODE {
	RF_MODE_915M_250K,
	RF_MODE_915M_38_4K,
	RF_MODE_433M_38_4K,
	RF_MODE_433M_PA_38_4K,
	RF_MODE_480M_250K,
	RF_MODE_480M_38_4K,
};

#define CCREG_STRING(x)	((x) == RF_MODE_915M_250K ? "915M_250K" : \
						((x) == RF_MODE_915M_38_4K ? "915M_38.4K" : \
						((x) == RF_MODE_433M_38_4K ? "433M_38.4K" : \
						((x) == RF_MODE_433M_PA_38_4K ? "433M_PA_38.4K" : \
						((x) == RF_MODE_480M_250K ? "480M_250K" : \
						((x) == RF_MODE_480M_38_4K ? "480M_38.4K" : \
						"UNKOWN"))))))

#define RF_IS_915M(m)		((m) <= RF_MODE_915M_38_4K)
#define RF_IS_433M(m)		(!RF_IS_915M(m))
#define RF_IS_915M_250K(m)	((m) == RF_MODE_915M_250K)
#define RF_IS_915M_38_4k(m)	((m) == RF_MODE_915M_38_4K)
#define RF_IS_433M_PA(m)	((m) == RF_MODE_433M_PA_38_4K)
#define RF_IS_480M(m)		((m) == RF_MODE_480M_250K || (m) == RF_MODE_480M_38_4K)

#define RF_IS_MANUAL_PA(m) (m == RF_MODE_433M_PA_38_4K)
#define RF_IS_USE_PA(m) (m != RF_MODE_433M_38_4K)

typedef struct
{
	// 模式
	unsigned char mode;
	// 发送频道
	unsigned char tx_channel;
	// 接收频道
	unsigned char rx_channel;
	// 发射功率
	unsigned char pa;
	// 频率
	unsigned char freq[3];
	// 相隔多久无通信则重启
	int comm_lost_timeout_ms;
} _RCTParam;

/*-------------------------------------------------------------------------------
  结 构 名:		_BlackListRule
  创   建:		wangds 2012-04-07
  功   能:		黑名单规则结构
-------------------------------------------------------------------------------*/
typedef struct
{
	// 连续Join容忍计数。当节点连续Join（而无数据）的次数达到此数值，将在未来一段时间内不再理会此节点的Join。
	//0 不启用，其它 次数
	int maxCnt;
	// 对由于连续Join而进入不理会状态的节点的锁定时间，超过这个时间，将重新开始响应此节点的Join，单位 秒
	int lockTime;
} _BlackListRule;

/*-------------------------------------------------------------------------------
  结 构 名:		_APInfo
  创   建:		wangds 2012-04-07
  功   能:		AP基本参数
-------------------------------------------------------------------------------*/
typedef struct
{
	// 测试类型
	u32 mode;
	// 服务器地址
	char servAddr[16];	/* 18 to 16, ipaddr max 16-byte, wjzhe */
	// 服务程序端口
	u16 servPort;
	// 无线接收器参数
	_RCTParam rf[2];
	// 地址过滤
	u8 addr;
	u8 debug;
	// 累加和校验
	u8 sum;
} _APParam;

//========================其它结构=========================//
/*-------------------------------------------------------------------------------
  结 构 名:		_REParam
  创   建:		wangds 2012-06-19
  功   能:		RE参数及状态值
-------------------------------------------------------------------------------*/
typedef struct
{
	// RE id
	u32 id;
	// 工作频道
	char channelSet, channelGet;
	// 报告RE运行状态的间隔，单位：秒
	u16 repIntervalSet, repIntervalGet;
	// 节点RSSI最大值
	u8 edRSSIMax;
	// 节点RSSI最小值
	u8 edRSSIMin;
	// 节点RSSI统计间隔
	u16 statIntervalSet, statIntervalGet;
	// 一键缴费状态：0 未获取，1 已获取
	u8 paramStatus;
} _REParam;

#endif

#ifndef _IAP_FLASH_H
#define _IAP_FLASH_H
#include "stm32f10x_flash.h"

void IAP_FLASH_Unlock(void);
void IAP_FLASH_Lock(void);
void IAP_FLASH_ClearFlag(unsigned int FLASH_FLAG);
int IAP_FLASH_If_Erase(unsigned int base_addr, unsigned int len, unsigned int page_size);
int IAP_FLASH_If_Write2(volatile unsigned int* FlashAddress, unsigned int* Data , int DataLength);

#endif


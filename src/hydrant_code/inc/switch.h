#ifndef _SWITCH_H_
#define	_SWITCH_H_

#include "system.h"
#include "device.h"

#define S1_STA (u8)((GPIOA->IDR&0X00008000)>>15)    //读S1引脚状态
#define S2_STA (u8)((GPIOC->IDR&0X00000400)>>10)    //读S2引脚状态
#define S3_STA (u8)((GPIOC->IDR&0X00001000)>>12)    //读S3引脚状态、第二版修改后的液位传感器的引脚

#define WATER_SENSOR_INIT_THRESHOLD_TIME	3000	//单位MS

extern u8 left_sensor_flag, right_sensor_flag;
extern u8 send_flag;
extern u8 adxl345_detect_flag;
void switch_init(void);
void get_device_state(dev_info *dev);

#endif

#ifndef TIMER_H
#define TIMER_H

#include "system.h"

extern vs32 jiffies;
extern vs32 _t1s_cnt;
extern u8 t1s_flag;
extern u8 t1s_flag_sensor;
void timer_1ms_config(void);
s32 TIM6_TickNum(void);

#define THREAD_CYCLE(n, ...) do { \
	static s32 _thread_last_time = 0; \
	if ((jiffies - _thread_last_time) < n) { \
		return __VA_ARGS__; \
	} \
	_thread_last_time = jiffies; \
} while (0)

#endif

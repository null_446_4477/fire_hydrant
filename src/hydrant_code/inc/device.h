#ifndef  _DEVICE_H
#define  _DEVICE_H
#include "system.h"
#define ID_BASE_ADDR 0x1FFFF7E8

//通用部分
#define PACKNUM_ID 0
#define PACKNUM_INDEX (PACKNUM_ID + 12)
#define PACKNUM_CMD (PACKNUM_INDEX + 1)
#define PACKNUM_DATALEN (PACKNUM_CMD + 1)
#define PACKNUM_DATA (PACKNUM_DATALEN + 2)
//状态上报
#define STA_NUM_DEV_STA	PACKNUM_DATA
#define STA_NUM_VOLT	STA_NUM_DEV_STA + 1
#define STA_NUM_VERSON_NUM	STA_NUM_VOLT + 2
#define STA_NUM_IMEI	STA_NUM_VERSON_NUM + 2
#define STA_NUM_IMSI	STA_NUM_IMEI + 15
#define STA_NUM_HYDRANT_UTC		STA_NUM_IMSI + 15
#define STA_NUM_WATER_UTC		STA_NUM_HYDRANT_UTC + 4
#define STA_NUM_CRC		STA_NUM_WATER_UTC + 4
//IAP请求
#define IAPREQ_NEW_VERSONNUM	PACKNUM_DATA
#define IAPREQ_PACK_CNT	(IAPREQ_NEW_VERSONNUM + 2)
#define IAPREQ_CRC		(IAPREQ_PACK_CNT + 2)
//状态应答
#define DATANUM_INDEX PACKNUM_DATA
#define DATANUM_UTC		(DATANUM_INDEX + 1)
#define DATANUM_VERSON_NUM (DATANUM_UTC + 4)
#define DATANUM_PACK_NUM (DATANUM_VERSON_NUM + 2)
#define DATANUM_BIN_CRC (DATANUM_PACK_NUM + 2)
#define DATANUM_BIN_LEN (DATANUM_BIN_CRC + 2)
//IAP应答
#define IAPNUM_PACK_CNT PACKNUM_DATA
#define IAPNUM_PACK_CRC (IAPNUM_PACK_CNT + 2)
#define IAPNUM_VERSON_NUM (IAPNUM_PACK_CRC + 2)
#define IAPNUM_DATA (IAPNUM_VERSON_NUM + 2)

//命令字
#define CMD_STA_TICK 0x10
#define CMD_STA_UPDATE 0x11
#define CMD_STA_RESPONSE 0x12
#define CMD_STA_RESPONSE_IAP 0x13
#define CMD_IAP_REQUEST 0x04
#define CMD_IAP_RESPONSE 0x05

#define STATE_LEN 43
#define IAP_LEN 4

typedef struct _dev_info {
	u16 id[6];//MCU_UID
	u8 serial_num[4];//设备序列号
	u8 run_mode;//工作模式，正常模式/未启用模式
	u8 gprs_conn_sta;//GPRS模块是否联网标志，未联网的话不允许工作
	u8 hydrant_sta;
	u8 water_sta;
	u16 voltage;
	u8 alarm;
	u8 reserve1;
	u8 reserve2;
	u8 gprs_cmd;
	u8 cmd_type;
	u8 is_retry;//标识是否是重发包
	double zero_ang;//标定的基准角度
	double real_ang;//实测的角度
	u32 hydrant_time;//发生变化的时刻
	u32 water_time;//发生变化的时刻
	u32 water_sensor_init_threshold_time;//液位传感器频繁误触发需要断电初始化的时间阈值
	u8 water_sta_change_flag;//用水状态改变标志，此标志用来做报警延迟功能，当用水状态改变时开始计时，
							//超过一定时间后改变water_sta，清零
} dev_info;

extern dev_info dev_state;

void get_uid(u16 *id);
void print_uid(void);

#endif


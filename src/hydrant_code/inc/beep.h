#ifndef _BEEP_H_
#define	_BEEP_H_

#include "system.h"
#include "device.h"

#define BEEP_ON {PAout(12) = 1;}			//蜂鸣器响
#define BEEP_OFF {PAout(12) = 0;}			//蜂鸣器灭

void beep_init(void);
void warning_beginning(void);
void beep_warnning_task(dev_info *dev);
#endif

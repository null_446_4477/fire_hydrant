#ifndef WAN_GPRS_H
#define WAN_GPRS_H

#include "system.h"

int uart3_tx(u8 *buf, u16 len);
int uart3_rx(void);
int dev_uart_init(void);

#endif

#ifndef UART_BLUETOOTH_H
#define UART_BLUETOOTH_H

#include "system.h"

typedef struct _param_conf_info {
	u8 conf_flag;//参数配置过程标识，置位表示正在一个配参流程中
	u8 open_uart_flag;//唤醒并开始配参流程后，首先开启USART2
	vs32 start_tick;//配参流程开始tick
	u8 recv_ready;
	int recv_len;
	u8 recv_buf[128];
	u8 send_buf[128];
} param_conf_info;

#define PARAM_GET_CPU_ID		0xA0
#define PARAM_GET_SERIAL_NUM	0xA1
#define PARAM_GET_HOST_INFO		0xA2
#define PARAM_GET_VERSION_NUM	0xA3
#define PARAM_GET_DEV_STA		0xA4
#define PARAM_GET_IMEI			0xA5
#define PARAM_GET_IMSI			0xA6
#define PARAM_GET_RUN_MODE		0xA7
#define PARAM_GET_WT_INIT_OFFSET	0xA8

#define PARAM_SET_SERIAL_NUM	0x50
#define PARAM_SET_HOST_INFO		0x51
#define PARAM_SET_RUN_MODE		0x52
#define PARAM_SET_WT_INIT_OFFSET	0x53

void uart2_tx(u8 *buf, u16 len);
int bluetooth_uart_init(void);
void bluetooth_uart_close(void);
void bluetooth_exitirq_init(void);
void bluetooth_exitirq_close(void);
int conf_scan(void);
int conf_get_ready(void);

#endif

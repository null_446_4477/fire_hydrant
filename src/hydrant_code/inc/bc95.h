#ifndef _BC95_H_
#define	_BC95_H_

#include "system.h"
#include "device.h"
#include "rtc.h"

#define BC95_INIT_TIMEOUT 3000
#define BC95_COMM_TIMEOUT 1000
#define BC95_RECV_TIMEOUT 45
#define BC95_SEND_TRYNUM 5

#define BC95_TICK_INTERVAL	28800
#define BC95_IAP_INTERVAL 1

#define BC95_COMM_SERVER_IP "47.105.148.45"
#define BC95_COMM_SERVER_PORT 10211

#define OFFSET_O 4
#define OFFSET_K 3

#define MAX_BUFSIZE 1024
#define MAX_IAPSIZE	256

#define BAND_800 20
#define BAND_850 5
#define BAND_900 8

#define BC95_COMM_STATUS_LEN 4
#define BC95_COMM_STATUS_CMD 1

#define RESP_INIT "Neul \r\nOK\r\n"  //初始化后的应答字符串

typedef struct _client_param {
	u8 fd;
	char type[5];
	char protocol[2];
	int port;
	u8 recv_mode;
	char imei[15];
	char imsi[15];
	u8 net_alive;
	u8 net_registe;
	u8 net_worksta;
} client_param;
typedef struct _server_param {
	char addr[128];
	int port;
} server_param;

typedef struct _state_update {
	u8 cmd;
	u16 id[6];
	u8 index;
	u8 dev_sta;
	u16 voltage;
	u16 verson_num;
	char code_imei[15];
	char code_imsi[15];
	u32 hydrant_time;
	u32 water_time;
} bc95_sta_update;

typedef struct _state_req {
	int recv_complete;//接收到新消息
	int cmd;//应答包分类
	int index;//命令唯一标识，索引号
	u32 utc_time;//校时
	int verson_num;//可升级版本号
	int pack_num;//总包数
	u16 bin_crc;//bin文件校验值
	int bin_len;//bin文件总长度
} bc95_sta_req;

typedef struct _iap_update {
	u16 id[6];
	u8 index;
	int new_verson_num;//正在请求升级的版本号
	int pack_cnt;//请求的包号
} bc95_iap_update;

typedef struct _iap_req {
	int recv_complete;//接收到新消息
	int index;//命令唯一标识，索引号
	int pack_cnt;//当前包号
	u16 pack_crc;//当前包校验值
	int verson_num;//正在升级的版本号
	char data[MAX_IAPSIZE];//升级包数据
	int pack_len;//当前包字节长度
} bc95_iap_req;

typedef struct _bc95_info {
	client_param client_info;
	server_param server_info;
	u32 comm_interval;//通信间隔
	int recv_len;//接收到的UDP消息包长度
	int send_num;//需要发送的UDP消息包长度
	u8 recv_buf[MAX_BUFSIZE];//接收缓存
	u8 send_buf[MAX_BUFSIZE];//发送缓存
	u8 recv_complete;//串口通信接收完成
	u8 recv_data_flag;//接收到UDP消息包提示
	bc95_sta_update sta_update;//存储已经发送的数据
	bc95_sta_req sta_req;//存储接收到的服务器应答
	bc95_iap_update iap_update;//存储已经发送的iap请求
	bc95_iap_req iap_req;//存储接收到的iap数据
} bc95_info;

enum {
	GPRS_CMD_NONE = 0,
	GPRS_CMD_SEND,
	GPRS_CMD_WAIT,
	GPRS_CMD_RECV,
};

typedef struct _iap_comm_info {
	int gprs_cmd;
	int iap_cmd;// iap命令，1为正在进行升级，0为无升级任务
	int version;// 新升级包的版本号
	int pack_num;// 新升级包的分包总数量
	int pack_id;// 当前传输的升级包
	u16 bin_crc;// 新升级包的CRC校验
	u16 data[MAX_IAPSIZE / 2];//接收到的升级包数据
	int data_len;//升级包的长度，分辨率为半字(u16)
	u32 write_len;//已经写入FILASH的长度，分辨率为半字(u16)
} iap_comm_info;

extern bc95_info bc95_body;
extern iap_comm_info iap;
extern int reboot_cnt;

int bc95_init(void);
int udp_trans(u8 *data, u8 num);
void get_net_sta(void);
int udp_creatsocket(void);
int udp_closesocket(void);
int bc95_cmd_reboot(void);
int bc95_req_ping(void);
int bc95_req_pdpaddr(void);
void server_task(dev_info *dev);
void iap_task(iap_comm_info *p);
void msg_recv_task(dev_info *dev);
void sync_rtc_time(void);

#endif

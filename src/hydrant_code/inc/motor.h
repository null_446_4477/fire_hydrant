#ifndef _MOTOR_H_
#define	_MOTOR_H_

#include "system.h"
#include "device.h"
enum {
	MOTOR_NONE = 0,
	MOTOR_LOCK,
	MOTOR_UNLOCK,
};

#define FWD_ON {PAout(7) = 1;}
#define FWD_OFF {PAout(7) = 0;}
#define REV_ON {PAout(6) = 1;}
#define REV_OFF {PAout(6) = 0;}

#define INA_STA (u8)((GPIOA->IDR&0X00000080)>>7)    //读INA引脚状态
#define INB_STA (u8)((GPIOA->IDR&0X00000040)>>6)    //读INB引脚状态
void relay_init(void);
void foreward_motor(void);
void reversal_motor(void);
void idle_motor(void);
void stop_motor(void);;
void motor_lock(void);
void motor_unlock(void);
u8 get_motor_sta(void);
void motor_task(dev_info *dev);

#endif

#ifndef SLEEP_H
#define SLEEP_H

#include "system.h"

u32 sys_sleep_gets(u32 start, u32 end);
int sys_sleep(u32 t);
void sys_wakeup_clkconfig(void);            //唤醒后时钟初始化
void sys_wakeup_config(void);               //唤醒后的各单元配置工作
void sys_sleep_scan(void);

#endif

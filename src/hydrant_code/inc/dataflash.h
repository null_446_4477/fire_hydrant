#ifndef DATA_FLASH_H
#define DATA_FLASH_H

#define DATAFLASH_PAGEZISE	264
#define DATAFLASH_PAGE_MASK	0x1FFF
#define DATAFLASH_BASE_MASK	0x3FFF

#define CODE_BASEPAGE 560

#define FLASH_ADDR(P, B)	(((P & DATAFLASH_PAGE_MASK) << 10) | (B & DATAFLASH_BASE_MASK))
#define FLASH_PAGE_BY_ADDR(A)	(((A) >> 10) & DATAFLASH_PAGE_MASK)
#define FLASH_BASE_BY_ADDR(A)	((A) & DATAFLASH_BASE_MASK)

#define PAGE_INVAILD	0xFFFF	// 不是可操作页

#define FLASH_PAGE_APP CODE_BASEPAGE

void dataflash_init(void);
int dataflash_read(u16 page, u16 base, u8 *buf, u16 len);
int dataflash_write(u32 page, u16 base, u8 *buf, u32 len);
int dataflash_earse(u16 page);

#endif


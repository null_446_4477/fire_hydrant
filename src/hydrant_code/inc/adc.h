#ifndef ADC_H
#define ADC_H
#include "system.h"
#include "device.h"

/*温度AD采样通道定义*/
#define ADC_CH_TEMP  	ADC_Channel_16 
extern u16 low_power_val;
void ADC_config(void);               //ADC初始化
u16 adc_get_realvol(void);       //获取真实电压，返回电压BCD码，精确度小数点后一位
u16 adc_sample(void);                //ADC采样
char Temperature_Get(char recollect);//获取温度
void check_batteryvolt(void);        //检测电池电压是否低于阈值
void battery_task(dev_info *dev);

#endif

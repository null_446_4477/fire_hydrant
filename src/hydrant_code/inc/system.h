#ifndef SYSTEM_H
#define SYSTEM_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "stm32f10x.h"
#include "stm32f10x_conf.h"
typedef int  s32;
typedef short s16;
//typedef char  s8;

typedef unsigned int  u32;
typedef unsigned short u16;
typedef unsigned char  u8;

typedef volatile int  vs32;
typedef volatile short vs16;
//typedef volatile char  vs8;

typedef volatile unsigned int  vu32;
typedef volatile unsigned short vu16;
typedef volatile unsigned char  vu8;

#define enableGlobalInterrupts()   __set_PRIMASK(0);
#define disableGlobalInterrupts()  __set_PRIMASK(1);
/*
 * __enable_irq "cpsie i" : : : "memory"
 * 内在函数			操作码		PRIMASK	FAULTMASK
 * __enable_irq		CPSIE i		0
 * __disable_irq	CPSID i		1	 
 * __enable_fiq		CPSIE f	 			0
 * __disable_fiq	CPSID f	 			1
 */

#define constant_swab32(x) ((u32)(					\
	(((u32)(x) & (u32)0x000000ffUL) << 24) |		\
	(((u32)(x) & (u32)0x0000ff00UL) <<  8) |		\
	(((u32)(x) & (u32)0x00ff0000UL) >>  8) |		\
	(((u32)(x) & (u32)0xff000000UL) >> 24)))


#define set_func_v1(f) f = f##_v1
#define set_func_v2(f) f = f##_v2

/*
 * DEBUG: 1 错误, 2 警告, 3 基本调试信息, 4 全部调试信息
 */

#define DBG_ERR		1
#define DBG_WARN	2
#define DBG_BASE	3
#define DBG_ALL		4
#define DEBUG		5
#ifdef  DEBUG
#define debug(fmt,args...)		printf (fmt ,##args)
#define debugL(level,fmt,args...)	\
	do {							\
		if (DEBUG >= level) {		\
			printf(fmt, ##args);		\
		}							\
	} while(0)
#else   
#define debug(fmt,args...)   
#define debugL(level,fmt,args...)   
#endif

#define PR_DEBUG		4
#ifdef  PR_DEBUG
#define pr_debug(level,fmt,args...)	\
	do {							\
		if (PR_DEBUG >= level) {	\
			printf(fmt, ##args);	\
		}							\
	} while(0)
#else   
#define pr_debug(level,fmt,args...)   
#endif

#define BUG() while (1)
	
#define PI 3.141592653589793f
#define deg2rad(x)	((x) * PI / 180.0f)
#define rad2deg(x)	((x) / PI * 180.0f)

#define min(x, y) (((x) < (y)) ? (x) : (y))
#define max(x, y) (((x) > (y)) ? (x) : (y))
#define offsetof(TYPE, MEMBER) ((u32) &((TYPE *)0)->MEMBER)

#define LOBYTE(W)	(unsigned char)((W) & 0xFF)
#define HIBYTE(W)	(unsigned char)(((W) >> 8) & 0xFF)
#define time_diff(a, b) ((s32)(a) - (s32)(b))
#define getbit(data, x) (((data) & (1<<(x)))?1:0)

/*************************************************************************************************************/
/*********************************************************位带操作*********************************************/
/*************************************************************************************************************/

//IO口操作宏定义
#define BITBAND(addr, bitnum) ((addr & 0xF0000000)+0x2000000+((addr &0xFFFFF)<<5)+(bitnum<<2)) 
#define MEM_ADDR(addr)  *((volatile unsigned long  *)(addr)) 
#define BIT_ADDR(addr, bitnum)   MEM_ADDR(BITBAND(addr, bitnum)) 
//IO地址映射
#define GPIOA_ODR_Addr    (GPIOA_BASE+12) //0x4001080C 
#define GPIOB_ODR_Addr    (GPIOB_BASE+12) //0x40010C0C 
#define GPIOC_ODR_Addr    (GPIOC_BASE+12) //0x4001100C 
#define GPIOD_ODR_Addr    (GPIOD_BASE+12) //0x4001140C 
#define GPIOE_ODR_Addr    (GPIOE_BASE+12) //0x4001180C 
#define GPIOF_ODR_Addr    (GPIOF_BASE+12) //0x40011A0C    
#define GPIOG_ODR_Addr    (GPIOG_BASE+12) //0x40011E0C    

#define GPIOA_IDR_Addr    (GPIOA_BASE+8) //0x40010808 
#define GPIOB_IDR_Addr    (GPIOB_BASE+8) //0x40010C08 
#define GPIOC_IDR_Addr    (GPIOC_BASE+8) //0x40011008 
#define GPIOD_IDR_Addr    (GPIOD_BASE+8) //0x40011408 
#define GPIOE_IDR_Addr    (GPIOE_BASE+8) //0x40011808 
#define GPIOF_IDR_Addr    (GPIOF_BASE+8) //0x40011A08 
#define GPIOG_IDR_Addr    (GPIOG_BASE+8) //0x40011E08 
 
//IO口操作，只对单一IO口!
//确保n的值小于16!
#define PAout(n)   BIT_ADDR(GPIOA_ODR_Addr,n)  //输出 
#define PAin(n)    BIT_ADDR(GPIOA_IDR_Addr,n)  //输入

#define PBout(n)   BIT_ADDR(GPIOB_ODR_Addr,n)  //输出
#define PBin(n)    BIT_ADDR(GPIOB_IDR_Addr,n)  //输入

#define PCout(n)   BIT_ADDR(GPIOC_ODR_Addr,n)  //输出
#define PCin(n)    BIT_ADDR(GPIOC_IDR_Addr,n)  //输入

#define PDout(n)   BIT_ADDR(GPIOD_ODR_Addr,n)  //输出
#define PDin(n)    BIT_ADDR(GPIOD_IDR_Addr,n)  //输入

#define PEout(n)   BIT_ADDR(GPIOE_ODR_Addr,n)  //输出
#define PEin(n)    BIT_ADDR(GPIOE_IDR_Addr,n)  //输入

#define PFout(n)   BIT_ADDR(GPIOF_ODR_Addr,n)  //输出
#define PFin(n)    BIT_ADDR(GPIOF_IDR_Addr,n)  //输入

#define PGout(n)   BIT_ADDR(GPIOG_ODR_Addr,n)  //输出
#define PGin(n)    BIT_ADDR(GPIOG_IDR_Addr,n)  //输入
/***************************************************************************************************************************************************/	

#define VERSION_NUM 200

void reset(void);
void iwdg_reload(void);
void hex_dump(char *str, unsigned char *pSrcBufVA, unsigned int SrcBufLen);

#endif

#ifndef UART_DBG_H
#define UART_DBG_H

#include <system.h>

#define DGBUART USART1

int dbg_uart_init(void);
int uart_scan(void);

void wait_key(void);

#endif


#ifndef __LED_H
#define __LED_H

#include "system.h"

/* LED Definitions */
#define LED_NUM     2                   /* Number of user LEDs                */
#define BATTERY_LED 0
void led_init(void);
void led_on(u32 num);
void led_off(u32 num);
void led_out(u32 value);
// 返回LED [?]当前状态, 返回1 on, 返回0 off
int led_status(u32 num);

#endif


#ifndef _CHIPINIT_H_
#define	_CHIPINIT_H_

#include "system.h"

void reset(void);
void iwdg_init(void);//初始狗
void iwdg_start(void);//养狗开始
void iwdg_reload(void);//喂狗
void RCC_config(void);//时钟配置
void updata_master_lock(void);//上报主机地锁状态
void updata_master_car(void);//上报主机车辆状态
void check_updata_master_car(void);//将上报车辆状态封装成独立的任务，省掉mdelay
void check_updata_master_lock(void);//将上报地锁状态封装成独立的任务，省掉mdelay
void updata_master(void);//上报主机
void auto_lock_function(void);//解锁状态下，无车时，自动上锁。
void wait_updata_time(u16 num);//等待定时上报主机
void onchipinit(void);

#endif

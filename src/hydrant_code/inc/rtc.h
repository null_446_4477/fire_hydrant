#ifndef RTC_H
#define RTC_H

#include "system.h"

// bkp 配置完成标志
#define RTC_PERIOD	(40000 - 1)

typedef long long time64_t;
typedef unsigned int time_t;

void RTC_config(void);

void rtc_nvic_config(void);
u32 rtc_get_msec(void);
u32 rtc_get_sec(void);
void rtc_set_sec(u32 sec);
time64_t mktime64(const unsigned int year0, const unsigned int mon0,
		const unsigned int day, const unsigned int hour,
		const unsigned int min, const unsigned int sec);


#endif
